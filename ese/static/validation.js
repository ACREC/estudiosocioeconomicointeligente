function validar()
{
    $.each($('.validation'), function(index, elemento){
        if ($(elemento).hasClass('val-required')){
            if($(elemento).val().trim() == 0){
                $(elemento).focus().addClass('invalid');
                return false;
            } 
            else
                $(elemento).addClass('valid').removeClass('invalid');
        }
    });
    return false;
}