from django.shortcuts import render
from project_zarah.models import *
import tensorflow as tf
from tensorflow import keras
import numpy as np
import json
import os
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.db.models import Q
from operator import __or__ as OR
import functools

def myconverter(o):
    if isinstance(o, np.float32):
        return float(o)

def homedes(request):
    return render(request, 'ese/home.html', {'paciente' : Pacientes.objects.using('acrec_principal').get(id = int(request.GET.get('paciente'))), 'ese' : request.GET.get('ese')})

def is_empty(diccionario):
    if(diccionario):
        return True
    else:
        return False


# URLS PARA LLENAR LISTAS POR AJAX

def lista_tipo_egreso(request):
    egresos = list(Tipo_Egreso_List.objects.values())
    data = {'data' : egresos}
    return HttpResponse(json.dumps(data))

def lista_escolaridad(request):
    modelo = list(Escolaridad_List.objects.values())
    data = {'data' : modelo}
    return HttpResponse(json.dumps(data))

def lista_parentesco(request):
    modelo = list(Parentesco_list.objects.values())
    data = {'data' : modelo}
    return HttpResponse(json.dumps(data))

def lista_sexo(request):
    modelo = list(Sexo_List.objects.values())
    data = {'data' : modelo}
    return HttpResponse(json.dumps(data))

def lista_tipo_ingreso(request):
    modelo = list(Ingresos_tipo_list.objects.values())
    data = {'data' : modelo}
    return HttpResponse(json.dumps(data))

def lista_tipo_vivienda(request):
    modelo = list(Tipo_Vivienda_List.objects.values())
    data = {'data' : modelo}
    return HttpResponse(json.dumps(data))

def lista_estado_civil(request):
    modelo = list(Estado_Civil_List.objects.values())
    data = {'data' : modelo}
    return HttpResponse(json.dumps(data))

def lista_ocupacion(request):
    modelo = list(Ocupaciones_list.objects.values())
    data = {'data' : modelo}
    return HttpResponse(json.dumps(data))

def lista_institucion(request):
    modelo = list(Institucion_Academica_List.objects.values())
    data = {'data' : modelo}
    return HttpResponse(json.dumps(data))

def lista_dependiente(request):
    modelo = list(Dependiente_List.objects.values())
    data = {'data' : modelo}
    return HttpResponse(json.dumps(data))

def lista_discapacidad(request):
    modelo = list(Discapacidad_List.objects.values())
    data = {'data' : modelo}
    return HttpResponse(json.dumps(data))

# FIN DE LAS URLS


def datos_generales(request, id = 0):
    sexo_list = Sexo_List.objects.all()
    edo_civil_list = Estado_Civil_List.objects.all()
    escolaridad_list = Escolaridad_List.objects.all()
    ocupacion_list = Ocupaciones_list.objects.all()
    discapacidad_list = Discapacidad_List.objects.all()
    lists = {
        'sexos' : sexo_list,
        'edos_civil' : edo_civil_list,
        'escolaridades' : escolaridad_list,
        'ocupaciones' : ocupacion_list,
        'discapacidades' : discapacidad_list
    }
    paciente = Pacientes.objects.using('acrec_principal').get(id = int(request.GET.get('paciente')))
    data = {
        'listas' : lists,
        'paciente' : paciente
    }
    if(id != 0):
        datos_g = Estudio_Socioeconomico.objects.get(id=id)
        data.update({'datos' : datos_g })
    return render(request, 'ese/datos_generales.html', data)

def datos_generales_crupdate(request, ese_id = 0):
    params = json.loads(request.body.decode('utf-8'))
    ese, created = Estudio_Socioeconomico.objects.update_or_create(
        id = (ese_id == 0 if None else ese_id),
        defaults ={
            'apaterno' : params['apaterno'],
            'amaterno' : params['amaterno'],
            'nombres' : params['nombres'],
            'fecha_nacimiento' : params['fecha_nacimiento'],
            'sexo_list_id' : params['sexo_list_id'],
            'estado_civil_list_id' : params['estado_civil_list_id'],
            'ocupaciones_list_id' : params['ocupaciones_list_id'],
            'escolaridad_list_id' : params['escolaridad_list_id'],
            'discapacidad_list_id' : params['discapacidad_list_id']
        }
    )
    data = {
        'status' : created,
        'id' : ese.id
    }
    return HttpResponse(json.dumps(data))


def domicilio(request, id):
    domicilio = Estudio_Socioeconomico.objects.get(id = id)
    return render(request, 'ese/domicilio.html', { 'datos' : domicilio })

def domicilio_crupdate(request, ese_id = 0):
    params = json.loads(request.body.decode('utf-8'))
    ese, created = Estudio_Socioeconomico.objects.update_or_create(
        id = ese_id,
        defaults = {
            'codigo_postal' : params['codigo_postal'],
            'calle' : params['calle'],
            'entre_calles' : params['entre_calles'],
            'colonia' : params['colonia'],
            'localidad' : params['localidad'],
            'email' : params['email'],
            'telefono_fijo' : params['telefono'],
            'telefono_celular' : params['celular'],
            'num_exterior' : params['num_exterior'],
            'num_interior' : params['num_interior'],
            'ref_domicilio' : params['ref_domicilio']
        }
    )
    data = {
        'status' : created,
        'id' : ese.id
    }
    return HttpResponse(json.dumps(data))

def integrantes(request, id):
    integrantes_hogar = Integrantes_Hogar.objects.filter(estudio_socioeconomico_id = id)
    ocupacion_list = Ocupaciones_list.objects.all()
    parentesco_list = Parentesco_list.objects.all()
    institucion_list = Institucion_Academica_List.objects.all()
    dependiente_list = Dependiente_List.objects.all()
    discapacidad_list = Discapacidad_List.objects.all()
    return render(request, 'ese/integrantes_hogar.html',
    {
        'datos' : integrantes_hogar,
        'ocupaciones' : ocupacion_list,
        'id' : id,
        'parentescos' : parentesco_list,
        'instituciones' : institucion_list,
        'dependientes' : dependiente_list,
        'discapacidades' : discapacidad_list
    })

def integrantes_crupdate(request, ese_id = None, id = None):
    params = json.loads(request.body.decode('utf-8'))
    if id == 0:
        id = None
    ese,created = Integrantes_Hogar.objects.update_or_create(
        id = id,
        defaults = {
            'nombre' : params['nombre'],
            'edad' : params['edad'],
            'dependiente_list_id' : params['dependiente_list_id'],
            'parentesco_list_id' : params['parentesco_list_id'],
            'institucion_academica_list_id' : params['institucion_academica_list_id'],
            'ocupaciones_list_id' : params['ocupaciones_list_id'],
            'estudio_socioeconomico_id' : ese_id,
            'discapacidad_list_id' : params['discapacidad_list_id'],
            'trabajo' : params['trabajo']
        }
    )
    data = {
        'status' : created,
        'id' : ese.id
    }
    return HttpResponse(json.dumps(data))

def integrantes_delete(request, id):
    integrante = Integrantes_Hogar.objects.get(id = id)
    integrante.delete()
    return HttpResponse(json.dumps({ 'status' : 'success' }))

def ingresos(request, id):
    ingresos = Ingresos.objects.filter(estudio_socioeconomico_id = id)
    parentesco_list = Parentesco_list.objects.all()
    tipos = Ingresos_tipo_list.objects.all()
    ocupaciones = Ocupaciones_list.objects.all()
    return render(request, 'ese/ingresos.html',
    {
        'datos' : ingresos,
        'id' : id,
        'parentescos' : parentesco_list,
        'tipos' : tipos,
        'ocupaciones' : ocupaciones
    })

def ingresos_crupdate(request, ese_id = None, id = None):
    params = json.loads(request.body.decode('utf-8'))
    if id == 0:
        id = None
    ese, created = Ingresos.objects.update_or_create(
        id = id,
        defaults = {
            'estudio_socioeconomico_id' : ese_id,
            'nombre' : params['nombre'],
            'edad' : params['edad'],
            'ingreso' : params['ingreso'],
            'ocupaciones_list_id' : params['ocupaciones_list_id'],
            'ingresos_tipo_list_id' : params['ingresos_tipo_list_id'],
            'parentesco_list_id' : params['parentesco_list_id']
        }
    )
    data = {
        'status' : created,
        'id' : ese.id
    }
    return HttpResponse(json.dumps(data))

def ingresos_delete(request, id):
    integrante = Ingresos.objects.get(id = id)
    integrante.delete()
    return HttpResponse(json.dumps({ 'status' : 'success' }))


def egresos(request, id):
    egresos = Egresos.objects.filter(estudio_socioeconomico_id = id)
    tipos = Tipo_Egreso_List.objects.all()
    return render(request, 'ese/egresos.html',{ 'datos' : egresos, 'tipos' : tipos, 'id' : id, })

def egresos_crupdate(request, ese_id = None, id=0):
    params = json.loads(request.body.decode('utf-8'))
    if id == 0:
        id = None
    ese, created = Egresos.objects.update_or_create(
        id = id,
        defaults = {
            'estudio_socioeconomico_id' : ese_id,
            'cantidad' : params['cantidad'],
            'observacion' : params['observacion'],
            'tipo_egreso_list_id' : params['tipo_egreso_list_id']
        }
    )
    data = { 'status' : created, 'id' : ese.id }
    return HttpResponse(json.dumps(data))

def egresos_delete(request, id):
    egreso = Egresos.objects.get(id = id)
    egreso.delete()
    return HttpResponse(json.dumps({ 'status' : 'success' }))

def vivienda(request, id):
    tipo_vivienda = Tipo_Vivienda_List.objects.all()
    tipo_propiedad = Tipo_Propiedad_List.objects.all()
    mat_piso = Tipo_Piso_List.objects.all()
    mat_paredes = Tipo_Paredes_List.objects.all()
    mat_techo = Tipo_Techo_List.objects.all()
    ese = Estudio_Socioeconomico.objects.get(id = id)
    lista_servicios = Servicios_List.objects.all()
    higiene = Higiene_List.objects.all()
    servicios = ese.servicios.all()
    lista_caracteristicas = Caracteristicas_List.objects.all()
    lista_manajes = Manaje_List.objects.all()
    lista_estado_general = Estado_General_Vivienda_List.objects.all()
    caracteristicas = Estudio_Socioeconomico_Caracteristicas.objects.filter(estudio_socioeconomico_id = id)
    manajes = Estudio_Socioeconomico_Manaje.objects.filter(estudio_socioeconomico_id = id)
    data = {
        'mat_piso' : mat_piso,
        'mat_paredes' : mat_paredes,
        'mat_techo' : mat_techo,
        'tipos_vivienda' : tipo_vivienda,
        'tipos_propiedad' : tipo_propiedad,
        'ese' : ese,
        'lista_servicios' : lista_servicios,
        'lista_higiene' : higiene,
        'lista_caracteristicas' : lista_caracteristicas,
        'lista_manajes' : lista_manajes,
        'servicios' : servicios,
        'caracteristicas' : caracteristicas,
        'manajes' : manajes,
        'lista_estado_general' : lista_estado_general
    }
    return render(request, 'ese/vivienda.html', { 'datos' : data })

def vivienda_crupdate(request, id):
    params = json.loads(request.body.decode('utf-8'))
    if id == 0:
        id = None
    ese, created = Estudio_Socioeconomico.objects.update_or_create(
        id = id,
        defaults = {
            'numero_habitantes' : params['numero_habitantes'],
            'tipo_vivienda_list_id' : params['tipo_vivienda_list_id'],
            'tipo_propiedad_list_id' : params['tipo_propiedad_list_id'],
            'm2_terreno' : params['m2_terreno'],
            'm2_construccion' : params['m2_construccion'],
            'tiempo_residencia' : params['tiempo_residencia'],
            'tipo_piso_list_id' : params['tipo_piso_list_id'],
            'tipo_techo_list_id' : params['tipo_techo_list_id'],
            'tipo_paredes_list_id' : params['tipo_paredes_list_id'],
            'higiene_list_id' : params['higiene_list_id'],
            'estado_general_vivienda_list_id' : params['estado_general_vivienda_list_id']
        }
    )
    ese.servicios.clear()
    ese.manajes.clear()
    ese.caracteristicas.clear()
    for servicio in params['servicios']:
        serv = Servicios_List.objects.get(id = servicio)
        ese.servicios.add(serv)
    for manaje in params['manajes']:
        man = Manaje_List.objects.get(id = manaje['id'])
        ese.manajes.add(man, through_defaults={ 'cantidad' : manaje['value']})
    for caracteristica in params['caracteristicas']:
        carac = Caracteristicas_List.objects.get(id = caracteristica['id'])
        ese.caracteristicas.add(carac, through_defaults={ 'cantidad' : caracteristica['value'] })
    data = { 'status' : created, 'id' : ese.id }
    return HttpResponse(json.dumps(data))

def alimentacion(request, id):
    lista_alimentos = Alimentos_List.objects.all()
    lista_frecuencias = Frecuencias_List.objects.all()
    lista_despensa = Despensas_List.objects.all()
    aliementos = Estudio_Socioeconomico_Alimentos.objects.filter(estudio_socioeconomico_id = id)
    ese = Estudio_Socioeconomico.objects.get(id = id)
    datos = {
        'lista_alimentos' : lista_alimentos,
        'lista_frecuencias' : lista_frecuencias,
        'lista_despensa' : lista_despensa,
        'alimentos' : aliementos,
        'ese' : ese
    }
    return render(request, 'ese/alimentacion.html', datos)

def alimentacion_crupdate(request, id):
    params = json.loads(request.body.decode('utf-8'))
    if id == 0:
        id = None
    ese, created = Estudio_Socioeconomico.objects.update_or_create(
        id = id,
        defaults = {'despensas_list_id' : params['despensas_list_id']}
    )
    ese.alimentacion.clear()
    for alimento in params['alimentos']:
        if alimento['value'] != '0':
            print(alimento['value'])
            ali = Alimentos_List.objects.get(id = alimento['id'])
            ese.alimentacion.add(ali, through_defaults = {'frecuencias_list_id' : alimento['value'], 'observaciones' : alimento['texto']})
    data = {'status' : created, 'id' : ese.id  }
    return HttpResponse(json.dumps(data))

def servicios_salud(request, id):
    ese = Estudio_Socioeconomico.objects.get(id = id)
    datos = {
        'lista_beneficiario' : Beneficiario_List.objects.all(),
        'lista_servicio_salud' : Servicio_Salud_List.objects.all(),
        'lista_enfermedad' : Enfermedades_List.objects.all(),
        'ese' : ese
    }
    return render(request, 'ese/servicios_de_salud.html', datos)

def servicios_salud_crupdate(request, id):
    params = json.loads(request.body.decode('utf-8'))
    if id == 0:
        id = None
    ese, created = Estudio_Socioeconomico.objects.update_or_create(
        id = id,
        defaults = {
            'beneficiario_list_id' : params['beneficiario_list_id'],
            'servicio_salud_list_id' : params['servicio_salud_list_id'],
            'enfermedades_list_id' : params['enfermedades_list_id']
        }
    )
    data = {'status' : created, 'id' : ese.id  }
    return HttpResponse(json.dumps(data))


def vehiculos(request, id):
    vehiculos = Vehiculos.objects.filter(estudio_socioeconomico_id = id)
    ese = Estudio_Socioeconomico.objects.get(id = id)
    return render(request, 'ese/vehiculos.html',{ 'datos' : vehiculos,  'id' : id, 'paciente' : ese.paciente_id})

def vehiculos_crupdate(request, ese_id = None, id=0):
    params = json.loads(request.body.decode('utf-8'))
    if id == 0:
        id = None
    ese, created = Vehiculos.objects.update_or_create(
        id = id,
        defaults = {
            'estudio_socioeconomico_id' : ese_id,
            'propietario' : params['propietario'],
            'marca' : params['marca'],
            'modelo' : params['modelo'],
            'valor_actual' : params['valor_actual'],
            'pagado' : params['pagado'],
            'adeudo' : params['adeudo']
        }
    )
    data = {'status' : created, 'id' : ese.id  }
    return HttpResponse(json.dumps(data))

def vehiculos_delete(request, id):
    vechiculo = Vehiculos.objects.get(id = id)
    vechiculo.delete()
    return HttpResponse(json.dumps({ 'status' : 'success' }))

def pacientes(request):
    pacientes_list = None
    if request.GET.get('apellido_paterno'):
        pacientes_list = Pacientes.objects.using('acrec_principal').filter(
            Q(apellido_paterno__icontains = request.GET.get('apellido_paterno')) &
            Q(apellido_materno__icontains = request.GET.get('apellido_materno')) &
            Q(nombre__icontains = request.GET.get('nombre'))
        ).order_by('-fecha_alta')
    elif (request.GET.get('clave')):
        pacientes_list = Pacientes.objects.using('acrec_principal').filter(clave = request.GET.get('clave')).order_by('-fecha_alta')
    else: 
        pacientes_list = Pacientes.objects.using('acrec_principal').all().order_by('-fecha_alta')
    paginator = Paginator(pacientes_list, 20)
    page = request.GET.get('page')
    pacientes = paginator.get_page(page)
    return render(request, 'ese/pacientes.html', {'pacientes' : pacientes, 'search_params' : request.GET})

def paciente(request, id):
    paciente = Pacientes.objects.using('acrec_principal').get(id = id)
    estudios = Estudio_Socioeconomico.objects.filter(paciente_id = id)
    clasificaciones = Clasificaciones_List.objects.all()
    return render(request, 'ese/paciente.html', {'paciente' : paciente, 'estudios' : estudios, 'clasificaciones' : clasificaciones})

def iniciar(request, id):
    ese, created = Estudio_Socioeconomico.objects.update_or_create(
        id = None,
        defaults = {
            'paciente_id' : id
        }
    )
    return HttpResponse(json.dumps({'status' : created, 'id' : ese.id }))

def observaciones(request, id):
    ese = Estudio_Socioeconomico.objects.get(id = id)
    clasificaciones = Clasificaciones_List.objects.all()
    return render(request, 'ese/observaciones.html', {'ese' : ese, 'clasificaciones' : clasificaciones})

def observaciones_crupdate(request, id):
    params = json.loads(request.body.decode('utf-8'))
    if id == 0:
        id = None
    ese, created = Estudio_Socioeconomico.objects.update_or_create(
        id = id,
        defaults = {
            'observaciones' : params['observaciones'],
            'diagnostico' : params['diagnostico'],
            'clasificacion_ts' : params['clasificacion_ts'],
            'falsea_datos' : params['falsea_datos']
        }
    )
    data = {'status' : created, 'id' : ese.id  }
    return HttpResponse(json.dumps(data))