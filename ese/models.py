from django.db import models

class House_Price(models.Model):
    date = models.DateTimeField('fecha')
    value = models.FloatField()
    quanta = models.IntegerField(default = 0)
    final_price = models.FloatField()

class Model_Def(models.Model):
    model_name = models.CharField(max_length = 45)
    model_optimizer = models.CharField(max_length = 100)
    model_loss = models.CharField(max_length = 100)
    model_trained = models.BooleanField()
    model_error = models.FloatField()
    model_acc = models.FloatField()
    model_dir = models.CharField(max_length = 100)