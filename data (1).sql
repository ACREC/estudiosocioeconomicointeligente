CREATE DATABASE  IF NOT EXISTS `ese_acrec` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ese_acrec`;
-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for Win64 (AMD64)
--
-- Host: 192.168.1.100    Database: ese_acrec
-- ------------------------------------------------------
-- Server version	10.2.6-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-04-04 18:55:03.771884'),(2,'auth','0001_initial','2019-04-04 18:55:04.209339'),(3,'admin','0001_initial','2019-04-04 18:55:04.309646'),(4,'admin','0002_logentry_remove_auto_add','2019-04-04 18:55:04.325270'),(5,'admin','0003_logentry_add_action_flag_choices','2019-04-04 18:55:04.356518'),(6,'contenttypes','0002_remove_content_type_name','2019-04-04 18:55:04.422264'),(7,'auth','0002_alter_permission_name_max_length','2019-04-04 18:55:04.470236'),(8,'auth','0003_alter_user_email_max_length','2019-04-04 18:55:04.507215'),(9,'auth','0004_alter_user_username_opts','2019-04-04 18:55:04.517209'),(10,'auth','0005_alter_user_last_login_null','2019-04-04 18:55:04.563467'),(11,'auth','0006_require_contenttypes_0002','2019-04-04 18:55:04.563467'),(12,'auth','0007_alter_validators_add_error_messages','2019-04-04 18:55:04.579092'),(13,'auth','0008_alter_user_username_max_length','2019-04-04 18:55:04.609732'),(14,'auth','0009_alter_user_last_name_max_length','2019-04-04 18:55:04.651287'),(15,'sessions','0001_initial','2019-04-04 18:55:04.694277');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ese_house_price`
--

DROP TABLE IF EXISTS `ese_house_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ese_house_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `value` float DEFAULT NULL,
  `quanta` int(11) DEFAULT NULL,
  `final_price` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ese_house_price`
--

LOCK TABLES `ese_house_price` WRITE;
/*!40000 ALTER TABLE `ese_house_price` DISABLE KEYS */;
INSERT INTO `ese_house_price` VALUES (1,'2019-04-04 15:32:00',50000,0,50000),(2,'2019-04-04 15:32:00',50000,1,100000),(3,'2019-04-04 15:32:00',50000,2,150000),(4,'2019-04-04 15:32:00',50000,3,200000),(5,'2019-04-04 15:32:00',50000,4,250000),(6,'2019-04-04 15:32:00',50000,15,800000);
/*!40000 ALTER TABLE `ese_house_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_caracteristicas`
--

DROP TABLE IF EXISTS `project_zarah_caracteristicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_caracteristicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caracteristicas_list_id` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_caracteristicas`
--

LOCK TABLES `project_zarah_caracteristicas` WRITE;
/*!40000 ALTER TABLE `project_zarah_caracteristicas` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_zarah_caracteristicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_caracteristicas_list`
--

DROP TABLE IF EXISTS `project_zarah_caracteristicas_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_caracteristicas_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caracteristica` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_caracteristicas_list`
--

LOCK TABLES `project_zarah_caracteristicas_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_caracteristicas_list` DISABLE KEYS */;
INSERT INTO `project_zarah_caracteristicas_list` VALUES (1,'Sala'),(2,'Comedor'),(3,'Cocina'),(4,'Recamaras'),(5,'Baños'),(6,'Garaje'),(7,'Jardín'),(8,'Alberca'),(9,'Otros');
/*!40000 ALTER TABLE `project_zarah_caracteristicas_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_egresos`
--

DROP TABLE IF EXISTS `project_zarah_egresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_egresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  `tipo_egreso_list_id` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `obervacion` varchar(100) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_egresos`
--

LOCK TABLES `project_zarah_egresos` WRITE;
/*!40000 ALTER TABLE `project_zarah_egresos` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_zarah_egresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_escolaridad_list`
--

DROP TABLE IF EXISTS `project_zarah_escolaridad_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_escolaridad_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `escolaridad` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_escolaridad_list`
--

LOCK TABLES `project_zarah_escolaridad_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_escolaridad_list` DISABLE KEYS */;
INSERT INTO `project_zarah_escolaridad_list` VALUES (1,'Primaria Trunca'),(2,'Primaria Concluida'),(3,'Secundaria Trunca'),(4,'Secundaria Concluida'),(5,'Bachillerato Trunco'),(6,'Bachillerato Concluido'),(7,'Carrera técnica Trunca'),(8,'Carrera técnica Concluida'),(9,'Licenciatura Trunca'),(10,'Licenciatura Concluida'),(11,'Maestría'),(12,'Doctorado'),(13,'Posdoctorado');
/*!40000 ALTER TABLE `project_zarah_escolaridad_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estado_civil_list`
--

DROP TABLE IF EXISTS `project_zarah_estado_civil_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estado_civil_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado_civil` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estado_civil_list`
--

LOCK TABLES `project_zarah_estado_civil_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_estado_civil_list` DISABLE KEYS */;
INSERT INTO `project_zarah_estado_civil_list` VALUES (1,'Soltero'),(2,'Casado'),(3,'Divorciado'),(4,'Unión Libre'),(5,'Separado'),(6,'Viudo');
/*!40000 ALTER TABLE `project_zarah_estado_civil_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estudio_socioeconomico`
--

DROP TABLE IF EXISTS `project_zarah_estudio_socioeconomico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estudio_socioeconomico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `sexo_list_id` int(11) DEFAULT NULL,
  `escolaridad_list_id` int(11) DEFAULT NULL,
  `estado_civil_list_id` int(11) DEFAULT NULL,
  `codigo_postal` int(11) DEFAULT NULL,
  `calle` varchar(255) DEFAULT NULL,
  `colonia` varchar(100) DEFAULT NULL,
  `entre_calles` varchar(100) DEFAULT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `telefono_fijo` varchar(20) DEFAULT NULL,
  `telefono_celular` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `apaterno` varchar(100) DEFAULT NULL,
  `amaterno` varchar(100) DEFAULT NULL,
  `nombres` varchar(100) DEFAULT NULL,
  `ocupaciones_list_id` int(11) DEFAULT NULL,
  `num_interior` varchar(45) DEFAULT NULL,
  `num_exterior` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estudio_socioeconomico`
--

LOCK TABLES `project_zarah_estudio_socioeconomico` WRITE;
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico` DISABLE KEYS */;
INSERT INTO `project_zarah_estudio_socioeconomico` VALUES (1,'CLI1605945','1995-05-10','2019-07-09 16:12:33',1,10,1,62320,'4A PRIVADA FRANCISCO VILLA','ANTONIO BARONA','EMILIANO ZAPATA Y FRANCISCO VILLA','CUERNAVACA','7779876543','7775956923','angelzuriel.barriosflores@gmail.com','BARRIOS','FLORES','ANGEL ZURIEL',17,'4','23');
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_ingresos`
--

DROP TABLE IF EXISTS `project_zarah_ingresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_ingresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `ingreso` double DEFAULT NULL,
  `ocupacion` varchar(100) DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_ingresos`
--

LOCK TABLES `project_zarah_ingresos` WRITE;
/*!40000 ALTER TABLE `project_zarah_ingresos` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_zarah_ingresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_integrantes_hogar`
--

DROP TABLE IF EXISTS `project_zarah_integrantes_hogar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_integrantes_hogar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `dependiente` tinyint(4) DEFAULT NULL,
  `parentezco` varchar(50) DEFAULT NULL,
  `institucion_academica` varchar(50) DEFAULT NULL,
  `ocupacoines_list_id` int(11) DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_integrantes_hogar`
--

LOCK TABLES `project_zarah_integrantes_hogar` WRITE;
/*!40000 ALTER TABLE `project_zarah_integrantes_hogar` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_zarah_integrantes_hogar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_model_def`
--

DROP TABLE IF EXISTS `project_zarah_model_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_model_def` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_optimizer` varchar(100) DEFAULT NULL,
  `model_loss` varchar(100) DEFAULT NULL,
  `model_trained` tinyint(1) DEFAULT NULL,
  `model_name` varchar(45) DEFAULT NULL,
  `model_error` double DEFAULT NULL,
  `model_acc` double DEFAULT NULL,
  `model_dir` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_model_def`
--

LOCK TABLES `project_zarah_model_def` WRITE;
/*!40000 ALTER TABLE `project_zarah_model_def` DISABLE KEYS */;
INSERT INTO `project_zarah_model_def` VALUES (1,'sgd','mse',1,'project_zarah',0.014284770004451275,0.9857152299955487,'project_zarah/cp.ckpt'),(2,NULL,NULL,0,'zarah_speech',0,0,'project_zarah/speech.ckpt');
/*!40000 ALTER TABLE `project_zarah_model_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_model_layers`
--

DROP TABLE IF EXISTS `project_zarah_model_layers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_model_layers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layer_units` int(11) DEFAULT NULL,
  `layer_activation` varchar(100) DEFAULT NULL,
  `layer_model_def_id` int(11) DEFAULT NULL,
  `layer_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_model_layers`
--

LOCK TABLES `project_zarah_model_layers` WRITE;
/*!40000 ALTER TABLE `project_zarah_model_layers` DISABLE KEYS */;
INSERT INTO `project_zarah_model_layers` VALUES (24,85,'relu',1,NULL),(25,100,'relu',1,NULL),(26,1,'sigmoid',1,NULL);
/*!40000 ALTER TABLE `project_zarah_model_layers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_ocupaciones_list`
--

DROP TABLE IF EXISTS `project_zarah_ocupaciones_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_ocupaciones_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ocupacion` varchar(100) DEFAULT NULL,
  `observacion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_ocupaciones_list`
--

LOCK TABLES `project_zarah_ocupaciones_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_ocupaciones_list` DISABLE KEYS */;
INSERT INTO `project_zarah_ocupaciones_list` VALUES (1,'Tecnicos y profesionales de nivel medio',NULL),(2,'Profesionales cientificos e intelectuales',NULL),(3,'Directores y gerentes de empresa',NULL),(4,'Dueños de Empresa',NULL),(5,'Ninguna','Solo menores de 5 años'),(6,'Hogar',NULL),(7,'Niños y Jovenes','Solo menores de 16 años que no trabajan ni estudian'),(8,'Desempleado (Mas de 3 meses sin trabajo)',NULL),(9,'Jubilado/Pensionado y Eventuales',NULL),(10,'Subempleo',NULL),(11,'Obrero',NULL),(12,'Empleado',NULL),(13,'Tecnico',NULL),(14,'Profesionista',NULL),(15,'Empresario o Ejecutivo',NULL),(16,'Estudiante',NULL),(17,'Trabajadores de Servicios y Vendedores de comercios y mercados',NULL),(18,'Oficiales, operarios y artesanos de artes mecanicas y otros oficios',NULL),(19,'Agricultores y trabjadores calificados agropecuarios y pesqueros',''),(20,'Empleados de Oficina',NULL),(21,'Dueños de Empresa e inversionistas',NULL),(22,'Desempleado/ (Mas de 3 meses sin trabajo) / Adultos con programas de gobierno',NULL),(23,'Empleados (Asalariado y Tecnico)',NULL),(24,'Profesionista, empresario o ejectuvo',NULL);
/*!40000 ALTER TABLE `project_zarah_ocupaciones_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_servicios`
--

DROP TABLE IF EXISTS `project_zarah_servicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_servicios` (
  `id` int(11) NOT NULL,
  `servicios_list_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_servicios`
--

LOCK TABLES `project_zarah_servicios` WRITE;
/*!40000 ALTER TABLE `project_zarah_servicios` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_zarah_servicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_servicios_list`
--

DROP TABLE IF EXISTS `project_zarah_servicios_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_servicios_list` (
  `id` int(11) NOT NULL,
  `servicio` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_servicios_list`
--

LOCK TABLES `project_zarah_servicios_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_servicios_list` DISABLE KEYS */;
INSERT INTO `project_zarah_servicios_list` VALUES (1,'Luz'),(2,'Agua'),(3,'Teléfono'),(4,'Pavimentación'),(5,'Drenaje'),(6,'Internet'),(7,'Vigilancia'),(8,'Servicio de Limpieza'),(9,'Gas'),(10,'TV. Paga'),(11,'TV. abierta'),(12,'Celular');
/*!40000 ALTER TABLE `project_zarah_servicios_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_sexo_list`
--

DROP TABLE IF EXISTS `project_zarah_sexo_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_sexo_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sexo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_sexo_list`
--

LOCK TABLES `project_zarah_sexo_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_sexo_list` DISABLE KEYS */;
INSERT INTO `project_zarah_sexo_list` VALUES (1,'Masculino'),(2,'Femenino');
/*!40000 ALTER TABLE `project_zarah_sexo_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_egreso_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_egreso_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_egreso_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `egreso` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_egreso_list`
--

LOCK TABLES `project_zarah_tipo_egreso_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_egreso_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_egreso_list` VALUES (1,'Agua'),(2,'Luz'),(3,'Telefono'),(4,'Salud'),(5,'Transporte'),(6,'Alimentación'),(7,'Ropa y calzado'),(8,'Educación'),(9,'Vivienda'),(10,'Otros');
/*!40000 ALTER TABLE `project_zarah_tipo_egreso_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_propiedad_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_propiedad_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_propiedad_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `propiedad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_propiedad_list`
--

LOCK TABLES `project_zarah_tipo_propiedad_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_propiedad_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_propiedad_list` VALUES (1,'Propia'),(2,'Rentada'),(3,'Hipotecada'),(4,'Renta Congelada'),(5,'Prestada'),(6,'Familiar');
/*!40000 ALTER TABLE `project_zarah_tipo_propiedad_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_vivienda_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_vivienda_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_vivienda_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vivienda` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_vivienda_list`
--

LOCK TABLES `project_zarah_tipo_vivienda_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_vivienda_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_vivienda_list` VALUES (1,'Casa Sola'),(2,'Duplex'),(3,'Condominio'),(4,'Departamento'),(5,'Vecindad'),(6,'Casa de Huéspedes');
/*!40000 ALTER TABLE `project_zarah_tipo_vivienda_list` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-09 13:08:10
