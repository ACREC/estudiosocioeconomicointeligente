CREATE DATABASE  IF NOT EXISTS `ese_acrec` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ese_acrec`;
-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for Win64 (AMD64)
--
-- Host: 192.168.1.100    Database: ese_acrec
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `cuenta_dependientes`
--

DROP TABLE IF EXISTS `cuenta_dependientes`;
/*!50001 DROP VIEW IF EXISTS `cuenta_dependientes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `cuenta_dependientes` (
  `dependientes` tinyint NOT NULL,
  `estudio` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `cuenta_egresos`
--

DROP TABLE IF EXISTS `cuenta_egresos`;
/*!50001 DROP VIEW IF EXISTS `cuenta_egresos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `cuenta_egresos` (
  `estudio` tinyint NOT NULL,
  `total_agua` tinyint NOT NULL,
  `total_luz` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `cuenta_ingresos`
--

DROP TABLE IF EXISTS `cuenta_ingresos`;
/*!50001 DROP VIEW IF EXISTS `cuenta_ingresos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `cuenta_ingresos` (
  `estudio` tinyint NOT NULL,
  `ingresos` tinyint NOT NULL,
  `edad` tinyint NOT NULL,
  `avg_parentesco` tinyint NOT NULL,
  `ocupacion` tinyint NOT NULL,
  `tipo` tinyint NOT NULL,
  `avg_ingreso` tinyint NOT NULL,
  `total_ingreso` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `cuenta_integrantes`
--

DROP TABLE IF EXISTS `cuenta_integrantes`;
/*!50001 DROP VIEW IF EXISTS `cuenta_integrantes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `cuenta_integrantes` (
  `estudio` tinyint NOT NULL,
  `integrantes` tinyint NOT NULL,
  `edad` tinyint NOT NULL,
  `avg_parentesco` tinyint NOT NULL,
  `institucion_academica` tinyint NOT NULL,
  `ocupacion` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-04-04 18:55:03.771884'),(2,'auth','0001_initial','2019-04-04 18:55:04.209339'),(3,'admin','0001_initial','2019-04-04 18:55:04.309646'),(4,'admin','0002_logentry_remove_auto_add','2019-04-04 18:55:04.325270'),(5,'admin','0003_logentry_add_action_flag_choices','2019-04-04 18:55:04.356518'),(6,'contenttypes','0002_remove_content_type_name','2019-04-04 18:55:04.422264'),(7,'auth','0002_alter_permission_name_max_length','2019-04-04 18:55:04.470236'),(8,'auth','0003_alter_user_email_max_length','2019-04-04 18:55:04.507215'),(9,'auth','0004_alter_user_username_opts','2019-04-04 18:55:04.517209'),(10,'auth','0005_alter_user_last_login_null','2019-04-04 18:55:04.563467'),(11,'auth','0006_require_contenttypes_0002','2019-04-04 18:55:04.563467'),(12,'auth','0007_alter_validators_add_error_messages','2019-04-04 18:55:04.579092'),(13,'auth','0008_alter_user_username_max_length','2019-04-04 18:55:04.609732'),(14,'auth','0009_alter_user_last_name_max_length','2019-04-04 18:55:04.651287'),(15,'sessions','0001_initial','2019-04-04 18:55:04.694277');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ese_house_price`
--

DROP TABLE IF EXISTS `ese_house_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ese_house_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `value` float DEFAULT NULL,
  `quanta` int(11) DEFAULT NULL,
  `final_price` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ese_house_price`
--

LOCK TABLES `ese_house_price` WRITE;
/*!40000 ALTER TABLE `ese_house_price` DISABLE KEYS */;
INSERT INTO `ese_house_price` VALUES (1,'2019-04-04 15:32:00',50000,0,50000),(2,'2019-04-04 15:32:00',50000,1,100000),(3,'2019-04-04 15:32:00',50000,2,150000),(4,'2019-04-04 15:32:00',50000,3,200000),(5,'2019-04-04 15:32:00',50000,4,250000),(6,'2019-04-04 15:32:00',50000,15,800000);
/*!40000 ALTER TABLE `ese_house_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `int_edades`
--

DROP TABLE IF EXISTS `int_edades`;
/*!50001 DROP VIEW IF EXISTS `int_edades`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `int_edades` (
  `edad` tinyint NOT NULL,
  `id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `normalizados`
--

DROP TABLE IF EXISTS `normalizados`;
/*!50001 DROP VIEW IF EXISTS `normalizados`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `normalizados` (
  `id` tinyint NOT NULL,
  `entrenamiento` tinyint NOT NULL,
  `edad` tinyint NOT NULL,
  `sexo` tinyint NOT NULL,
  `escolaridad` tinyint NOT NULL,
  `estado_civil` tinyint NOT NULL,
  `ocupacion` tinyint NOT NULL,
  `codigo_postal` tinyint NOT NULL,
  `integrantes` tinyint NOT NULL,
  `dependientes` tinyint NOT NULL,
  `int_edad` tinyint NOT NULL,
  `int_parentesco` tinyint NOT NULL,
  `institucion_academica` tinyint NOT NULL,
  `int_ocupacion` tinyint NOT NULL,
  `ingresos` tinyint NOT NULL,
  `ing_edad` tinyint NOT NULL,
  `avg_tipo_ingreso` tinyint NOT NULL,
  `avg_ing_parentesco` tinyint NOT NULL,
  `ing_ocupacion` tinyint NOT NULL,
  `avg_ingreso` tinyint NOT NULL,
  `total_ingreso` tinyint NOT NULL,
  `total_agua` tinyint NOT NULL,
  `total_luz` tinyint NOT NULL,
  `clasificacion_ts` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `project_zarah_alimentos_list`
--

DROP TABLE IF EXISTS `project_zarah_alimentos_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_alimentos_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_alimentos_list`
--

LOCK TABLES `project_zarah_alimentos_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_alimentos_list` DISABLE KEYS */;
INSERT INTO `project_zarah_alimentos_list` VALUES (1,'POLLO'),(2,'RES'),(3,'CERDO'),(4,'PESCADO'),(5,'LECHE'),(6,'CEREALES'),(7,'HUEVO'),(8,'FRUTAS'),(9,'VERDURAS'),(10,'LEGUMINOSAS (FRIJOL, LENTEJA, HABA)'),(11,'OTROS');
/*!40000 ALTER TABLE `project_zarah_alimentos_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_beneficiario_list`
--

DROP TABLE IF EXISTS `project_zarah_beneficiario_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_beneficiario_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_beneficiario_list`
--

LOCK TABLES `project_zarah_beneficiario_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_beneficiario_list` DISABLE KEYS */;
INSERT INTO `project_zarah_beneficiario_list` VALUES (1,'Propio'),(2,'Conyugue'),(3,'Hijo'),(4,'Padres'),(5,'Escuela');
/*!40000 ALTER TABLE `project_zarah_beneficiario_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_caracteristicas_list`
--

DROP TABLE IF EXISTS `project_zarah_caracteristicas_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_caracteristicas_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_caracteristicas_list`
--

LOCK TABLES `project_zarah_caracteristicas_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_caracteristicas_list` DISABLE KEYS */;
INSERT INTO `project_zarah_caracteristicas_list` VALUES (1,'SALA'),(2,'COMEDOR'),(3,'COCINA'),(4,'RECAMARAS'),(5,'BAÑOS'),(6,'GARAGE'),(7,'JARDIN'),(8,'ALBERCA'),(9,'PATIO DE SERVICIO');
/*!40000 ALTER TABLE `project_zarah_caracteristicas_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_clasificaciones_list`
--

DROP TABLE IF EXISTS `project_zarah_clasificaciones_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_clasificaciones_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_clasificaciones_list`
--

LOCK TABLES `project_zarah_clasificaciones_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_clasificaciones_list` DISABLE KEYS */;
INSERT INTO `project_zarah_clasificaciones_list` VALUES (1,'A-'),(2,'A'),(3,'A+'),(4,'B'),(5,'B+'),(6,'C'),(7,'C+'),(8,'D'),(9,'D+');
/*!40000 ALTER TABLE `project_zarah_clasificaciones_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_dependiente_list`
--

DROP TABLE IF EXISTS `project_zarah_dependiente_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_dependiente_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_dependiente_list`
--

LOCK TABLES `project_zarah_dependiente_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_dependiente_list` DISABLE KEYS */;
INSERT INTO `project_zarah_dependiente_list` VALUES (1,'Sí'),(2,'No');
/*!40000 ALTER TABLE `project_zarah_dependiente_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_despensas_list`
--

DROP TABLE IF EXISTS `project_zarah_despensas_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_despensas_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_despensas_list`
--

LOCK TABLES `project_zarah_despensas_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_despensas_list` DISABLE KEYS */;
INSERT INTO `project_zarah_despensas_list` VALUES (1,'Diario'),(2,'Semanal'),(3,'Quincenal'),(4,'Mensual');
/*!40000 ALTER TABLE `project_zarah_despensas_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_discapacidad_list`
--

DROP TABLE IF EXISTS `project_zarah_discapacidad_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_discapacidad_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_discapacidad_list`
--

LOCK TABLES `project_zarah_discapacidad_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_discapacidad_list` DISABLE KEYS */;
INSERT INTO `project_zarah_discapacidad_list` VALUES (1,'Discapacidad motriz'),(2,'Discapacidad visual.'),(3,'Discapacidad mental.'),(4,'Discapacidad auditiva.'),(5,'Discapacidad para hablar o comunicarse.'),(6,'Discapacidad de atención y aprendizaje.'),(7,'Discapacidad de autocuidado.');
/*!40000 ALTER TABLE `project_zarah_discapacidad_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_egresos`
--

DROP TABLE IF EXISTS `project_zarah_egresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_egresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  `tipo_egreso_list_id` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `observacion` varchar(100) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_egresos`
--

LOCK TABLES `project_zarah_egresos` WRITE;
/*!40000 ALTER TABLE `project_zarah_egresos` DISABLE KEYS */;
INSERT INTO `project_zarah_egresos` VALUES (2,1,6,7200,'Incluye despensa','2019-09-28 17:09:12'),(4,2,1,90,'cada 2 meses','2019-08-20 16:16:22'),(6,2,3,150,'recargas al celular, telefono de casa lo paga su hijo','2019-08-20 16:07:23'),(7,2,9,83,'lo paga su hijo (se lo presta su suegro)','2019-08-20 16:07:47'),(8,2,5,620,'gasolina/pasajes','2019-08-20 16:08:01'),(9,2,6,1650,'incluye despensa','2019-08-20 16:08:21'),(10,2,7,83,'cada año','2019-08-20 16:08:32'),(11,2,10,950,'seguro de vida, alimento para mascotas, gas','2019-08-20 16:08:53'),(12,39,1,120,'CADA BIMESTRE','2019-09-18 19:12:41'),(14,1,1,120,'Cada mes','2019-09-18 19:34:58'),(15,1,4,80,'levotiroxina','2019-09-18 19:35:30'),(17,41,1,60,'','2019-09-24 14:46:22'),(18,41,2,600,'','2019-09-24 14:46:30'),(19,41,3,400,'recargas al celular','2019-09-24 14:46:56'),(20,41,5,1200,'','2019-09-24 14:47:13'),(21,41,6,6000,'','2019-09-24 14:47:27'),(22,41,7,166,'','2019-09-24 14:47:44'),(23,41,8,400,'','2019-09-24 14:47:54'),(24,41,10,345,'gas','2019-09-24 14:48:13'),(26,1,3,300,'Recargas','2019-09-28 17:08:13'),(32,1,5,800,'Gasolina + Transporte público','2019-09-25 16:30:31'),(40,1,7,50,'Dividido al año','2019-09-25 17:15:47'),(41,1,3,529,'Internet y Teléfono','2019-09-28 17:08:32'),(42,1,2,125,'250 bimestral','2019-09-28 17:10:16'),(43,1,8,2000,'Escuela de la hermana','2019-09-28 17:11:55'),(44,1,10,1200,'Cine','2019-09-28 17:13:28'),(45,45,2,100,'','2019-09-30 15:29:19'),(46,45,1,200,'','2019-09-30 15:29:25'),(47,45,3,200,'cable y teléfono','2019-09-30 15:29:47'),(48,45,9,1000,'renta/amistad','2019-09-30 15:30:03'),(49,45,5,800,'Casi no sale, mas que a consultas (IMSS y ACREC)','2019-09-30 15:30:28'),(50,45,6,1200,'Super, al mes','2019-09-30 15:30:41'),(51,45,11,200,'','2019-09-30 15:31:42'),(52,46,2,50,'SE LA PASAN VECINOS, DA COOPERACIÓN','2019-09-30 15:56:20'),(53,46,1,120,'POR COOPERACION DE VECINOS','2019-09-30 15:56:37'),(54,46,3,30,'RECARGAS AL MES CELULAR','2019-09-30 15:56:51'),(55,46,5,320,'TAXIS','2019-09-30 15:57:03'),(56,46,6,2000,'','2019-09-30 15:57:13'),(57,46,8,400,'COLEGIATURAS, LAS HIJAS CONSIGUIERON BECA POR SACERDOTE MEDIANTE PROMEDIO','2019-09-30 15:57:44'),(58,46,4,250,'SEGURO POPULAR, INSULINA DIABETES, ESPOSO E HIJA','2019-09-30 15:58:08'),(59,46,11,395,'','2019-09-30 15:58:13'),(60,47,2,300,'PROPIA','2019-09-30 16:20:05'),(61,47,1,100,'PROPIA','2019-09-30 16:20:13'),(62,47,3,480,'SOLO INTERNET Y TELEFONO','2019-09-30 16:20:27'),(63,47,5,1000,'TAXIS Y GASOLINA','2019-09-30 16:20:52'),(64,47,6,5200,'','2019-09-30 16:21:06'),(65,47,4,60,'','2019-09-30 16:21:28'),(66,48,1,50,'LE APOYA UNA PRIMA Y E LA PASA UN VECINO','2019-09-30 16:35:47'),(67,48,2,100,'SE LA PASA UN VECINO (CUOTA DE RECUPERACIÓN)','2019-09-30 16:36:06'),(68,48,5,440,'CASI NO SALEN','2019-09-30 16:36:24'),(69,48,9,0,'ZONA EJIDAL','2019-09-30 16:36:49'),(70,48,6,0,'VECINOS LES LLEVAN ALIMENTOS','2019-09-30 16:37:03'),(71,48,7,0,'NO PUEDEN COMPRARSE','2019-09-30 16:37:14'),(72,48,4,0,'SEGURO POPULAR','2019-09-30 16:37:35'),(73,48,11,0,'USAN LEÑA','2019-09-30 16:37:44'),(74,49,1,193,'RECOLECTA DE LLUVIA Y SE LA PASAN','2019-09-30 16:51:30'),(75,49,2,240,'','2019-09-30 16:51:37'),(76,49,3,298,'','2019-09-30 16:51:49'),(77,49,9,0,'ZONA COMUNAL','2019-09-30 16:52:00'),(78,49,5,864,'PASAJES Y GASOLINA','2019-09-30 16:52:18'),(79,49,6,3200,'','2019-09-30 16:52:28'),(80,49,7,166,'','2019-09-30 16:52:41'),(81,49,8,2000,'COLEGIATURA','2019-09-30 16:52:56'),(82,49,4,784,'CUENTA CON IMSS','2019-09-30 16:53:21'),(83,49,11,100,'','2019-09-30 16:53:32'),(84,50,1,390,'','2019-09-30 17:07:06'),(85,50,2,300,'','2019-09-30 17:07:12'),(86,50,3,230,'SOLO FIJO','2019-09-30 17:07:21'),(87,50,9,0,'ZONA EJIDAL','2019-09-30 17:07:31'),(88,50,5,800,'SOLO SU HIJO ES QUIEN SALE','2019-09-30 17:07:48'),(89,50,6,4800,'','2019-09-30 17:07:58'),(90,50,7,166,'','2019-09-30 17:08:06'),(91,50,8,200,'KINDER, UTILES ESCOLARES','2019-09-30 17:08:21'),(92,50,4,80,'FARMACIAS SIMILARES','2019-09-30 17:08:39'),(93,50,11,386,'','2019-09-30 17:08:58'),(94,51,2,320,'','2019-09-30 17:52:41'),(95,51,1,160,'','2019-09-30 17:53:00'),(96,51,3,377,'Básico con Internet','2019-09-30 17:53:26'),(97,51,9,0,'zona federal','2019-09-30 17:53:55'),(98,51,5,200,'pasajes','2019-09-30 17:54:08'),(99,51,6,2800,'','2019-09-30 17:54:17'),(100,51,4,0,'del ahorro','2019-09-30 17:54:31'),(101,51,12,200,'gas','2019-09-30 17:54:42'),(102,52,2,100,'','2019-09-30 18:19:50'),(103,52,1,25,'','2019-09-30 18:19:55'),(104,52,9,1000,'RENTA','2019-09-30 18:20:09'),(105,52,3,20,'RECARGAS AL CELULAR','2019-09-30 18:20:20'),(106,52,5,85,'PASAJES','2019-09-30 18:20:30'),(107,52,6,2000,'','2019-09-30 18:20:39'),(108,52,7,0,'LE REGALAN SUS VECINOS','2019-09-30 18:20:50'),(109,52,11,0,'TIENE PARRILLA ELECTRICA','2019-09-30 18:21:14'),(110,53,2,100,'','2019-09-30 18:25:13'),(111,53,1,205,'','2019-09-30 18:25:20'),(112,53,3,450,'Internet y telefono','2019-09-30 18:25:35'),(113,53,9,33,'predial','2019-09-30 18:25:54'),(114,53,5,200,'','2019-09-30 18:26:04'),(115,53,6,2500,'','2019-09-30 18:26:12'),(116,53,7,0,'le regalan sus familiares','2019-09-30 18:26:30'),(117,53,4,100,'','2019-09-30 18:27:06'),(118,53,11,390,'','2019-09-30 18:27:30'),(119,54,2,200,'','2019-09-30 18:37:27'),(120,54,1,100,'','2019-09-30 18:37:33'),(121,54,3,100,'RECARGAS DEL CEL','2019-09-30 18:37:46'),(122,54,9,200,'PREDIAL','2019-09-30 18:37:58'),(123,54,5,300,'SOLO SALE SU ESPOSO','2019-09-30 18:38:12'),(124,54,6,3500,'INCLUYE DESPENSA','2019-09-30 18:38:34'),(125,54,7,0,'LE REGALAN SUS FAMILIARES','2019-09-30 18:38:45'),(126,54,11,200,'','2019-09-30 18:39:22'),(127,55,2,160,'','2019-09-30 18:39:57'),(128,55,1,80,'','2019-09-30 18:40:09'),(129,55,6,2200,'le invita su hermana','2019-09-30 18:40:45'),(130,55,7,0,'le regalan sus familiares','2019-09-30 18:41:01'),(131,55,4,120,'medicamentos','2019-09-30 18:41:16'),(132,55,11,200,'','2019-09-30 18:41:24'),(133,56,2,320,'','2019-09-30 18:54:55'),(134,56,1,200,'','2019-09-30 18:55:03'),(135,56,3,100,'COOPERACION EN TEL, SIN INTERNET','2019-09-30 18:55:27'),(136,56,9,800,'RENTA, AMISTAD','2019-09-30 18:55:39'),(137,56,5,640,'PASAJES','2019-09-30 18:55:52'),(138,56,6,2000,'INCLUYE DESPENSA','2019-09-30 18:56:06'),(139,56,7,208,'','2019-09-30 18:56:16'),(140,56,11,320,'','2019-09-30 18:56:23'),(141,57,2,154,'','2019-09-30 19:09:01'),(142,57,1,140,'','2019-09-30 19:09:08'),(143,57,3,0,'lo paga su hermana','2019-09-30 19:10:11'),(144,57,9,0,'le presta su hermana','2019-09-30 19:10:22'),(145,57,5,800,'pasajes','2019-09-30 19:10:35'),(146,57,6,4500,'','2019-09-30 19:10:47'),(147,57,7,166,'','2019-09-30 19:10:55'),(148,57,4,0,'similares','2019-09-30 19:11:06'),(149,57,11,800,'','2019-09-30 19:11:26'),(150,58,2,150,'','2019-09-30 19:33:14'),(151,58,1,60,'','2019-09-30 19:33:21'),(152,58,3,50,'','2019-09-30 19:33:30'),(153,58,9,0,'COMUNAL','2019-09-30 19:33:48'),(154,58,5,120,'PASAJES','2019-09-30 19:33:59'),(155,58,6,2500,'INCLUYE DESPENSA','2019-09-30 19:34:14'),(156,58,7,166,'LES REGALAN Y COMPRAN A VECES','2019-09-30 19:34:33'),(157,58,8,150,'UTILES ESCOLARES','2019-09-30 19:34:55'),(158,58,4,200,'','2019-09-30 19:35:09'),(159,58,11,200,'GAS Y LEÑA','2019-09-30 19:35:22'),(160,59,2,126,'','2019-09-30 20:12:06'),(161,59,1,124,'','2019-09-30 20:12:28'),(162,59,3,640,'INTERNET,CABLE,RECARGAS CELULARES','2019-09-30 20:13:00'),(163,59,9,30,'PREDIAL','2019-09-30 20:13:11'),(164,59,5,200,'GASOLINA','2019-09-30 20:13:25'),(165,59,6,1200,'INCLUYE DESPENSA','2019-09-30 20:13:41'),(166,59,4,90,'','2019-09-30 20:13:59'),(167,59,11,300,'','2019-09-30 20:14:07'),(168,60,3,850,'INTERNET,CABLE,RECARGAS CELULARES','2019-09-30 20:24:46'),(169,60,9,3200,'RENTA','2019-09-30 20:24:59'),(170,60,5,400,'TAXIS','2019-09-30 20:25:11'),(171,60,6,3000,'INCLUYE DESPENSA','2019-09-30 20:25:23'),(172,60,7,416,'','2019-09-30 20:25:36'),(173,60,4,700,'','2019-09-30 20:25:51'),(174,60,12,850,'GAS Y PRESTAMO','2019-09-30 20:26:10');
/*!40000 ALTER TABLE `project_zarah_egresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_enfermedades_list`
--

DROP TABLE IF EXISTS `project_zarah_enfermedades_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_enfermedades_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_enfermedades_list`
--

LOCK TABLES `project_zarah_enfermedades_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_enfermedades_list` DISABLE KEYS */;
INSERT INTO `project_zarah_enfermedades_list` VALUES (1,'Diabetes'),(2,'Hipertensión Arterial (HAS)'),(3,'Artritis'),(4,'Glaucoma'),(5,'Cáncer'),(6,'Otra'),(7,'Ninguna');
/*!40000 ALTER TABLE `project_zarah_enfermedades_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_escolaridad_list`
--

DROP TABLE IF EXISTS `project_zarah_escolaridad_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_escolaridad_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_escolaridad_list`
--

LOCK TABLES `project_zarah_escolaridad_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_escolaridad_list` DISABLE KEYS */;
INSERT INTO `project_zarah_escolaridad_list` VALUES (1,'Primaria Trunca'),(2,'Primaria Concluida'),(3,'Secundaria Trunca'),(4,'Secundaria Concluida'),(5,'Bachillerato Trunco'),(6,'Bachillerato Concluido'),(7,'Carrera técnica Trunca'),(8,'Carrera técnica Concluida'),(9,'Licenciatura Trunca'),(10,'Licenciatura Concluida'),(11,'Maestría'),(12,'Doctorado'),(13,'Posdoctorado');
/*!40000 ALTER TABLE `project_zarah_escolaridad_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estado_civil_list`
--

DROP TABLE IF EXISTS `project_zarah_estado_civil_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estado_civil_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estado_civil_list`
--

LOCK TABLES `project_zarah_estado_civil_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_estado_civil_list` DISABLE KEYS */;
INSERT INTO `project_zarah_estado_civil_list` VALUES (1,'Soltero'),(2,'Casado'),(3,'Divorciado'),(4,'Unión Libre'),(5,'Separado'),(6,'Viudo');
/*!40000 ALTER TABLE `project_zarah_estado_civil_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estado_general_vivienda_list`
--

DROP TABLE IF EXISTS `project_zarah_estado_general_vivienda_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estado_general_vivienda_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estado_general_vivienda_list`
--

LOCK TABLES `project_zarah_estado_general_vivienda_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_estado_general_vivienda_list` DISABLE KEYS */;
INSERT INTO `project_zarah_estado_general_vivienda_list` VALUES (1,'Excelente'),(2,'Buena'),(3,'Regular'),(4,'Malo');
/*!40000 ALTER TABLE `project_zarah_estado_general_vivienda_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estudio_socioeconomico`
--

DROP TABLE IF EXISTS `project_zarah_estudio_socioeconomico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estudio_socioeconomico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `sexo_list_id` int(11) DEFAULT NULL,
  `escolaridad_list_id` int(11) DEFAULT NULL,
  `estado_civil_list_id` int(11) DEFAULT NULL,
  `codigo_postal` int(11) DEFAULT NULL,
  `calle` varchar(255) DEFAULT NULL,
  `colonia` varchar(100) DEFAULT NULL,
  `entre_calles` varchar(100) DEFAULT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `telefono_fijo` varchar(20) DEFAULT NULL,
  `telefono_celular` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `apaterno` varchar(100) DEFAULT NULL,
  `amaterno` varchar(100) DEFAULT NULL,
  `nombres` varchar(100) DEFAULT NULL,
  `ocupaciones_list_id` int(11) DEFAULT NULL,
  `num_interior` varchar(45) DEFAULT NULL,
  `num_exterior` varchar(45) DEFAULT NULL,
  `numero_habitantes` int(11) DEFAULT NULL,
  `tipo_vivienda_list_id` int(11) DEFAULT NULL,
  `tipo_propiedad_list_id` int(11) DEFAULT NULL,
  `m2_terreno` int(11) DEFAULT NULL,
  `m2_construccion` int(11) DEFAULT NULL,
  `tiempo_residencia` int(11) DEFAULT NULL,
  `tipo_piso_list_id` int(11) DEFAULT NULL,
  `tipo_techo_list_id` int(11) DEFAULT NULL,
  `tipo_paredes_list_id` int(11) DEFAULT NULL,
  `higiene_list_id` int(11) DEFAULT NULL,
  `estado_general_vivienda_list_id` int(11) DEFAULT NULL,
  `discapacidad_list_id` int(11) DEFAULT NULL,
  `despensas_list_id` int(11) DEFAULT NULL,
  `beneficiario_list_id` int(11) DEFAULT NULL,
  `servicio_salud_list_id` int(11) DEFAULT NULL,
  `enfermedades_list_id` int(11) DEFAULT NULL,
  `paciente_id` int(11) DEFAULT NULL,
  `clasificacion_ts` int(11) DEFAULT NULL,
  `clasificacion_ia` int(11) DEFAULT NULL,
  `observaciones` text DEFAULT NULL,
  `diagnostico` text DEFAULT NULL,
  `falsea_datos` tinyint(1) DEFAULT NULL,
  `ref_domicilio` varchar(500) DEFAULT NULL,
  `entrenamiento` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estudio_socioeconomico`
--

LOCK TABLES `project_zarah_estudio_socioeconomico` WRITE;
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico` DISABLE KEYS */;
INSERT INTO `project_zarah_estudio_socioeconomico` VALUES (1,'CLI1605945','1995-05-10','2019-09-28 17:24:59',2,10,1,62320,'4A PRIVADA FRANCISCO VILLA','ANTONIO BARONA','EMILIANO ZAPATA Y FRANCISCO VILLA','CUERNAVACA','7779876543','7775956923','angelzuriel.barriosflores@gmail.com','BARRIOS','FLORES','ANGEL ZURIEL',5,'4','23',3,1,2,50,50,24,2,3,1,2,2,2,4,1,2,6,109450,5,NULL,'HOLIII','HOLIII',0,'POR LA CALLE, GIRAR A LA OTRA CALLE Y ENTRAR POR LA OTRA CALLE. TOCAR EN LA PUERTA.',1),(2,'CLI1305122','1948-02-28','2019-09-26 17:01:54',2,6,3,62460,'CLAVEL','SATELITE','TULIPAN Y GLADIOLA','CUERNAVACA','7775619088','7773150833','','MARTINEZ','MARTINEZ','RUFINO',2,'','19',2,1,5,800,600,35,3,1,4,1,1,NULL,4,2,3,4,77799,NULL,NULL,NULL,NULL,0,NULL,1),(41,'','1999-01-24','2019-09-26 16:56:38',1,6,1,62950,'los pinos','cuauhtemoc','allende y eufemio zapata','axochiapan','','735-169-38-54','','CORTES','MORALES','ELDA DEYSI',7,'','0',10,1,5,100,90,20,2,1,4,1,2,NULL,2,NULL,2,7,141456,2,NULL,'','',0,NULL,NULL),(43,'','1961-11-03','2019-09-26 17:33:18',1,NULL,1,NULL,'','','','','','','','SANCHEZ','RANGEL','SILVIA',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,141489,NULL,NULL,'','',0,NULL,NULL),(44,'','2003-01-15','2019-09-26 17:33:51',2,NULL,1,NULL,'','','','','','','','ARTEAGA','ALBITER','KEVIN RUBEN',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,141734,NULL,NULL,'','',0,NULL,NULL),(45,'','1928-07-30','2019-09-30 15:40:54',1,6,3,62400,'PRIV. DE LOS CIRUELOS','EL VERGEL','PATIOS DE LA ESTACION','CUERNAVACA','7773142787','','','diaz de la vega','alvarez del castillo','lucila del carmen',2,'','14',2,1,2,0,0,20,3,1,1,1,2,NULL,4,1,2,2,24676,3,NULL,'','',0,'',1),(46,'','1989-02-18','2019-09-30 16:03:54',1,4,6,62190,'RUBEN DARIO','LA CAROLINA','CENTENARIO Y LIBERTAD','CUERNAVACA','','7771998710','','HERRERA','MENDOZA','JANETTE',1,'L','105',5,1,5,50,50,30,2,6,1,2,3,NULL,1,2,1,1,136694,2,NULL,'','',0,'PORTON AMARILLO Y TALLER MECANICO, CASA AL FONDO',1),(47,'','1941-07-31','2019-09-30 16:26:21',1,1,3,62250,'AV. ESTRADA CAIJGAL','EL EMPLEADO','','CUERNAVACA','','7772451779','','ESTRADA','BAHENA','PAULINA',1,'','317',5,1,5,300,50,5,2,1,4,2,2,NULL,2,1,1,2,125633,2,NULL,'','',0,'',1),(48,'','1960-01-15','2019-09-30 16:40:38',1,2,2,62220,'VIA DEL FERROCARRIL','OCOTEPEC','PRIV. CASILLO Y LOS DUCTOS PEMEX','CUERNAVACA','','7771972040','','GONZALEZ','PAREDES','GLORIA',1,'','',2,1,1,55,55,14,2,1,4,3,4,NULL,1,1,1,1,137284,1,NULL,'','',0,'',1),(49,'','1971-05-09','2019-09-30 16:57:17',1,4,2,62300,'TRES CRUCES','OCOTEPEC','ALARCON Y LOS RAMOS','CUERNAVACA','','7773282246','','PACHECO','MARTINEZ','MA DE LOS ANGELES',1,'','18',3,1,1,130,25,25,2,1,4,2,3,NULL,2,3,2,1,136070,2,NULL,'','',0,'PASANDO UNA VIRGEN, TODO DERECHO, LA ULTIMA CASA DEL CERRO',1),(50,'','1948-08-06','2019-09-30 17:13:57',1,2,3,62100,'VICENTE GUERRERO','SANTA MARIA AHUACATITLAN','MORELOS Y CARR. MEXICO-CUERNAVACA','CUERNAVACA','7773100131','7774420843','','MILLAN','MARTINEZ','MA. FELIX',1,'','13',5,1,1,60,60,30,2,1,1,2,3,NULL,3,1,5,1,14327,4,NULL,'','',0,'',1),(51,'','1942-10-21','2019-09-30 18:03:03',1,1,1,62350,'J. GARCIA HEROE DE NACOZARI','Teopanzolco','FRENTE UNIVERSIDAD ULA','Cuernavaca','7773168313','','','AVILA','REYES','NEMESIA',3,'','',1,1,1,47,47,35,3,1,4,1,2,NULL,1,1,5,7,137109,3,NULL,'','',0,'',1),(52,'','1948-10-14','2019-09-30 18:26:45',2,6,6,62570,'1era PRIV. INDEPENDENCIA','TEJALPA','INDEPENDENCIA Y PRIV. DE INDEPENDENCIA','JIUTEPEC','','7772805362','','SALOMON','GARCIA','CALIXTO',3,'','8',2,1,2,140,75,10,2,1,1,3,3,NULL,4,1,1,NULL,26727,1,NULL,'','',0,'PUERTA NEGRA Y CASA BLANCA',1),(53,'','1979-03-13','2019-09-30 18:33:04',1,3,6,62570,'Emilio Riva Palacio','AMPLIACIÓN VICENTE GUERRERO','JESUS CASTILLO Y EL TEXCAL','jiutepec','','','','CARDENAS','RUIZ','TEODORA',3,'','10',4,1,1,198,20,0,2,1,4,2,3,NULL,1,1,1,1,128911,3,NULL,'','',0,'',1),(54,'','1959-06-17','2019-09-30 18:45:33',1,2,2,62577,'MILANO','EL PORVENIR','CONDOR Y SANTA ISABEL','JIUTEPEC','','','','ARELLANO','LUNA','JUANA ROSAURA',1,'','15',2,1,1,230,115,20,2,1,1,2,3,NULL,2,3,2,2,135481,3,NULL,'','',0,'',1),(55,'','1968-10-06','2019-09-30 18:47:17',1,2,4,62554,'PRIV. PEDREGAL','SAN LUCAS TEJALPA','TABACHINES Y PEDREGAL','JIUTEPEC','7772960451','4313514','','DIAZ','FLORES','GRISELDA',1,'','',2,1,5,200,50,10,2,1,4,3,4,NULL,1,1,1,1,135028,1,NULL,'','',0,'',1),(56,'','1964-06-22','2019-09-30 19:02:52',2,2,2,62000,'PRIV. INDEPENDENCIA','CENTRO','LEYVA Y HUMBOLT','CUERNAVACA','7773189431','7772225246','','RAMIREZ','ROMALDO','PAULINO',4,'','38',3,1,2,250,40,10,2,3,6,2,2,NULL,3,2,2,NULL,138528,3,NULL,'','',0,'',1),(57,'','1960-01-28','2019-09-30 19:15:05',1,10,3,62580,'Ejido','Temixco Centro','CERCA ZOCALO TEMIXCO','Temixco','7773251973','7771835696','','FLORES','CASPETA','INES',3,'','',2,1,5,250,100,8,3,1,1,1,2,NULL,1,NULL,NULL,NULL,38263,5,NULL,'','',0,'',1),(58,'','1946-04-24','2019-09-30 19:51:00',2,2,2,62587,'AV.GUERRERO','TETLAMA','','TEMIXCO','','7772139811','','ESPINOZA','RAMIREZ','FIDEL',1,'','',5,1,1,900,100,3,1,6,4,2,4,NULL,2,1,1,1,126227,1,NULL,'','',0,'BORDE DE CARRETERA CASA AL FONDO',1),(59,'','1934-01-07','2019-09-30 20:19:43',1,1,3,62580,'PRIV.ALVARO OBREGON','CENTRO','VENUSTIANO CARRANZA Y PRINCIPAL ALVARO OBREGON','TEMIXCO','7772418211','','','ABARCA','ROMERO','RICARDA',1,'','6',2,1,1,143,90,34,3,1,4,1,2,NULL,1,3,2,6,130259,3,NULL,'','',1,'',1),(60,'','1930-02-02','2019-09-30 20:22:34',1,4,3,62588,'AV.CONALEP','AZTECA','TELEFORO OJEDA Y PRIV.TIZOC','TEMIXCO','7774338099','','','CAMACHO','INZUNZA','ALICIA',2,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,136752,NULL,NULL,'','',NULL,'',1);
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estudio_socioeconomico_alimentos`
--

DROP TABLE IF EXISTS `project_zarah_estudio_socioeconomico_alimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estudio_socioeconomico_alimentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  `alimentos_list_id` int(11) DEFAULT NULL,
  `frecuencias_list_id` int(11) DEFAULT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=520 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estudio_socioeconomico_alimentos`
--

LOCK TABLES `project_zarah_estudio_socioeconomico_alimentos` WRITE;
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_alimentos` DISABLE KEYS */;
INSERT INTO `project_zarah_estudio_socioeconomico_alimentos` VALUES (32,2,1,2,NULL),(33,2,2,6,NULL),(34,2,3,6,NULL),(35,2,4,4,NULL),(36,2,5,1,NULL),(37,2,6,3,NULL),(38,2,7,1,NULL),(39,2,8,1,NULL),(40,2,9,1,NULL),(41,2,10,2,NULL),(42,2,11,6,NULL),(262,41,1,2,NULL),(263,41,2,3,NULL),(264,41,3,3,NULL),(265,41,4,5,NULL),(266,41,5,3,NULL),(267,41,6,2,NULL),(268,41,7,2,NULL),(269,41,8,1,NULL),(270,41,9,1,NULL),(271,41,10,2,NULL),(272,41,11,5,NULL),(354,1,1,3,''),(355,1,2,3,''),(356,1,3,3,''),(357,1,4,5,''),(358,1,5,2,''),(359,1,6,5,''),(360,1,7,4,''),(361,1,8,4,''),(362,1,9,2,''),(363,1,10,1,''),(364,45,1,2,''),(365,45,2,3,''),(366,45,3,2,''),(367,45,4,6,''),(368,45,5,1,''),(369,45,6,1,''),(370,45,7,2,''),(371,45,8,1,''),(372,45,9,1,''),(373,45,10,1,''),(374,46,1,4,''),(375,46,2,4,''),(376,46,3,4,''),(377,46,4,6,''),(378,46,5,1,''),(379,46,6,1,''),(380,46,7,1,''),(381,46,8,2,''),(382,46,9,1,''),(383,46,10,1,''),(384,47,1,3,''),(385,47,2,3,''),(386,47,3,3,''),(387,47,4,5,''),(388,47,5,1,''),(389,47,6,1,''),(390,47,7,2,''),(391,47,8,3,''),(392,47,9,3,''),(393,47,10,1,''),(394,48,1,3,''),(395,48,2,6,''),(396,48,3,6,''),(397,48,4,6,''),(398,48,5,2,''),(399,48,6,1,''),(400,48,7,2,''),(401,48,8,6,''),(402,48,9,2,''),(403,48,10,1,''),(404,49,1,2,''),(405,49,2,6,''),(406,49,3,6,''),(407,49,4,6,''),(408,49,5,1,''),(409,49,6,1,''),(410,49,7,3,''),(411,49,8,4,''),(412,49,9,1,''),(413,49,10,1,''),(414,50,1,3,''),(415,50,2,4,''),(416,50,3,4,''),(417,50,4,4,''),(418,50,5,1,''),(419,50,6,1,''),(420,50,7,2,''),(421,50,8,3,''),(422,50,9,3,''),(423,50,10,1,''),(424,51,1,3,''),(425,51,2,4,''),(426,51,3,3,''),(427,51,4,5,''),(428,51,5,6,''),(429,51,6,6,''),(430,51,7,6,''),(431,51,8,1,''),(432,51,9,1,''),(433,51,10,2,''),(434,51,11,6,''),(435,52,1,3,''),(436,52,2,3,''),(437,52,3,3,''),(438,52,4,5,''),(439,52,5,6,''),(440,52,6,1,''),(441,52,7,3,''),(442,52,8,2,''),(443,52,9,2,''),(444,52,10,1,''),(445,53,1,3,''),(446,53,2,5,''),(447,53,3,5,''),(448,53,4,5,''),(449,53,5,1,''),(450,53,6,3,''),(451,53,7,1,''),(452,53,8,1,''),(453,53,9,2,''),(454,53,10,1,''),(455,53,11,6,''),(456,54,1,3,''),(457,54,2,3,''),(458,54,3,3,''),(459,54,4,5,''),(460,54,5,1,''),(461,54,6,1,''),(462,54,7,3,''),(463,54,8,2,''),(464,54,9,2,''),(465,54,10,1,''),(466,55,1,3,''),(467,55,2,3,''),(468,55,3,5,''),(469,55,4,6,''),(470,55,5,1,''),(471,55,6,2,''),(472,55,7,1,''),(473,55,8,2,''),(474,55,9,3,''),(475,55,10,1,''),(476,55,11,6,''),(477,56,1,3,''),(478,56,2,2,''),(479,56,3,2,''),(480,56,4,5,''),(481,56,5,1,''),(482,56,6,1,''),(483,56,7,2,''),(484,56,8,2,''),(485,56,9,3,''),(486,56,10,1,''),(487,57,1,4,''),(488,57,2,5,''),(489,57,3,5,''),(490,57,4,3,''),(491,57,5,1,''),(492,57,6,5,''),(493,57,7,2,''),(494,57,8,3,''),(495,57,9,3,''),(496,57,10,1,''),(497,57,11,6,''),(498,58,1,3,''),(499,58,2,3,''),(500,58,3,3,''),(501,58,4,5,''),(502,58,5,1,''),(503,58,6,3,''),(504,58,7,2,''),(505,58,8,2,''),(506,58,9,3,''),(507,58,10,1,''),(508,58,11,6,''),(509,59,1,4,''),(510,59,2,3,''),(511,59,3,3,''),(512,59,4,5,''),(513,59,5,2,''),(514,59,6,3,''),(515,59,7,4,''),(516,59,8,2,''),(517,59,9,2,''),(518,59,10,1,''),(519,59,11,6,'');
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_alimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estudio_socioeconomico_caracteristicas`
--

DROP TABLE IF EXISTS `project_zarah_estudio_socioeconomico_caracteristicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estudio_socioeconomico_caracteristicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caracteristicas_list_id` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estudio_socioeconomico_caracteristicas`
--

LOCK TABLES `project_zarah_estudio_socioeconomico_caracteristicas` WRITE;
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_caracteristicas` DISABLE KEYS */;
INSERT INTO `project_zarah_estudio_socioeconomico_caracteristicas` VALUES (46,1,1,'2019-08-20 16:36:26',2),(47,2,1,'2019-08-20 16:36:26',2),(48,3,1,'2019-08-20 16:36:26',2),(49,4,3,'2019-08-20 16:36:26',2),(50,5,2,'2019-08-20 16:36:26',2),(51,6,1,'2019-08-20 16:36:26',2),(52,7,1,'2019-08-20 16:36:26',2),(53,8,1,'2019-08-20 16:36:26',2),(54,9,1,'2019-08-20 16:36:26',2),(100,1,0,'2019-09-24 14:51:08',41),(101,2,1,'2019-09-24 14:51:08',41),(102,3,1,'2019-09-24 14:51:08',41),(103,4,3,'2019-09-24 14:51:08',41),(104,5,1,'2019-09-24 14:51:08',41),(105,6,0,'2019-09-24 14:51:08',41),(106,7,0,'2019-09-24 14:51:08',41),(107,8,0,'2019-09-24 14:51:08',41),(108,9,0,'2019-09-24 14:51:08',41),(109,1,0,'2019-09-28 17:25:00',1),(110,2,1,'2019-09-28 17:25:00',1),(111,3,1,'2019-09-28 17:25:00',1),(112,4,3,'2019-09-28 17:25:00',1),(113,5,1,'2019-09-28 17:25:00',1),(114,6,0,'2019-09-28 17:25:00',1),(115,7,1,'2019-09-28 17:25:00',1),(116,8,0,'2019-09-28 17:25:00',1),(117,9,0,'2019-09-28 17:25:00',1),(118,1,1,'2019-09-30 15:36:30',45),(119,2,1,'2019-09-30 15:36:30',45),(120,3,1,'2019-09-30 15:36:30',45),(121,4,2,'2019-09-30 15:36:30',45),(122,5,1,'2019-09-30 15:36:30',45),(123,6,0,'2019-09-30 15:36:30',45),(124,7,0,'2019-09-30 15:36:30',45),(125,8,0,'2019-09-30 15:36:30',45),(126,9,1,'2019-09-30 15:36:30',45),(127,1,1,'2019-09-30 16:01:03',46),(128,2,1,'2019-09-30 16:01:03',46),(129,3,1,'2019-09-30 16:01:03',46),(130,4,3,'2019-09-30 16:01:03',46),(131,5,1,'2019-09-30 16:01:03',46),(132,6,0,'2019-09-30 16:01:03',46),(133,7,0,'2019-09-30 16:01:03',46),(134,8,0,'2019-09-30 16:01:03',46),(135,9,0,'2019-09-30 16:01:03',46),(136,1,1,'2019-09-30 16:23:33',47),(137,2,1,'2019-09-30 16:23:33',47),(138,3,1,'2019-09-30 16:23:33',47),(139,4,2,'2019-09-30 16:23:33',47),(140,5,1,'2019-09-30 16:23:33',47),(141,6,0,'2019-09-30 16:23:33',47),(142,7,1,'2019-09-30 16:23:33',47),(143,8,0,'2019-09-30 16:23:33',47),(144,9,0,'2019-09-30 16:23:33',47),(145,1,0,'2019-09-30 16:39:09',48),(146,2,1,'2019-09-30 16:39:09',48),(147,3,0,'2019-09-30 16:39:09',48),(148,4,1,'2019-09-30 16:39:09',48),(149,5,1,'2019-09-30 16:39:09',48),(150,6,0,'2019-09-30 16:39:09',48),(151,7,0,'2019-09-30 16:39:09',48),(152,8,0,'2019-09-30 16:39:09',48),(153,9,0,'2019-09-30 16:39:09',48),(154,1,0,'2019-09-30 16:55:50',49),(155,2,1,'2019-09-30 16:55:50',49),(156,3,1,'2019-09-30 16:55:50',49),(157,4,3,'2019-09-30 16:55:50',49),(158,5,1,'2019-09-30 16:55:50',49),(159,6,0,'2019-09-30 16:55:50',49),(160,7,0,'2019-09-30 16:55:50',49),(161,8,0,'2019-09-30 16:55:50',49),(162,9,0,'2019-09-30 16:55:50',49),(163,1,1,'2019-09-30 17:11:59',50),(164,2,1,'2019-09-30 17:11:59',50),(165,3,1,'2019-09-30 17:11:59',50),(166,4,3,'2019-09-30 17:11:59',50),(167,5,1,'2019-09-30 17:11:59',50),(168,6,0,'2019-09-30 17:11:59',50),(169,7,0,'2019-09-30 17:11:59',50),(170,8,0,'2019-09-30 17:11:59',50),(171,9,0,'2019-09-30 17:11:59',50),(172,1,1,'2019-09-30 18:00:18',51),(173,2,1,'2019-09-30 18:00:18',51),(174,3,1,'2019-09-30 18:00:18',51),(175,4,2,'2019-09-30 18:00:18',51),(176,5,2,'2019-09-30 18:00:18',51),(177,6,0,'2019-09-30 18:00:18',51),(178,7,0,'2019-09-30 18:00:18',51),(179,8,0,'2019-09-30 18:00:18',51),(180,9,0,'2019-09-30 18:00:18',51),(181,1,0,'2019-09-30 18:23:54',52),(182,2,1,'2019-09-30 18:23:54',52),(183,3,0,'2019-09-30 18:23:54',52),(184,4,1,'2019-09-30 18:23:54',52),(185,5,1,'2019-09-30 18:23:54',52),(186,6,0,'2019-09-30 18:23:54',52),(187,7,0,'2019-09-30 18:23:54',52),(188,8,0,'2019-09-30 18:23:54',52),(189,9,0,'2019-09-30 18:23:54',52),(190,1,0,'2019-09-30 18:30:53',53),(191,2,1,'2019-09-30 18:30:53',53),(192,3,1,'2019-09-30 18:30:53',53),(193,4,3,'2019-09-30 18:30:53',53),(194,5,1,'2019-09-30 18:30:53',53),(195,6,0,'2019-09-30 18:30:53',53),(196,7,0,'2019-09-30 18:30:53',53),(197,8,0,'2019-09-30 18:30:53',53),(198,9,1,'2019-09-30 18:30:53',53),(199,1,1,'2019-09-30 18:41:09',54),(200,2,1,'2019-09-30 18:41:09',54),(201,3,1,'2019-09-30 18:41:09',54),(202,4,2,'2019-09-30 18:41:09',54),(203,5,1,'2019-09-30 18:41:09',54),(204,6,0,'2019-09-30 18:41:09',54),(205,7,0,'2019-09-30 18:41:09',54),(206,8,0,'2019-09-30 18:41:09',54),(207,9,0,'2019-09-30 18:41:09',54),(218,1,1,'2019-09-30 18:44:57',55),(219,2,1,'2019-09-30 18:44:57',55),(220,3,1,'2019-09-30 18:44:57',55),(221,4,2,'2019-09-30 18:44:57',55),(222,5,1,'2019-09-30 18:44:57',55),(223,6,0,'2019-09-30 18:44:57',55),(224,7,0,'2019-09-30 18:44:57',55),(225,8,0,'2019-09-30 18:44:57',55),(226,9,1,'2019-09-30 18:44:57',55),(227,1,1,'2019-09-30 18:58:53',56),(228,2,1,'2019-09-30 18:58:53',56),(229,3,1,'2019-09-30 18:58:53',56),(230,4,2,'2019-09-30 18:58:53',56),(231,5,1,'2019-09-30 18:58:53',56),(232,6,0,'2019-09-30 18:58:53',56),(233,7,0,'2019-09-30 18:58:53',56),(234,8,0,'2019-09-30 18:58:53',56),(235,9,0,'2019-09-30 18:58:53',56),(236,1,1,'2019-09-30 19:13:44',57),(237,2,1,'2019-09-30 19:13:44',57),(238,3,1,'2019-09-30 19:13:44',57),(239,4,1,'2019-09-30 19:13:44',57),(240,5,1,'2019-09-30 19:13:44',57),(241,6,0,'2019-09-30 19:13:44',57),(242,7,0,'2019-09-30 19:13:44',57),(243,8,0,'2019-09-30 19:13:44',57),(244,9,0,'2019-09-30 19:13:44',57),(245,1,0,'2019-09-30 19:48:55',58),(246,2,1,'2019-09-30 19:48:55',58),(247,3,1,'2019-09-30 19:48:56',58),(248,4,3,'2019-09-30 19:48:56',58),(249,5,1,'2019-09-30 19:48:56',58),(250,6,0,'2019-09-30 19:48:56',58),(251,7,0,'2019-09-30 19:48:56',58),(252,8,0,'2019-09-30 19:48:56',58),(253,9,0,'2019-09-30 19:48:56',58),(254,1,0,'2019-09-30 20:15:57',59),(255,2,1,'2019-09-30 20:15:57',59),(256,3,1,'2019-09-30 20:15:57',59),(257,4,2,'2019-09-30 20:15:57',59),(258,5,1,'2019-09-30 20:15:57',59),(259,6,0,'2019-09-30 20:15:57',59),(260,7,0,'2019-09-30 20:15:57',59),(261,8,0,'2019-09-30 20:15:57',59),(262,9,0,'2019-09-30 20:15:57',59);
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_caracteristicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estudio_socioeconomico_manaje`
--

DROP TABLE IF EXISTS `project_zarah_estudio_socioeconomico_manaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estudio_socioeconomico_manaje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manaje_list_id` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=383 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estudio_socioeconomico_manaje`
--

LOCK TABLES `project_zarah_estudio_socioeconomico_manaje` WRITE;
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_manaje` DISABLE KEYS */;
INSERT INTO `project_zarah_estudio_socioeconomico_manaje` VALUES (56,1,2,2),(57,2,0,2),(58,3,0,2),(59,4,1,2),(60,5,1,2),(61,6,0,2),(62,7,1,2),(63,8,1,2),(64,9,1,2),(65,10,0,2),(66,11,0,2),(67,12,1,2),(68,13,1,2),(134,1,1,41),(135,2,0,41),(136,3,1,41),(137,4,1,41),(138,5,0,41),(139,6,0,41),(140,7,0,41),(141,8,0,41),(142,9,0,41),(143,10,0,41),(144,11,0,41),(145,12,1,41),(146,13,1,41),(147,1,2,1),(148,2,0,1),(149,3,2,1),(150,4,1,1),(151,5,1,1),(152,6,0,1),(153,7,0,1),(154,8,1,1),(155,9,2,1),(156,10,0,1),(157,11,1,1),(158,12,1,1),(159,13,1,1),(160,1,1,45),(161,2,0,45),(162,3,0,45),(163,4,1,45),(164,5,0,45),(165,6,0,45),(166,7,0,45),(167,8,1,45),(168,9,0,45),(169,10,0,45),(170,11,0,45),(171,12,1,45),(172,13,1,45),(173,1,1,46),(174,2,1,46),(175,3,0,46),(176,4,1,46),(177,5,0,46),(178,6,0,46),(179,7,0,46),(180,8,1,46),(181,9,0,46),(182,10,0,46),(183,11,0,46),(184,12,1,46),(185,13,1,46),(186,1,1,47),(187,2,1,47),(188,3,0,47),(189,4,1,47),(190,5,1,47),(191,6,0,47),(192,7,1,47),(193,8,0,47),(194,9,0,47),(195,10,0,47),(196,11,0,47),(197,12,1,47),(198,13,1,47),(199,1,1,48),(200,2,0,48),(201,3,0,48),(202,4,0,48),(203,5,0,48),(204,6,0,48),(205,7,0,48),(206,8,0,48),(207,9,0,48),(208,10,0,48),(209,11,0,48),(210,12,0,48),(211,13,0,48),(212,1,1,49),(213,2,0,49),(214,3,0,49),(215,4,1,49),(216,5,0,49),(217,6,0,49),(218,7,0,49),(219,8,1,49),(220,9,0,49),(221,10,0,49),(222,11,0,49),(223,12,1,49),(224,13,1,49),(225,1,1,50),(226,2,0,50),(227,3,0,50),(228,4,1,50),(229,5,0,50),(230,6,0,50),(231,7,0,50),(232,8,0,50),(233,9,0,50),(234,10,0,50),(235,11,0,50),(236,12,1,50),(237,13,1,50),(240,1,1,51),(241,2,0,51),(242,3,0,51),(243,4,1,51),(244,5,0,51),(245,6,0,51),(246,7,0,51),(247,8,1,51),(248,9,0,51),(249,10,0,51),(250,11,0,51),(251,12,1,51),(252,13,1,51),(253,1,0,52),(254,2,0,52),(255,3,0,52),(256,4,1,52),(257,5,0,52),(258,6,0,52),(259,7,0,52),(260,8,0,52),(261,9,0,52),(262,10,0,52),(263,11,0,52),(264,12,0,52),(265,13,0,52),(266,1,1,53),(267,2,0,53),(268,3,0,53),(269,4,1,53),(270,5,0,53),(271,6,0,53),(272,7,0,53),(273,8,1,53),(274,9,0,53),(275,10,0,53),(276,11,0,53),(277,12,1,53),(278,13,1,53),(279,1,1,54),(280,2,1,54),(281,3,1,54),(282,4,1,54),(283,5,0,54),(284,6,0,54),(285,7,1,54),(286,8,1,54),(287,9,0,54),(288,10,0,54),(289,11,0,54),(290,12,1,54),(291,13,1,54),(318,1,1,55),(319,2,0,55),(320,3,0,55),(321,4,1,55),(322,5,0,55),(323,6,0,55),(324,7,0,55),(325,8,0,55),(326,9,0,55),(327,10,0,55),(328,11,0,55),(329,12,1,55),(330,13,1,55),(331,1,1,56),(332,2,0,56),(333,3,0,56),(334,4,1,56),(335,5,0,56),(336,6,0,56),(337,7,0,56),(338,8,0,56),(339,9,0,56),(340,10,0,56),(341,11,0,56),(342,12,1,56),(343,13,1,56),(344,1,1,57),(345,2,0,57),(346,3,0,57),(347,4,1,57),(348,5,1,57),(349,6,0,57),(350,7,0,57),(351,8,1,57),(352,9,0,57),(353,10,0,57),(354,11,0,57),(355,12,1,57),(356,13,1,57),(357,1,0,58),(358,2,0,58),(359,3,0,58),(360,4,1,58),(361,5,0,58),(362,6,0,58),(363,7,0,58),(364,8,1,58),(365,9,0,58),(366,10,0,58),(367,11,0,58),(368,12,1,58),(369,13,1,58),(370,1,1,59),(371,2,0,59),(372,3,0,59),(373,4,1,59),(374,5,1,59),(375,6,0,59),(376,7,0,59),(377,8,1,59),(378,9,0,59),(379,10,0,59),(380,11,0,59),(381,12,1,59),(382,13,1,59);
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_manaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estudio_socioeconomico_servicios`
--

DROP TABLE IF EXISTS `project_zarah_estudio_socioeconomico_servicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estudio_socioeconomico_servicios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servicios_list_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=280 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estudio_socioeconomico_servicios`
--

LOCK TABLES `project_zarah_estudio_socioeconomico_servicios` WRITE;
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_servicios` DISABLE KEYS */;
INSERT INTO `project_zarah_estudio_socioeconomico_servicios` VALUES (79,1,'2019-08-20 16:36:26',2),(80,2,'2019-08-20 16:36:26',2),(81,4,'2019-08-20 16:36:26',2),(82,5,'2019-08-20 16:36:26',2),(83,6,'2019-08-20 16:36:26',2),(84,7,'2019-08-20 16:36:26',2),(85,8,'2019-08-20 16:36:26',2),(86,12,'2019-08-20 16:36:26',2),(132,1,'2019-09-24 14:51:08',41),(133,2,'2019-09-24 14:51:08',41),(134,4,'2019-09-24 14:51:08',41),(135,5,'2019-09-24 14:51:08',41),(136,7,'2019-09-24 14:51:08',41),(137,8,'2019-09-24 14:51:08',41),(138,1,'2019-09-28 17:24:59',1),(139,2,'2019-09-28 17:24:59',1),(140,3,'2019-09-28 17:24:59',1),(141,4,'2019-09-28 17:24:59',1),(142,5,'2019-09-28 17:24:59',1),(143,6,'2019-09-28 17:24:59',1),(144,7,'2019-09-28 17:24:59',1),(145,8,'2019-09-28 17:24:59',1),(146,12,'2019-09-28 17:24:59',1),(147,1,'2019-09-30 15:36:30',45),(148,2,'2019-09-30 15:36:30',45),(149,3,'2019-09-30 15:36:30',45),(150,4,'2019-09-30 15:36:30',45),(151,5,'2019-09-30 15:36:30',45),(152,6,'2019-09-30 15:36:30',45),(153,9,'2019-09-30 15:36:30',45),(154,11,'2019-09-30 15:36:30',45),(155,1,'2019-09-30 16:01:03',46),(156,2,'2019-09-30 16:01:03',46),(157,4,'2019-09-30 16:01:03',46),(158,5,'2019-09-30 16:01:03',46),(159,7,'2019-09-30 16:01:03',46),(160,8,'2019-09-30 16:01:03',46),(161,1,'2019-09-30 16:23:33',47),(162,2,'2019-09-30 16:23:33',47),(163,3,'2019-09-30 16:23:33',47),(164,4,'2019-09-30 16:23:33',47),(165,5,'2019-09-30 16:23:33',47),(166,6,'2019-09-30 16:23:33',47),(167,7,'2019-09-30 16:23:33',47),(168,9,'2019-09-30 16:23:33',47),(169,1,'2019-09-30 16:39:09',48),(170,2,'2019-09-30 16:39:09',48),(171,4,'2019-09-30 16:39:09',48),(172,1,'2019-09-30 16:55:50',49),(173,2,'2019-09-30 16:55:50',49),(174,5,'2019-09-30 16:55:50',49),(175,6,'2019-09-30 16:55:50',49),(176,1,'2019-09-30 17:11:59',50),(177,2,'2019-09-30 17:11:59',50),(178,4,'2019-09-30 17:11:59',50),(179,5,'2019-09-30 17:11:59',50),(180,6,'2019-09-30 17:11:59',50),(181,8,'2019-09-30 17:11:59',50),(182,12,'2019-09-30 17:11:59',50),(201,1,'2019-09-30 18:00:18',51),(202,2,'2019-09-30 18:00:18',51),(203,3,'2019-09-30 18:00:18',51),(204,4,'2019-09-30 18:00:18',51),(205,5,'2019-09-30 18:00:18',51),(206,6,'2019-09-30 18:00:18',51),(207,9,'2019-09-30 18:00:18',51),(208,11,'2019-09-30 18:00:18',51),(209,12,'2019-09-30 18:00:18',51),(210,1,'2019-09-30 18:23:53',52),(211,2,'2019-09-30 18:23:53',52),(212,4,'2019-09-30 18:23:53',52),(213,5,'2019-09-30 18:23:54',52),(214,7,'2019-09-30 18:23:54',52),(215,12,'2019-09-30 18:23:54',52),(216,1,'2019-09-30 18:30:53',53),(217,2,'2019-09-30 18:30:53',53),(218,3,'2019-09-30 18:30:53',53),(219,4,'2019-09-30 18:30:53',53),(220,5,'2019-09-30 18:30:53',53),(221,6,'2019-09-30 18:30:53',53),(222,7,'2019-09-30 18:30:53',53),(223,9,'2019-09-30 18:30:53',53),(224,11,'2019-09-30 18:30:53',53),(225,12,'2019-09-30 18:30:53',53),(226,1,'2019-09-30 18:41:08',54),(227,2,'2019-09-30 18:41:08',54),(228,4,'2019-09-30 18:41:08',54),(229,5,'2019-09-30 18:41:08',54),(230,7,'2019-09-30 18:41:08',54),(231,12,'2019-09-30 18:41:08',54),(244,1,'2019-09-30 18:44:57',55),(245,2,'2019-09-30 18:44:57',55),(246,4,'2019-09-30 18:44:57',55),(247,5,'2019-09-30 18:44:57',55),(248,9,'2019-09-30 18:44:57',55),(249,12,'2019-09-30 18:44:57',55),(250,1,'2019-09-30 18:58:53',56),(251,2,'2019-09-30 18:58:53',56),(252,4,'2019-09-30 18:58:53',56),(253,5,'2019-09-30 18:58:53',56),(254,6,'2019-09-30 18:58:53',56),(255,7,'2019-09-30 18:58:53',56),(256,9,'2019-09-30 18:58:53',56),(257,12,'2019-09-30 18:58:53',56),(258,1,'2019-09-30 19:13:44',57),(259,2,'2019-09-30 19:13:44',57),(260,3,'2019-09-30 19:13:44',57),(261,4,'2019-09-30 19:13:44',57),(262,5,'2019-09-30 19:13:44',57),(263,6,'2019-09-30 19:13:44',57),(264,11,'2019-09-30 19:13:44',57),(265,12,'2019-09-30 19:13:44',57),(266,1,'2019-09-30 19:48:55',58),(267,2,'2019-09-30 19:48:55',58),(268,5,'2019-09-30 19:48:55',58),(269,7,'2019-09-30 19:48:55',58),(270,11,'2019-09-30 19:48:55',58),(271,12,'2019-09-30 19:48:55',58),(272,1,'2019-09-30 20:15:57',59),(273,2,'2019-09-30 20:15:57',59),(274,3,'2019-09-30 20:15:57',59),(275,4,'2019-09-30 20:15:57',59),(276,5,'2019-09-30 20:15:57',59),(277,6,'2019-09-30 20:15:57',59),(278,7,'2019-09-30 20:15:57',59),(279,11,'2019-09-30 20:15:57',59);
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_servicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_frecuencias_list`
--

DROP TABLE IF EXISTS `project_zarah_frecuencias_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_frecuencias_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_frecuencias_list`
--

LOCK TABLES `project_zarah_frecuencias_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_frecuencias_list` DISABLE KEYS */;
INSERT INTO `project_zarah_frecuencias_list` VALUES (1,'Diario'),(2,'Cada tercer día'),(3,'Una vez por semana'),(4,'Una vez al mes'),(5,'Ocasionalmente'),(6,'Nunca');
/*!40000 ALTER TABLE `project_zarah_frecuencias_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_higiene_list`
--

DROP TABLE IF EXISTS `project_zarah_higiene_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_higiene_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_higiene_list`
--

LOCK TABLES `project_zarah_higiene_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_higiene_list` DISABLE KEYS */;
INSERT INTO `project_zarah_higiene_list` VALUES (1,'Buena'),(2,'Regular'),(3,'Mala');
/*!40000 ALTER TABLE `project_zarah_higiene_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_ingresos`
--

DROP TABLE IF EXISTS `project_zarah_ingresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_ingresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `ingreso` double DEFAULT NULL,
  `ocupaciones_list_id` int(11) DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `ingresos_tipo_list_id` int(11) DEFAULT NULL,
  `parentesco_list_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_ingresos`
--

LOCK TABLES `project_zarah_ingresos` WRITE;
/*!40000 ALTER TABLE `project_zarah_ingresos` DISABLE KEYS */;
INSERT INTO `project_zarah_ingresos` VALUES (11,'ISABEL BARRIOS FLORES',24,6000,5,1,'2019-09-28 17:07:14',1,43),(17,'YARENI YADIRA MARTINEZ SANCHEZ',36,1000,12,2,'2019-08-16 16:41:32',2,5),(18,'NIDIA MARTINEZ SANCHEZ',39,1000,6,2,'2019-08-16 16:41:54',2,5),(19,'PENSIÓN',0,3500,NULL,2,'2019-08-16 16:42:52',6,NULL),(20,'60+',0,1250,NULL,2,'2019-08-16 16:43:16',4,NULL),(22,'BARRIOS FLORES ANGEL ZURIEL',24,9000,5,1,'2019-09-28 17:07:31',6,45),(23,'',49,2000,10,41,'2019-09-24 14:43:49',1,1),(24,'',51,2400,NULL,41,'2019-09-24 14:44:06',NULL,2),(25,'',32,2400,NULL,41,'2019-09-24 14:44:27',NULL,3),(26,'',26,2000,NULL,41,'2019-09-24 14:44:41',NULL,NULL),(27,'',20,2400,NULL,41,'2019-09-24 14:45:47',5,NULL),(31,'MARIA DEL CARMEN SUSANA DOMINGUEZ BENITEZ',78,400,3,45,'2019-09-30 15:22:22',3,23),(32,'DIAZ DE LA VEGA ALVAREZ DEL CASTILLO LUCILA DEL CARMEN',90,3000,2,45,'2019-09-30 15:25:09',6,NULL),(33,'ALICIA MENDOZA MONTES DE OCA',51,2400,3,46,'2019-09-30 15:52:38',1,43),(34,'GERARDO FUENTES',57,4800,4,47,'2019-09-30 16:19:25',1,40),(35,'ISAAC FUENTES MARQUINA',32,3600,NULL,47,'2019-09-30 16:19:45',NULL,30),(36,'MARIO GONZALEZ BAHENA',63,1200,4,48,'2019-09-30 16:35:18',1,2),(37,'SAMUEL AUSTASIO ARELLANO',54,1000,3,49,'2019-09-30 16:50:23',1,2),(38,'CLAUDIA EDITH ARELLANO PACHECO',26,500,5,49,'2019-09-30 16:50:53',2,3),(39,'JOEL CRISTOBAL CARDOSO MILLAN',49,3400,3,50,'2019-09-30 17:05:57',1,4),(40,'EMMA SANDOVAL RIVAS',44,800,3,50,'2019-09-30 17:06:24',1,39),(41,'MA. FELIX MILLAN MARTINEZ',70,800,1,50,'2019-09-30 17:06:50',6,NULL),(42,'Avila Reyes Nemesia',76,2800,3,51,'2019-09-30 17:49:01',1,45),(43,'Avila Mejia Jose',55,1280,5,51,'2019-09-30 17:49:48',2,4),(44,'60+',0,1250,NULL,51,'2019-09-30 17:51:54',4,45),(45,'ALMA DELIA SALOMON',42,2000,3,52,'2019-09-30 18:18:36',2,3),(46,'CALIXTO SALOMON GARCIA',71,300,3,52,'2019-09-30 18:19:09',6,NULL),(47,'60+',0,1250,NULL,52,'2019-09-30 18:19:30',4,NULL),(48,'Teodora Cardenas Ruiz',40,800,3,53,'2019-09-30 18:23:08',1,45),(49,'Juan Daniel Zagal Cardenas',18,3600,4,53,'2019-09-30 18:23:35',1,4),(50,'Esperanza Jaqueline Zagal Cardenas ',20,500,3,53,'2019-09-30 18:24:49',1,3),(51,'ARTURO GARCIA HERNANDEZ',68,4000,4,54,'2019-09-30 18:35:55',1,2),(52,'ARTURO GARCIA HERNANDEZ   60+',68,1250,NULL,54,'2019-09-30 18:36:27',4,NULL),(53,'Hector Velazquez',50,2000,3,55,'2019-09-30 18:39:48',1,20),(54,'Diaz Flores Eduviges',48,800,3,55,'2019-09-30 18:39:23',2,5),(55,'RAMIREZ ROMALDO PAULINO',55,4800,4,56,'2019-09-30 18:54:11',6,NULL),(56,'GABRIELA TAPIA SANTIAGO',60,2150,2,56,'2019-09-30 18:54:36',1,1),(57,'Margarita Caspeta',79,1000,3,57,'2019-09-30 18:55:29',1,43),(58,'Flores Caspeta Ines',59,1000,3,57,'2019-09-30 18:55:58',1,NULL),(59,'Armando Flores Caspeta',52,5000,6,57,'2019-09-30 18:56:28',2,6),(60,'65+',0,1250,NULL,57,'2019-09-30 18:57:11',4,43),(61,'GLORIA GARCIA PEREZ ',66,2000,3,58,'2019-09-30 19:31:59',1,1),(62,'ANA LAURA ESPINOZA GARCIA',26,1200,NULL,58,'2019-09-30 19:32:27',1,3),(63,'60+',66,1250,NULL,58,'2019-09-30 19:32:57',4,1),(64,'FABIOLA HERNANDEZ ABARCA',50,1500,6,59,'2019-09-30 20:11:19',2,3),(65,'60+',0,1275,NULL,59,'2019-09-30 20:11:43',4,NULL),(66,'JORGE DORANTES CAMACHO',61,7000,6,60,'2019-09-30 20:23:35',1,4),(67,'CAMACHO INZUNZA ALICIA',89,2000,2,60,'2019-09-30 20:24:13',1,NULL);
/*!40000 ALTER TABLE `project_zarah_ingresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_ingresos_tipo_list`
--

DROP TABLE IF EXISTS `project_zarah_ingresos_tipo_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_ingresos_tipo_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_ingresos_tipo_list`
--

LOCK TABLES `project_zarah_ingresos_tipo_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_ingresos_tipo_list` DISABLE KEYS */;
INSERT INTO `project_zarah_ingresos_tipo_list` VALUES (1,'Familiar'),(2,'Familiar Externo'),(3,'Otro Externo'),(4,'Gubernamental'),(5,'Estudiante Beca'),(6,'Propio');
/*!40000 ALTER TABLE `project_zarah_ingresos_tipo_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_institucion_academica_list`
--

DROP TABLE IF EXISTS `project_zarah_institucion_academica_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_institucion_academica_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_institucion_academica_list`
--

LOCK TABLES `project_zarah_institucion_academica_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_institucion_academica_list` DISABLE KEYS */;
INSERT INTO `project_zarah_institucion_academica_list` VALUES (1,'Pública'),(2,'Privada');
/*!40000 ALTER TABLE `project_zarah_institucion_academica_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_integrantes_hogar`
--

DROP TABLE IF EXISTS `project_zarah_integrantes_hogar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_integrantes_hogar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `dependiente_list_id` int(11) DEFAULT NULL,
  `parentesco_list_id` int(11) DEFAULT NULL,
  `institucion_academica_list_id` int(11) DEFAULT NULL,
  `ocupaciones_list_id` int(11) DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `discapacidad_list_id` int(11) DEFAULT NULL,
  `trabajo` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_integrantes_hogar`
--

LOCK TABLES `project_zarah_integrantes_hogar` WRITE;
/*!40000 ALTER TABLE `project_zarah_integrantes_hogar` DISABLE KEYS */;
INSERT INTO `project_zarah_integrantes_hogar` VALUES (9,'ISABEL BARRIOS FLORES',49,2,43,1,5,1,'2019-09-25 17:59:33',NULL,''),(12,'YAZMIN ALEXANDRA BARRIOS FLORES',16,1,5,1,7,1,'2019-09-25 17:59:38',NULL,''),(17,'ORLANDO MARTINEZ MARTINEZ',37,2,5,1,12,2,'2019-08-16 16:40:15',NULL,NULL),(18,'',49,2,1,NULL,10,41,'2019-09-24 14:43:09',NULL,NULL),(19,'',51,2,2,NULL,10,41,'2019-09-24 14:34:45',NULL,NULL),(20,'',22,1,3,1,16,41,'2019-09-24 14:35:17',NULL,NULL),(21,'',18,1,3,1,16,41,'2019-09-24 14:35:39',NULL,NULL),(23,'',26,2,3,1,10,41,'2019-09-24 14:36:26',NULL,NULL),(24,'',7,1,3,1,16,41,'2019-09-24 14:36:42',NULL,NULL),(26,'',40,1,6,NULL,8,41,'2019-09-24 14:40:47',NULL,NULL),(27,'',32,2,3,NULL,10,41,'2019-09-24 14:41:23',NULL,NULL),(28,'',74,1,6,NULL,6,41,'2019-09-24 14:42:54',NULL,NULL),(29,'MARIA DEL CARMEN DOMINGUEZ BENITEZ',78,2,23,1,3,45,'2019-09-30 15:21:34',NULL,'CUIDADORA'),(30,'HUMBERTO HERRERA VARGAS',51,2,44,1,3,46,'2019-09-30 15:48:36',NULL,'INCAPACIDAD, USO DE MULETAS'),(31,'JESSICA JANNA H. MENDOZA',17,2,5,2,7,46,'2019-09-30 15:49:06',NULL,'PREPARATORIA DON BOSCO'),(32,'JENNIFER J. HERNANDEZ MENDOZA',15,2,5,2,7,46,'2019-09-30 15:49:31',NULL,'PREPARATORIA DON BOSCO'),(33,'ALICIA MENDOZA MONTES DE OCA',51,1,43,1,3,46,'2019-09-30 15:50:08',NULL,'EMPLEADA DOMÉSTICA Y PLANCHA'),(34,'REYNA MARQUINA ESTRADA',54,2,3,1,1,47,'2019-09-30 16:15:56',NULL,'HOGAR'),(35,'GERARDO FUENTES',57,1,40,1,4,47,'2019-09-30 16:16:30',NULL,'ALBAÑIL'),(36,'MONICA ANDREA GONZALEZ B.',23,2,45,1,1,47,'2019-09-30 16:17:16',NULL,'HOGAR'),(37,'ISAAC FUENTES MARQUINA',32,1,30,1,4,47,'2019-09-30 16:17:56',NULL,'ALBAÑIL'),(38,'MARIO GONZALEZ BAHENA',63,1,2,1,4,48,'2019-09-30 16:34:47',NULL,'BARNIZADOR EVENTUAL, BARRE'),(39,'SAMUEL EUSTASIO ARELLANO',54,1,2,1,3,49,'2019-09-30 16:47:11',NULL,'VENDE ALGODON DE AZUCAR'),(40,'CLAUDIA EDITH ARELLANO PACHECO',26,1,3,1,5,49,'2019-09-30 16:47:46',NULL,'EMFERMERA (IMSS)'),(41,'IVONNE E. ARELLANO PACHECO',22,2,3,2,7,49,'2019-09-30 16:48:12',NULL,'ESTUDIANTE CON BECA'),(42,'EDITH ESTHER MOLINA ARELLANO',1,2,29,NULL,NULL,49,'2019-09-30 16:48:39',NULL,''),(43,'JOEL CRISTOBAL CARDOSO M.',49,1,4,1,4,50,'2019-09-30 17:03:33',NULL,'MECANICO AYUDANTE'),(44,'EMMA SANDOVAL RIVAS',44,2,39,1,1,50,'2019-09-30 17:04:25',NULL,'TIANGUIS CHAMILPA'),(45,'LUCERO CARDOSO SANDOVAL',24,2,29,1,3,50,'2019-09-30 17:04:55',NULL,'TIENDA DE ABARROTES'),(46,'MIA CARDOSO SANDOVAL',5,2,31,1,7,50,'2019-09-30 17:05:21',NULL,'KINDER'),(47,'JULIO CESAR SALOMON C.',21,2,30,NULL,1,52,'2019-09-30 18:18:06',3,'NO ESTUDIA'),(48,'Esperanza Jaqueline Zagal Cardenas ',20,1,3,NULL,3,53,'2019-09-30 18:24:04',NULL,'Venta de bazar papeleria'),(49,'Elian Zagal Cardenas',0,1,30,NULL,NULL,53,'2019-09-30 18:21:48',NULL,''),(50,'Juan Daniel Zagal Cardenas',18,1,4,NULL,4,53,'2019-09-30 18:22:30',NULL,'Chalan de Electricidad'),(51,'ARTURO GARCIA HERNANDEZ',68,1,2,1,4,54,'2019-09-30 18:34:47',NULL,'AUDANTE DE ALBAÑIL'),(52,'Hector Velazquez',50,1,2,NULL,3,55,'2019-09-30 18:38:26',NULL,'Viene viene'),(53,'GABRIELA TAPIA SANTIAGO',60,2,1,1,1,56,'2019-09-30 18:52:18',NULL,'AMA DE CASA'),(54,'RICARDO RAFAEL R. TAPIA',15,2,4,1,7,56,'2019-09-30 18:52:44',NULL,'ESTUDIANTE DE PREPA'),(55,'Margarita Caspeta',79,1,43,NULL,3,57,'2019-09-30 18:54:58',NULL,''),(56,'GLORIA GARCIA PEREZ',66,1,1,NULL,3,58,'2019-09-30 19:28:34',NULL,'VENTA DE VERDURAS'),(57,'ALVARO ESPINOZA GARCIA',39,2,4,NULL,1,58,'2019-09-30 19:29:26',NULL,'HOGAR'),(58,'ANA LAURA ESPINOZA GARCIA',26,1,3,NULL,3,58,'2019-09-30 19:30:52',NULL,'VENTA DE VERDURAS'),(59,'ANA LUZ ESPINOZA GARCIA',9,2,29,1,7,58,'2019-09-30 19:31:30',NULL,''),(60,'FABIO GERARDO HERNANDEZ ABARCA',54,1,4,NULL,1,59,'2019-09-30 20:10:49',NULL,'CONTADOR CON MAS DE 1 AÑO DESEMPLEADO'),(61,'JORGE DORANTES CAMACHO',61,1,4,1,6,60,'2019-09-30 20:23:13',NULL,'DOCENTE CONALEP');
/*!40000 ALTER TABLE `project_zarah_integrantes_hogar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_manaje_list`
--

DROP TABLE IF EXISTS `project_zarah_manaje_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_manaje_list` (
  `id` int(11) NOT NULL,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_manaje_list`
--

LOCK TABLES `project_zarah_manaje_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_manaje_list` DISABLE KEYS */;
INSERT INTO `project_zarah_manaje_list` VALUES (1,'TELEVISIÓN O PLASMA'),(2,'REPRODUCTOR DE DVD O BLU RAY'),(3,'EQUIPO DE SONIDO'),(4,'REFRIGERADOR'),(5,'HORNO DE MICROONDAS'),(6,'LAVAVAJILLAS'),(7,'CAFETERA'),(8,'LAVADORA'),(9,'COMPUTADORA'),(10,'CENTRO DE LAVADO'),(11,'JUEGOS DE VIDEO'),(12,'ESTUFA'),(13,'LICUADORA');
/*!40000 ALTER TABLE `project_zarah_manaje_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_model_def`
--

DROP TABLE IF EXISTS `project_zarah_model_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_model_def` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_optimizer` varchar(100) DEFAULT NULL,
  `model_loss` varchar(100) DEFAULT NULL,
  `model_trained` tinyint(1) DEFAULT NULL,
  `model_name` varchar(45) DEFAULT NULL,
  `model_error` double DEFAULT NULL,
  `model_acc` double DEFAULT NULL,
  `model_dir` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_model_def`
--

LOCK TABLES `project_zarah_model_def` WRITE;
/*!40000 ALTER TABLE `project_zarah_model_def` DISABLE KEYS */;
INSERT INTO `project_zarah_model_def` VALUES (1,'sgd','mse',1,'project_zarah',0.005112275015562773,0.9948877249844372,'project_zarah/cp.ckpt'),(2,NULL,NULL,0,'zarah_speech',0,0,'project_zarah/speech.ckpt');
/*!40000 ALTER TABLE `project_zarah_model_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_model_layers`
--

DROP TABLE IF EXISTS `project_zarah_model_layers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_model_layers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layer_units` int(11) DEFAULT NULL,
  `layer_activation` varchar(100) DEFAULT NULL,
  `layer_model_def_id` int(11) DEFAULT NULL,
  `layer_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_model_layers`
--

LOCK TABLES `project_zarah_model_layers` WRITE;
/*!40000 ALTER TABLE `project_zarah_model_layers` DISABLE KEYS */;
INSERT INTO `project_zarah_model_layers` VALUES (28,200,'relu',1,NULL),(29,200,'relu',1,NULL),(30,1,'sigmoid',1,NULL);
/*!40000 ALTER TABLE `project_zarah_model_layers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_ocupaciones_list`
--

DROP TABLE IF EXISTS `project_zarah_ocupaciones_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_ocupaciones_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(100) DEFAULT NULL,
  `observacion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_ocupaciones_list`
--

LOCK TABLES `project_zarah_ocupaciones_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_ocupaciones_list` DISABLE KEYS */;
INSERT INTO `project_zarah_ocupaciones_list` VALUES (1,'Desempleado (Más de 3 meses sin trabajo) / Adultos con programas de gobierno',NULL),(2,'Jubilado / Pensionado',NULL),(3,'Subempleado',NULL),(4,'Obrero',NULL),(5,'Empleados (Asalariado y Técnico)','Solo menores de 5 años'),(6,'Profesionista, empresario o ejecutivo',NULL),(7,'Estudiante','Solo menores de 16 años que no trabajan ni estudian');
/*!40000 ALTER TABLE `project_zarah_ocupaciones_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_parentesco_list`
--

DROP TABLE IF EXISTS `project_zarah_parentesco_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_parentesco_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_parentesco_list`
--

LOCK TABLES `project_zarah_parentesco_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_parentesco_list` DISABLE KEYS */;
INSERT INTO `project_zarah_parentesco_list` VALUES (1,'Esposa'),(2,'Esposo'),(3,'Hija'),(4,'Hijo'),(5,'Hermana'),(6,'Hermano'),(7,'Prima'),(8,'Primo'),(9,'Abuela'),(10,'Abuelo'),(11,'Tía'),(12,'Tío'),(13,'Sobrina'),(14,'Sobrino'),(15,'Madrastra'),(16,'Padrastro'),(17,'Hermanastra'),(18,'Hermanastro'),(19,'Concubina'),(20,'Concubino'),(21,'Cuñada'),(22,'Cuñado'),(23,'Amiga'),(24,'Amigo'),(25,'Bisabuela'),(26,'Bisabuelo'),(27,'Concuña'),(28,'Concuño'),(29,'Nieta'),(30,'Nieto'),(31,'Bisnieta'),(32,'Bisnieto'),(33,'Tataranieta'),(34,'Tataranieto'),(35,'Tatarabuela'),(36,'Tatarabuelo'),(37,'Hija Adoptiva'),(38,'Hijo Adoptivo'),(39,'Nuera'),(40,'Yerno'),(41,'Suegra'),(42,'Suegro'),(43,'Mamá'),(44,'Papá'),(45,'Otro');
/*!40000 ALTER TABLE `project_zarah_parentesco_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_servicio_salud_list`
--

DROP TABLE IF EXISTS `project_zarah_servicio_salud_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_servicio_salud_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_servicio_salud_list`
--

LOCK TABLES `project_zarah_servicio_salud_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_servicio_salud_list` DISABLE KEYS */;
INSERT INTO `project_zarah_servicio_salud_list` VALUES (1,'Seguro popular'),(2,'IMSS-ISSSTE'),(3,'PEMEX, SEDENA o particulares'),(4,'Seguro de Gastos Médicos'),(5,'Farmacias'),(6,'Sin acceso (literalmente no puede atenderse)'),(7,'Otros (Curandero, homeopatía)');
/*!40000 ALTER TABLE `project_zarah_servicio_salud_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_servicios_list`
--

DROP TABLE IF EXISTS `project_zarah_servicios_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_servicios_list` (
  `id` int(11) NOT NULL,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_servicios_list`
--

LOCK TABLES `project_zarah_servicios_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_servicios_list` DISABLE KEYS */;
INSERT INTO `project_zarah_servicios_list` VALUES (1,'Agua'),(2,'Luz'),(3,'Internet'),(4,'Drenaje'),(5,'Gas'),(6,'Teléfono fijo'),(7,'Teléfono celular'),(8,'Televisión abierta'),(9,'Televisión de paga'),(10,'Vigilancia'),(11,'Servicio de limpieza'),(12,'Pavimentación');
/*!40000 ALTER TABLE `project_zarah_servicios_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_sexo_list`
--

DROP TABLE IF EXISTS `project_zarah_sexo_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_sexo_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_sexo_list`
--

LOCK TABLES `project_zarah_sexo_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_sexo_list` DISABLE KEYS */;
INSERT INTO `project_zarah_sexo_list` VALUES (1,'Femenino'),(2,'Masculino');
/*!40000 ALTER TABLE `project_zarah_sexo_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_egreso_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_egreso_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_egreso_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_egreso_list`
--

LOCK TABLES `project_zarah_tipo_egreso_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_egreso_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_egreso_list` VALUES (1,'Agua'),(2,'Luz'),(3,'Telefono'),(4,'Salud'),(5,'Transporte'),(6,'Alimentación'),(7,'Ropa y calzado'),(8,'Educación'),(9,'Vivienda'),(10,'Entretenimiento'),(11,'Gas'),(12,'Otro');
/*!40000 ALTER TABLE `project_zarah_tipo_egreso_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_paredes_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_paredes_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_paredes_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_paredes_list`
--

LOCK TABLES `project_zarah_tipo_paredes_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_paredes_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_paredes_list` VALUES (1,'Ladrillo'),(2,'Adobe'),(3,'Madera'),(4,'Block'),(5,'Cartón'),(6,'Piedra');
/*!40000 ALTER TABLE `project_zarah_tipo_paredes_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_piso_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_piso_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_piso_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_piso_list`
--

LOCK TABLES `project_zarah_tipo_piso_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_piso_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_piso_list` VALUES (1,'Tierra'),(2,'Cemento o Firme'),(3,'Loseta o Mosaico'),(4,'Madera');
/*!40000 ALTER TABLE `project_zarah_tipo_piso_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_propiedad_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_propiedad_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_propiedad_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_propiedad_list`
--

LOCK TABLES `project_zarah_tipo_propiedad_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_propiedad_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_propiedad_list` VALUES (1,'Propia'),(2,'Rentada'),(3,'Hipotecada'),(4,'Renta Congelada'),(5,'Prestada'),(6,'Familiar');
/*!40000 ALTER TABLE `project_zarah_tipo_propiedad_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_techo_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_techo_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_techo_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_techo_list`
--

LOCK TABLES `project_zarah_tipo_techo_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_techo_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_techo_list` VALUES (1,'Loza concreto'),(2,'Lamina de cartón'),(3,'Lámina de Asbesto'),(4,'Palma, paja o madera'),(5,'teja'),(6,'Lamina galbanizada');
/*!40000 ALTER TABLE `project_zarah_tipo_techo_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_vivienda_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_vivienda_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_vivienda_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_vivienda_list`
--

LOCK TABLES `project_zarah_tipo_vivienda_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_vivienda_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_vivienda_list` VALUES (1,'Casa Sola'),(2,'Duplex'),(3,'Condominio'),(4,'Departamento'),(5,'Vecindad'),(6,'Casa de Huéspedes');
/*!40000 ALTER TABLE `project_zarah_tipo_vivienda_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_vehiculos`
--

DROP TABLE IF EXISTS `project_zarah_vehiculos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_vehiculos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(50) DEFAULT NULL,
  `modelo` varchar(10) DEFAULT NULL,
  `propietario` varchar(100) DEFAULT NULL,
  `valor_actual` varchar(100) DEFAULT NULL,
  `pagado` tinyint(1) DEFAULT NULL,
  `adeudo` varchar(100) DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_vehiculos`
--

LOCK TABLES `project_zarah_vehiculos` WRITE;
/*!40000 ALTER TABLE `project_zarah_vehiculos` DISABLE KEYS */;
INSERT INTO `project_zarah_vehiculos` VALUES (1,'CHEVROLET MERIVA','2004','ISABEL BARRIOS FLORES','60000',1,'0',2),(3,'CHEVROLET MERIVA','2004','ISABEL BARRIOS FLORES','60000',1,'0',1),(4,'CHEVY','2010','GERARDO FUENTES','',1,'0',47),(5,'-','-','-','-',1,'0',50),(6,'-','-','-','-',1,'0',50),(7,'NISSAN','1996','GERARDO','0',1,'TENENCIAS',59);
/*!40000 ALTER TABLE `project_zarah_vehiculos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `cuenta_dependientes`
--

/*!50001 DROP TABLE IF EXISTS `cuenta_dependientes`*/;
/*!50001 DROP VIEW IF EXISTS `cuenta_dependientes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`prueba`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `cuenta_dependientes` AS select count(distinct `project_zarah_integrantes_hogar`.`id`) AS `dependientes`,`project_zarah_integrantes_hogar`.`estudio_socioeconomico_id` AS `estudio` from `project_zarah_integrantes_hogar` where `project_zarah_integrantes_hogar`.`dependiente_list_id` = 1 group by `project_zarah_integrantes_hogar`.`estudio_socioeconomico_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `cuenta_egresos`
--

/*!50001 DROP TABLE IF EXISTS `cuenta_egresos`*/;
/*!50001 DROP VIEW IF EXISTS `cuenta_egresos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`prueba`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `cuenta_egresos` AS select `t1`.`estudio_socioeconomico_id` AS `estudio`,`t_agua`.`total` AS `total_agua`,`t_luz`.`total` AS `total_luz` from ((`ese_acrec`.`project_zarah_egresos` `t1` join (select `ese_acrec`.`project_zarah_egresos`.`estudio_socioeconomico_id` AS `estudio_socioeconomico_id`,`ese_acrec`.`project_zarah_egresos`.`cantidad` AS `total` from `ese_acrec`.`project_zarah_egresos` where `ese_acrec`.`project_zarah_egresos`.`tipo_egreso_list_id` = 1) `t_agua` on(`t1`.`estudio_socioeconomico_id` = `t_agua`.`estudio_socioeconomico_id`)) join (select `ese_acrec`.`project_zarah_egresos`.`estudio_socioeconomico_id` AS `estudio_socioeconomico_id`,`ese_acrec`.`project_zarah_egresos`.`cantidad` AS `total` from `ese_acrec`.`project_zarah_egresos` where `ese_acrec`.`project_zarah_egresos`.`tipo_egreso_list_id` = 2) `t_luz` on(`t1`.`estudio_socioeconomico_id` = `t_luz`.`estudio_socioeconomico_id`)) group by `t1`.`estudio_socioeconomico_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `cuenta_ingresos`
--

/*!50001 DROP TABLE IF EXISTS `cuenta_ingresos`*/;
/*!50001 DROP VIEW IF EXISTS `cuenta_ingresos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`prueba`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `cuenta_ingresos` AS select `project_zarah_ingresos`.`estudio_socioeconomico_id` AS `estudio`,count(distinct `project_zarah_ingresos`.`id`) AS `ingresos`,avg(distinct `project_zarah_ingresos`.`edad`) AS `edad`,avg(distinct `project_zarah_ingresos`.`parentesco_list_id`) AS `avg_parentesco`,avg(distinct `project_zarah_ingresos`.`ocupaciones_list_id`) AS `ocupacion`,avg(distinct `project_zarah_ingresos`.`ingresos_tipo_list_id`) AS `tipo`,avg(distinct `project_zarah_ingresos`.`ingreso`) AS `avg_ingreso`,sum(distinct `project_zarah_ingresos`.`ingreso`) AS `total_ingreso` from `project_zarah_ingresos` group by `project_zarah_ingresos`.`estudio_socioeconomico_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `cuenta_integrantes`
--

/*!50001 DROP TABLE IF EXISTS `cuenta_integrantes`*/;
/*!50001 DROP VIEW IF EXISTS `cuenta_integrantes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`prueba`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `cuenta_integrantes` AS select `project_zarah_integrantes_hogar`.`estudio_socioeconomico_id` AS `estudio`,count(distinct `project_zarah_integrantes_hogar`.`id`) AS `integrantes`,avg(distinct `project_zarah_integrantes_hogar`.`edad`) AS `edad`,avg(distinct `project_zarah_integrantes_hogar`.`parentesco_list_id`) AS `avg_parentesco`,avg(distinct `project_zarah_integrantes_hogar`.`institucion_academica_list_id`) AS `institucion_academica`,avg(distinct `project_zarah_integrantes_hogar`.`ocupaciones_list_id`) AS `ocupacion` from `project_zarah_integrantes_hogar` group by `project_zarah_integrantes_hogar`.`estudio_socioeconomico_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `int_edades`
--

/*!50001 DROP TABLE IF EXISTS `int_edades`*/;
/*!50001 DROP VIEW IF EXISTS `int_edades`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`prueba`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `int_edades` AS select round((to_days(curdate()) - to_days(`project_zarah_estudio_socioeconomico`.`fecha_nacimiento`)) / 365.25,0) AS `edad`,`project_zarah_estudio_socioeconomico`.`id` AS `id` from `project_zarah_estudio_socioeconomico` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `normalizados`
--

/*!50001 DROP TABLE IF EXISTS `normalizados`*/;
/*!50001 DROP VIEW IF EXISTS `normalizados`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`prueba`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `normalizados` AS select `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id` AS `id`,`ese_acrec`.`project_zarah_estudio_socioeconomico`.`entrenamiento` AS `entrenamiento`,coalesce(((select `int_edades`.`edad` from `ese_acrec`.`int_edades` where `int_edades`.`id` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`int_edades`.`edad`) from `ese_acrec`.`int_edades`)) / ((select max(`int_edades`.`edad`) from `ese_acrec`.`int_edades`) - (select min(`int_edades`.`edad`) from `ese_acrec`.`int_edades`)),0) AS `edad`,coalesce((`ese_acrec`.`project_zarah_estudio_socioeconomico`.`sexo_list_id` - (select min(`ese_acrec`.`project_zarah_sexo_list`.`id`) from `ese_acrec`.`project_zarah_sexo_list`)) / ((select max(`ese_acrec`.`project_zarah_sexo_list`.`id`) from `ese_acrec`.`project_zarah_sexo_list`) - (select min(`ese_acrec`.`project_zarah_sexo_list`.`id`) from `ese_acrec`.`project_zarah_sexo_list`)),0) AS `sexo`,coalesce((`ese_acrec`.`project_zarah_estudio_socioeconomico`.`escolaridad_list_id` - (select min(`ese_acrec`.`project_zarah_escolaridad_list`.`id`) from `ese_acrec`.`project_zarah_escolaridad_list`)) / ((select max(`ese_acrec`.`project_zarah_escolaridad_list`.`id`) from `ese_acrec`.`project_zarah_escolaridad_list`) - (select min(`ese_acrec`.`project_zarah_escolaridad_list`.`id`) from `ese_acrec`.`project_zarah_escolaridad_list`)),0) AS `escolaridad`,coalesce((`ese_acrec`.`project_zarah_estudio_socioeconomico`.`estado_civil_list_id` - (select min(`ese_acrec`.`project_zarah_estado_civil_list`.`id`) from `ese_acrec`.`project_zarah_estado_civil_list`)) / ((select max(`ese_acrec`.`project_zarah_estado_civil_list`.`id`) from `ese_acrec`.`project_zarah_estado_civil_list`) - (select min(`ese_acrec`.`project_zarah_estado_civil_list`.`id`) from `ese_acrec`.`project_zarah_estado_civil_list`)),0) AS `estado_civil`,coalesce((`ese_acrec`.`project_zarah_estudio_socioeconomico`.`ocupaciones_list_id` - (select min(`ese_acrec`.`project_zarah_ocupaciones_list`.`id`) from `ese_acrec`.`project_zarah_ocupaciones_list`)) / ((select max(`ese_acrec`.`project_zarah_ocupaciones_list`.`id`) from `ese_acrec`.`project_zarah_ocupaciones_list`) - (select min(`ese_acrec`.`project_zarah_ocupaciones_list`.`id`) from `ese_acrec`.`project_zarah_ocupaciones_list`)),0) AS `ocupacion`,coalesce((`ese_acrec`.`project_zarah_estudio_socioeconomico`.`codigo_postal` - (select min(`ese_acrec`.`project_zarah_estudio_socioeconomico`.`codigo_postal`) from `ese_acrec`.`project_zarah_estudio_socioeconomico`)) / ((select max(`ese_acrec`.`project_zarah_estudio_socioeconomico`.`codigo_postal`) from `ese_acrec`.`project_zarah_estudio_socioeconomico`) - (select min(`ese_acrec`.`project_zarah_estudio_socioeconomico`.`codigo_postal`) from `ese_acrec`.`project_zarah_estudio_socioeconomico`)),0) AS `codigo_postal`,coalesce(((select `cuenta_integrantes`.`integrantes` from `ese_acrec`.`cuenta_integrantes` where `cuenta_integrantes`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_integrantes`.`integrantes`) from `ese_acrec`.`cuenta_integrantes`)) / ((select max(`cuenta_integrantes`.`integrantes`) from `ese_acrec`.`cuenta_integrantes`) - (select min(`cuenta_integrantes`.`integrantes`) from `ese_acrec`.`cuenta_integrantes`)),0) AS `integrantes`,coalesce(((select `cuenta_dependientes`.`dependientes` from `ese_acrec`.`cuenta_dependientes` where `cuenta_dependientes`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_dependientes`.`dependientes`) from `ese_acrec`.`cuenta_dependientes`)) / ((select max(`cuenta_dependientes`.`dependientes`) from `ese_acrec`.`cuenta_dependientes`) - (select min(`cuenta_dependientes`.`dependientes`) from `ese_acrec`.`cuenta_dependientes`)),0) AS `dependientes`,coalesce(((select `cuenta_integrantes`.`edad` from `ese_acrec`.`cuenta_integrantes` where `cuenta_integrantes`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_integrantes`.`edad`) from `ese_acrec`.`cuenta_integrantes`)) / ((select max(`cuenta_integrantes`.`edad`) from `ese_acrec`.`cuenta_integrantes`) - (select min(`cuenta_integrantes`.`edad`) from `ese_acrec`.`cuenta_integrantes`)),0) AS `int_edad`,coalesce(((select `cuenta_integrantes`.`avg_parentesco` from `ese_acrec`.`cuenta_integrantes` where `cuenta_integrantes`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_integrantes`.`avg_parentesco`) from `ese_acrec`.`cuenta_integrantes`)) / ((select max(`cuenta_integrantes`.`avg_parentesco`) from `ese_acrec`.`cuenta_integrantes`) - (select min(`cuenta_integrantes`.`avg_parentesco`) from `ese_acrec`.`cuenta_integrantes`)),0) AS `int_parentesco`,coalesce(((select `cuenta_integrantes`.`institucion_academica` from `ese_acrec`.`cuenta_integrantes` where `cuenta_integrantes`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_integrantes`.`institucion_academica`) from `ese_acrec`.`cuenta_integrantes`)) / ((select max(`cuenta_integrantes`.`institucion_academica`) from `ese_acrec`.`cuenta_integrantes`) - (select min(`cuenta_integrantes`.`institucion_academica`) from `ese_acrec`.`cuenta_integrantes`)),0) AS `institucion_academica`,coalesce(((select `cuenta_integrantes`.`ocupacion` from `ese_acrec`.`cuenta_integrantes` where `cuenta_integrantes`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_integrantes`.`ocupacion`) from `ese_acrec`.`cuenta_integrantes`)) / ((select max(`cuenta_integrantes`.`ocupacion`) from `ese_acrec`.`cuenta_integrantes`) - (select min(`cuenta_integrantes`.`ocupacion`) from `ese_acrec`.`cuenta_integrantes`)),0) AS `int_ocupacion`,coalesce(((select `cuenta_ingresos`.`ingresos` from `ese_acrec`.`cuenta_ingresos` where `cuenta_ingresos`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_ingresos`.`ingresos`) from `ese_acrec`.`cuenta_ingresos`)) / ((select max(`cuenta_ingresos`.`ingresos`) from `ese_acrec`.`cuenta_ingresos`) - (select min(`cuenta_ingresos`.`ingresos`) from `ese_acrec`.`cuenta_ingresos`)),0) AS `ingresos`,coalesce(((select `cuenta_ingresos`.`edad` from `ese_acrec`.`cuenta_ingresos` where `cuenta_ingresos`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_ingresos`.`edad`) from `ese_acrec`.`cuenta_ingresos`)) / ((select max(`cuenta_ingresos`.`edad`) from `ese_acrec`.`cuenta_ingresos`) - (select min(`cuenta_ingresos`.`edad`) from `ese_acrec`.`cuenta_ingresos`)),0) AS `ing_edad`,coalesce(((select `cuenta_ingresos`.`tipo` from `ese_acrec`.`cuenta_ingresos` where `cuenta_ingresos`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_ingresos`.`tipo`) from `ese_acrec`.`cuenta_ingresos`)) / ((select max(`cuenta_ingresos`.`tipo`) from `ese_acrec`.`cuenta_ingresos`) - (select min(`cuenta_ingresos`.`tipo`) from `ese_acrec`.`cuenta_ingresos`)),0) AS `avg_tipo_ingreso`,coalesce(((select `cuenta_ingresos`.`avg_parentesco` from `ese_acrec`.`cuenta_ingresos` where `cuenta_ingresos`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_ingresos`.`avg_parentesco`) from `ese_acrec`.`cuenta_ingresos`)) / ((select max(`cuenta_ingresos`.`avg_parentesco`) from `ese_acrec`.`cuenta_ingresos`) - (select min(`cuenta_ingresos`.`avg_parentesco`) from `ese_acrec`.`cuenta_ingresos`)),0) AS `avg_ing_parentesco`,coalesce(((select `cuenta_ingresos`.`ocupacion` from `ese_acrec`.`cuenta_ingresos` where `cuenta_ingresos`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_ingresos`.`ocupacion`) from `ese_acrec`.`cuenta_ingresos`)) / ((select max(`cuenta_ingresos`.`ocupacion`) from `ese_acrec`.`cuenta_ingresos`) - (select min(`cuenta_ingresos`.`ocupacion`) from `ese_acrec`.`cuenta_ingresos`)),0) AS `ing_ocupacion`,coalesce(((select `cuenta_ingresos`.`avg_ingreso` from `ese_acrec`.`cuenta_ingresos` where `cuenta_ingresos`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_ingresos`.`avg_ingreso`) from `ese_acrec`.`cuenta_ingresos`)) / ((select max(`cuenta_ingresos`.`avg_ingreso`) from `ese_acrec`.`cuenta_ingresos`) - (select min(`cuenta_ingresos`.`avg_ingreso`) from `ese_acrec`.`cuenta_ingresos`)),0) AS `avg_ingreso`,coalesce(((select `cuenta_ingresos`.`total_ingreso` from `ese_acrec`.`cuenta_ingresos` where `cuenta_ingresos`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_ingresos`.`total_ingreso`) from `ese_acrec`.`cuenta_ingresos`)) / ((select max(`cuenta_ingresos`.`total_ingreso`) from `ese_acrec`.`cuenta_ingresos`) - (select min(`cuenta_ingresos`.`total_ingreso`) from `ese_acrec`.`cuenta_ingresos`)),0) AS `total_ingreso`,coalesce(((select `cuenta_egresos`.`total_agua` from `ese_acrec`.`cuenta_egresos` where `cuenta_egresos`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_egresos`.`total_agua`) from `ese_acrec`.`cuenta_egresos`)) / ((select max(`cuenta_egresos`.`total_agua`) from `ese_acrec`.`cuenta_egresos`) - (select min(`cuenta_egresos`.`total_agua`) from `ese_acrec`.`cuenta_egresos`)),0) AS `total_agua`,coalesce(((select `cuenta_egresos`.`total_luz` from `ese_acrec`.`cuenta_egresos` where `cuenta_egresos`.`estudio` = `ese_acrec`.`project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_egresos`.`total_luz`) from `ese_acrec`.`cuenta_egresos`)) / ((select max(`cuenta_egresos`.`total_luz`) from `ese_acrec`.`cuenta_egresos`) - (select min(`cuenta_egresos`.`total_luz`) from `ese_acrec`.`cuenta_egresos`)),0) AS `total_luz`,coalesce((`ese_acrec`.`project_zarah_estudio_socioeconomico`.`clasificacion_ts` - (select min(`ese_acrec`.`project_zarah_clasificaciones_list`.`id`) from `ese_acrec`.`project_zarah_clasificaciones_list`)) / ((select max(`ese_acrec`.`project_zarah_clasificaciones_list`.`id`) from `ese_acrec`.`project_zarah_clasificaciones_list`) - (select min(`ese_acrec`.`project_zarah_clasificaciones_list`.`id`) from `ese_acrec`.`project_zarah_clasificaciones_list`)),0) AS `clasificacion_ts` from `ese_acrec`.`project_zarah_estudio_socioeconomico` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-01 10:23:03
