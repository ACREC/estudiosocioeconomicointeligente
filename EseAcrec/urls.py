from django.conf.urls import url,include
from django.urls import path

urlpatterns = [
    url('ese/', include('ese.urls')),
    url('encuestas/', include('vfq.urls')),
    url('zarah/', include('project_zarah.urls'))
]
