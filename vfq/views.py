from django.shortcuts import render
import json
from django.http import JsonResponse
from vfq.models import Vfq
from django.core import serializers


def index(request):
    vfq_res = list(Vfq.objects.exclude(p1_ts__isnull=True).exclude(pre="POST"))

    return render(request, 'vfq/home.html', {'vfq' : vfq_res})
    #return JsonResponse(vfq_res, safe=False)