from django.db import models
from django.utils import timezone


class Vfq(models.Model):
    fecha = models.DateField(default = timezone.now)
    id_paciente = models.IntegerField(default = 0)
    p1 = models.IntegerField(default = 0)
    p2 = models.IntegerField(default = 0)
    p3 = models.IntegerField(default = 0)
    p4 = models.IntegerField(default = 0)
    p5 = models.IntegerField(default = 0)
    p6 = models.IntegerField(default = 0)
    p7a = models.IntegerField(default = 0)
    p7b = models.IntegerField(default = 0)
    p8 = models.IntegerField(default = 0)
    p9 = models.IntegerField(default = 0)
    p10 = models.IntegerField(default = 0)
    p11a = models.IntegerField(default = 0)
    p11b = models.IntegerField(default = 0)
    p1_ts = models.IntegerField(default = 0)
    p2_ts = models.IntegerField(default = 0)
    p3_ts = models.IntegerField(default = 0)
    p4_ts = models.IntegerField(default = 0)
    p5_ts = models.IntegerField(default = 0)
    p6_ts = models.IntegerField(default = 0)
    p7a_ts = models.IntegerField(default = 0)
    p7b_ts = models.IntegerField(default = 0)
    p8_ts = models.IntegerField(default = 0)
    p9_ts = models.IntegerField(default = 0)
    p10_ts = models.IntegerField(default = 0)
    p11a_ts = models.IntegerField(default = 0)
    p11b_ts = models.IntegerField(default = 0)
    pre = models.CharField(max_length = 4)
    procedimiento = models.CharField(max_length = 100)
    IdEmpleado = models.IntegerField(default = 0)
    lent = models.BooleanField()
    notas = models.BooleanField()


    class Meta:
        db_table = "encuesta_vfq14"