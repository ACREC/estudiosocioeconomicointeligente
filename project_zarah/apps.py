from django.apps import AppConfig


class ProjectZarahConfig(AppConfig):
    name = 'project_zarah'
