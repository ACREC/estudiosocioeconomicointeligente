from django.urls import path
from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index),
    path('train/', views.train),
    path('predict/', views.predict),
    path('add_layer/', views.add_layer),
    path('del_layer/', views.del_layer),
    path('speech/', views.speech)
]