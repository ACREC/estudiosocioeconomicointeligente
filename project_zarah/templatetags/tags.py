from django import template
import numpy as np

register = template.Library()

@register.filter
def multiply(value, arg):
    return round(value * arg,4)

@register.filter
def sumar(value):
    return np.sum(value)

@register.filter
def contar(value):
    return np.count_nonzero(value)