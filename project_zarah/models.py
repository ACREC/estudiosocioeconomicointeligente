from django.db import models

# Create your models here.
class Model_Def(models.Model):
    model_name = models.CharField(max_length = 45)
    model_optimizer = models.CharField(max_length = 100)
    model_loss = models.CharField(max_length = 100)
    model_trained = models.BooleanField()
    model_error = models.FloatField()
    model_acc = models.FloatField()
    model_dir = models.CharField(max_length = 100)

class Model_Layers(models.Model):
    layer_units = models.IntegerField()
    layer_activation = models.CharField(max_length = 100)
    layer_model_def = models.ForeignKey(Model_Def, on_delete=models.CASCADE)
    layer_order = models.IntegerField()

class Escolaridad_List(models.Model):
    texto = models.CharField(max_length=50)
class Sexo_List(models.Model):
    texto = models.CharField(max_length=50)
class Estado_Civil_List(models.Model):
    texto = models.CharField(max_length=50)
class Tipo_Egreso_List(models.Model):
    texto = models.CharField(max_length=50)
class Servicios_List(models.Model):
    texto = models.CharField(max_length=50)
class Caracteristicas_List(models.Model):
    texto = models.CharField(max_length=50)
class Ocupaciones_list(models.Model):
    texto = models.CharField(max_length=100)
    observacion = models.CharField(max_length=100)
class Parentesco_list(models.Model):
    texto = models.CharField(max_length=45)
class Ingresos_tipo_list(models.Model):
    texto = models.CharField(max_length=50)
class Tipo_Vivienda_List(models.Model):
    texto = models.CharField(max_length=50)
class Institucion_Academica_List(models.Model):
    texto = models.CharField(max_length=50)
class Dependiente_List(models.Model):
    texto = models.CharField(max_length=50)
class Tipo_Propiedad_List(models.Model):
    texto = models.CharField(max_length=50)
class Tipo_Techo_List(models.Model):
    texto = models.CharField(max_length=50)
class Tipo_Piso_List(models.Model):
    texto = models.CharField(max_length=50)
class Tipo_Paredes_List(models.Model):
    texto = models.CharField(max_length=50)
class Higiene_List(models.Model):
    texto = models.CharField(max_length=50)
class Estado_General_Vivienda_List(models.Model):
    texto = models.CharField(max_length=50)
class Manaje_List(models.Model):
    texto = models.CharField(max_length=50)
class Discapacidad_List(models.Model):
    texto = models.CharField(max_length=50)
class Frecuencias_List(models.Model):
    texto = models.CharField(max_length=50)
class Alimentos_List(models.Model):
    texto = models.CharField(max_length=50)
class Despensas_List(models.Model):
    texto = models.CharField(max_length=50)
class Beneficiario_List(models.Model):
    texto = models.CharField(max_length=50)
class Servicio_Salud_List(models.Model):
    texto = models.CharField(max_length=50)
class Enfermedades_List(models.Model):
    texto = models.CharField(max_length=50)
class Clasificaciones_List(models.Model):
    texto = models.CharField(max_length=50)


class Pacientes(models.Model):
    class Meta:
        db_table = 'pacientes'
    apellido_paterno = models.CharField(max_length=128)
    apellido_materno = models.CharField(max_length=128)
    nombre = models.CharField(max_length=128)
    clave = models.CharField(max_length=50)
    fecha_alta = models.DateField(auto_now=True, auto_now_add=False)
    fecha_nac = models.DateField(auto_now=False, auto_now_add=False)
    sexo_id = models.IntegerField()
    estado_civil_id = models.IntegerField()


class Estudio_Socioeconomico(models.Model):
    clave = models.CharField(max_length=45)
    fecha_nacimiento = models.DateField(auto_now=False, auto_now_add=False)
    fecha = models.DateTimeField(auto_now=True)
    sexo_list = models.ForeignKey(Sexo_List, on_delete=models.CASCADE)
    escolaridad_list = models.ForeignKey(Escolaridad_List, on_delete=models.CASCADE)
    estado_civil_list = models.ForeignKey(Estado_Civil_List, on_delete=models.CASCADE)
    codigo_postal = models.IntegerField()
    calle = models.CharField(max_length=255)
    colonia = models.CharField(max_length=100)
    entre_calles = models.CharField(max_length=100)
    localidad = models.CharField(max_length=100)
    telefono_fijo = models.CharField(max_length=20)
    telefono_celular = models.CharField(max_length=20)
    email = models.EmailField(max_length=100)
    apaterno = models.CharField(max_length=100)
    amaterno = models.CharField(max_length=100)
    nombres = models.CharField(max_length=100)
    ocupaciones_list = models.ForeignKey(Ocupaciones_list, on_delete=models.CASCADE)
    num_interior = models.CharField(max_length=45)
    num_exterior = models.CharField(max_length=45)
    numero_habitantes = models.IntegerField()
    tipo_vivienda_list = models.ForeignKey(Tipo_Vivienda_List, on_delete=models.CASCADE)
    tipo_propiedad_list = models.ForeignKey(Tipo_Propiedad_List, on_delete=models.CASCADE)
    m2_terreno = models.IntegerField()
    m2_construccion = models.IntegerField()
    tiempo_residencia = models.IntegerField()
    tipo_piso_list = models.ForeignKey(Tipo_Piso_List, on_delete=models.CASCADE)
    tipo_techo_list = models.ForeignKey(Tipo_Techo_List,  on_delete=models.CASCADE)
    tipo_paredes_list = models.ForeignKey(Tipo_Paredes_List, on_delete=models.CASCADE)
    higiene_list = models.ForeignKey(Higiene_List, on_delete=models.CASCADE)
    servicios = models.ManyToManyField(Servicios_List, through='Estudio_Socioeconomico_Servicios')
    estado_general_vivienda_list = models.ForeignKey(Estado_General_Vivienda_List, on_delete=models.CASCADE)
    caracteristicas = models.ManyToManyField(Caracteristicas_List, through='Estudio_Socioeconomico_Caracteristicas')
    manajes = models.ManyToManyField(Manaje_List, through='Estudio_Socioeconomico_Manaje')
    discapacidad_list = models.ForeignKey(Discapacidad_List, on_delete=models.CASCADE)
    alimentacion = models.ManyToManyField(Alimentos_List, through='Estudio_Socioeconomico_Alimentos')
    despensas_list = models.ForeignKey(Despensas_List, on_delete=models.CASCADE)
    beneficiario_list = models.ForeignKey(Beneficiario_List, on_delete=models.CASCADE)
    servicio_salud_list = models.ForeignKey(Servicio_Salud_List, on_delete=models.CASCADE)
    enfermedades_list = models.ForeignKey(Enfermedades_List, on_delete=models.CASCADE)
    paciente = models.ForeignKey(Pacientes, on_delete=models.CASCADE)
    clasificacion_ts = models.IntegerField()
    clasificacion_ia = models.IntegerField()
    observaciones = models.TextField()
    diagnostico = models.TextField()
    falsea_datos = models.BooleanField()
    ref_domicilio = models.CharField(max_length=500)
    entrenamiento = models.BooleanField()

class Integrantes_Hogar(models.Model):
    nombre = models.CharField(max_length=50)
    edad = models.IntegerField()
    dependiente_list = models.ForeignKey(Dependiente_List, on_delete=models.CASCADE)
    parentesco_list = models.ForeignKey(Parentesco_list, on_delete=models.CASCADE)
    institucion_academica_list = models.ForeignKey(Institucion_Academica_List, on_delete=models.CASCADE)
    estudio_socioeconomico = models.ForeignKey(Estudio_Socioeconomico, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now=True)
    ocupaciones_list = models.ForeignKey(Ocupaciones_list, on_delete=models.CASCADE)
    discapacidad_list = models.ForeignKey(Discapacidad_List,  on_delete=models.CASCADE)
    trabajo = models.CharField(max_length=512)

class Ingresos(models.Model):
    nombre = models.CharField(max_length=50)
    edad = models.IntegerField()
    ingreso = models.FloatField()
    ocupaciones_list = models.ForeignKey(Ocupaciones_list,on_delete=models.CASCADE)
    estudio_socioeconomico = models.ForeignKey(Estudio_Socioeconomico, on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now=True)
    parentesco_list = models.ForeignKey(Parentesco_list, on_delete=models.CASCADE)
    ingresos_tipo_list = models.ForeignKey(Ingresos_tipo_list, on_delete=models.CASCADE)

class Egresos(models.Model):
    estudio_socioeconomico = models.ForeignKey(Estudio_Socioeconomico, on_delete=models.CASCADE)
    tipo_egreso_list = models.ForeignKey(Tipo_Egreso_List, on_delete=models.CASCADE)
    cantidad = models.FloatField()
    observacion = models.CharField(max_length=100)
    fecha = models.DateTimeField(auto_now=True)

class Estudio_Socioeconomico_Servicios(models.Model):
    servicios_list = models.ForeignKey(Servicios_List, on_delete=models.CASCADE)
    estudio_socioeconomico = models.ForeignKey(Estudio_Socioeconomico,  on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now=True)

class Estudio_Socioeconomico_Caracteristicas(models.Model):
    caracteristicas_list = models.ForeignKey(Caracteristicas_List, on_delete=models.CASCADE)
    estudio_socioeconomico = models.ForeignKey(Estudio_Socioeconomico, on_delete=models.CASCADE)
    cantidad = models.IntegerField()
    fecha = models.DateTimeField(auto_now=True)

class Estudio_Socioeconomico_Manaje(models.Model):
    manaje_list = models.ForeignKey(Manaje_List, on_delete=models.CASCADE)
    estudio_socioeconomico = models.ForeignKey(Estudio_Socioeconomico, on_delete=models.CASCADE)
    cantidad  = models.IntegerField()

class Estudio_Socioeconomico_Alimentos(models.Model):
    estudio_socioeconomico = models.ForeignKey(Estudio_Socioeconomico, on_delete=models.CASCADE)
    alimentos_list = models.ForeignKey(Alimentos_List, on_delete=models.CASCADE)
    frecuencias_list = models.ForeignKey(Frecuencias_List, on_delete=models.CASCADE)
    observaciones = models.CharField(max_length=50)

class Vehiculos(models.Model):
    marca = models.CharField(max_length=50)
    modelo = models.CharField(max_length=50)
    propietario = models.CharField(max_length=50)
    valor_actual = models.CharField(max_length=50)
    pagado = models.BooleanField()
    adeudo = models.CharField(max_length=50)
    estudio_socioeconomico = models.ForeignKey(Estudio_Socioeconomico, on_delete=models.CASCADE)

class Normalizados(models.Model):
    entrenamiento = models.BooleanField()
    edad = models.FloatField()
    sexo = models.FloatField()
    escolaridad = models.FloatField()
    estado_civil = models.FloatField()
    ocupacion = models.FloatField()
    codigo_postal = models.FloatField()
    integrantes = models.FloatField()
    dependientes = models.FloatField()
    int_edad = models.FloatField()
    int_parentesco = models.FloatField()
    institucion_academica = models.FloatField()
    int_ocupacion = models.FloatField()
    ingresos = models.FloatField()
    ing_edad = models.FloatField()
    avg_tipo_ingreso = models.FloatField()
    avg_ing_parentesco = models.FloatField()
    ing_ocupacion = models.FloatField()
    avg_ingreso = models.FloatField()
    total_ingreso = models.FloatField()
    total_agua = models.FloatField()
    total_luz = models.FloatField()

    clasificacion_ts = models.FloatField()
    
    class Meta:
        managed = False
        db_table = 'normalizados'
