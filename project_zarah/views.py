from django.shortcuts import render
from project_zarah.models import *
import tensorflow as tf
from scipy.optimize import root
from tensorflow import keras
from tensorflow.keras import backend as K
from django.conf import settings
import numpy as np
import json, decimal, os
from django.db.models import Q
from django.http import HttpResponse

def decimal_default(obj):
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    raise TypeError


def index(request):
    rna = Model_Def.objects.get(model_name = "project_zarah")
    rna_layers = list(rna.model_layers_set.values_list('layer_units', flat=True))
    return render(request, 'project_zarah/index.html', { 'trained_model' : rna, 'model_layers' : rna_layers })


def speech(request):
    rna = Model_Def.objects.get(model_name = "zarah_speech")
    rna_layers = list(rna.model_layers_set.values_list('layer_units', flat=True))
    return render(request, 'project_zarah/index.html', { 'trained_model' : rna, 'model_layers' : rna_layers })
    
def train(request):
    params = json.loads(request.body.decode('utf-8'))
    datos_entrenamiento = Normalizados.objects.filter(entrenamiento = True).values_list(
        'edad', 
        'sexo', 
        'escolaridad', 
        'estado_civil', 
        'ocupacion', 
        'codigo_postal', 
        'integrantes', 
        'dependientes',
        'int_edad',
        'int_parentesco',
        'institucion_academica',
        'int_ocupacion',
        'ingresos',
        'ing_edad',
        'avg_tipo_ingreso',
        'avg_ing_parentesco',
        'ing_ocupacion',
        'avg_ingreso',
        'total_ingreso',
        'total_agua',
        'total_luz'
    )
    salidas_entrenamiento = Normalizados.objects.filter(entrenamiento = True).values_list('clasificacion_ts')

    entradas_entrenamiento_list = list(datos_entrenamiento)
    salidas_entrenamiento_list = list(salidas_entrenamiento)
    stored_model = Model_Def.objects.get(id = params['model'])
    layers = []
    #checkPointDir = os.path.join(settings.BASE_DIR + "\\project_zarah\\", stored_model.model_dir)
    checkPointDir = os.path.join('./' + stored_model.model_dir)
    cp_callback = keras.callbacks.ModelCheckpoint(
        checkPointDir, 
        save_weights_only = False, 
        verbose = 1,
        mode='max')
    
    layers.append(keras.layers.Flatten())
    for  model_layer in stored_model.model_layers_set.all():
        layer = keras.layers.Dense(
           units = model_layer.layer_units,
            activation = model_layer.layer_activation, 
            use_bias=True,
            bias_initializer='zeros',
            input_shape= np.array(entradas_entrenamiento_list).shape
        )
        layers.append(layer)
        
    model = keras.Sequential(layers)
    optimizer = keras.optimizers.SGD(
        lr=float(params['lr']), 
        momentum=float(params['momentum']), 
        decay=1e-5, 
        nesterov=False
    )
    model.compile(
        optimizer = optimizer, 
        loss = params['error'], 
        metrics=[params['error']]
    )
    
    loss = model.fit(
        np.array(entradas_entrenamiento_list), 
        np.array(salidas_entrenamiento_list), 
        epochs=int(params['epochs']), 
        verbose=1, 
        callbacks=[cp_callback], 
    )

    stored_model.model_error = loss.history['loss'][-1]
    stored_model.model_acc = (1- loss.history['loss'][-1])
    stored_model.save()

    return HttpResponse(json.dumps({'error' : loss.history['loss'][-1]}))

def predict(request):
    return HttpResponse("Predicción")

def add_layer(request):
    data = json.loads(request.body.decode('utf-8'))
    model = Model_Def.objects.get(id = data['model'])
    layer = Model_Layers(layer_units = int(data['units']), layer_activation = data['activation'], layer_model_def = model)
    layer.save()
    op_data = {
        'text' : 'Capa añadida a la red: ' + model.model_name,
        'id' : int(layer.id)
    }
    return HttpResponse(json.dumps(op_data))

def del_layer(request):
    data = json.loads(request.body.decode('utf-8'))
    layer = Model_Layers.objects.get(id = int(data['id']))
    layer.delete()
    op_data = {
        'text' : 'Capa eliminada de la red'
    }
    return HttpResponse(json.dumps(op_data))