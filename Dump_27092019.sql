-- MySQL dump 10.16  Distrib 10.1.28-MariaDB, for Win32 (AMD64)
--
-- Host: 192.168.1.100    Database: ese_acrec
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `cuenta_dependientes`
--

DROP TABLE IF EXISTS `cuenta_dependientes`;
/*!50001 DROP VIEW IF EXISTS `cuenta_dependientes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `cuenta_dependientes` (
  `dependientes` tinyint NOT NULL,
  `estudio` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `cuenta_ingresos`
--

DROP TABLE IF EXISTS `cuenta_ingresos`;
/*!50001 DROP VIEW IF EXISTS `cuenta_ingresos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `cuenta_ingresos` (
  `estudio` tinyint NOT NULL,
  `ingresos` tinyint NOT NULL,
  `edad` tinyint NOT NULL,
  `avg_parentesco` tinyint NOT NULL,
  `ocupacion` tinyint NOT NULL,
  `tipo` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `cuenta_integrantes`
--

DROP TABLE IF EXISTS `cuenta_integrantes`;
/*!50001 DROP VIEW IF EXISTS `cuenta_integrantes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `cuenta_integrantes` (
  `estudio` tinyint NOT NULL,
  `integrantes` tinyint NOT NULL,
  `edad` tinyint NOT NULL,
  `avg_parentesco` tinyint NOT NULL,
  `institucion_academica` tinyint NOT NULL,
  `ocupacion` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (1,'contenttypes','0001_initial','2019-04-04 18:55:03.771884'),(2,'auth','0001_initial','2019-04-04 18:55:04.209339'),(3,'admin','0001_initial','2019-04-04 18:55:04.309646'),(4,'admin','0002_logentry_remove_auto_add','2019-04-04 18:55:04.325270'),(5,'admin','0003_logentry_add_action_flag_choices','2019-04-04 18:55:04.356518'),(6,'contenttypes','0002_remove_content_type_name','2019-04-04 18:55:04.422264'),(7,'auth','0002_alter_permission_name_max_length','2019-04-04 18:55:04.470236'),(8,'auth','0003_alter_user_email_max_length','2019-04-04 18:55:04.507215'),(9,'auth','0004_alter_user_username_opts','2019-04-04 18:55:04.517209'),(10,'auth','0005_alter_user_last_login_null','2019-04-04 18:55:04.563467'),(11,'auth','0006_require_contenttypes_0002','2019-04-04 18:55:04.563467'),(12,'auth','0007_alter_validators_add_error_messages','2019-04-04 18:55:04.579092'),(13,'auth','0008_alter_user_username_max_length','2019-04-04 18:55:04.609732'),(14,'auth','0009_alter_user_last_name_max_length','2019-04-04 18:55:04.651287'),(15,'sessions','0001_initial','2019-04-04 18:55:04.694277');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ese_house_price`
--

DROP TABLE IF EXISTS `ese_house_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ese_house_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `value` float DEFAULT NULL,
  `quanta` int(11) DEFAULT NULL,
  `final_price` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ese_house_price`
--

LOCK TABLES `ese_house_price` WRITE;
/*!40000 ALTER TABLE `ese_house_price` DISABLE KEYS */;
INSERT INTO `ese_house_price` (`id`, `date`, `value`, `quanta`, `final_price`) VALUES (1,'2019-04-04 15:32:00',50000,0,50000),(2,'2019-04-04 15:32:00',50000,1,100000),(3,'2019-04-04 15:32:00',50000,2,150000),(4,'2019-04-04 15:32:00',50000,3,200000),(5,'2019-04-04 15:32:00',50000,4,250000),(6,'2019-04-04 15:32:00',50000,15,800000);
/*!40000 ALTER TABLE `ese_house_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `int_edades`
--

DROP TABLE IF EXISTS `int_edades`;
/*!50001 DROP VIEW IF EXISTS `int_edades`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `int_edades` (
  `edad` tinyint NOT NULL,
  `id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `normalizados`
--

DROP TABLE IF EXISTS `normalizados`;
/*!50001 DROP VIEW IF EXISTS `normalizados`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `normalizados` (
  `id` tinyint NOT NULL,
  `entrenamiento` tinyint NOT NULL,
  `edad` tinyint NOT NULL,
  `sexo` tinyint NOT NULL,
  `escolaridad` tinyint NOT NULL,
  `estado_civil` tinyint NOT NULL,
  `ocupacion` tinyint NOT NULL,
  `codigo_postal` tinyint NOT NULL,
  `integrantes` tinyint NOT NULL,
  `dependientes` tinyint NOT NULL,
  `int_edad` tinyint NOT NULL,
  `int_parentesco` tinyint NOT NULL,
  `institucion_academica` tinyint NOT NULL,
  `int_ocupacion` tinyint NOT NULL,
  `clasificacion_ts` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `project_zarah_alimentos_list`
--

DROP TABLE IF EXISTS `project_zarah_alimentos_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_alimentos_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_alimentos_list`
--

LOCK TABLES `project_zarah_alimentos_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_alimentos_list` DISABLE KEYS */;
INSERT INTO `project_zarah_alimentos_list` (`id`, `texto`) VALUES (1,'POLLO'),(2,'RES'),(3,'CERDO'),(4,'PESCADO'),(5,'LECHE'),(6,'CEREALES'),(7,'HUEVO'),(8,'FRUTAS'),(9,'VERDURAS'),(10,'LEGUMINOSAS (FRIJOL, LENTEJA, HABA)'),(11,'OTROS');
/*!40000 ALTER TABLE `project_zarah_alimentos_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_beneficiario_list`
--

DROP TABLE IF EXISTS `project_zarah_beneficiario_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_beneficiario_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_beneficiario_list`
--

LOCK TABLES `project_zarah_beneficiario_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_beneficiario_list` DISABLE KEYS */;
INSERT INTO `project_zarah_beneficiario_list` (`id`, `texto`) VALUES (1,'Propio'),(2,'Conyugue'),(3,'Hijo'),(4,'Padres'),(5,'Escuela');
/*!40000 ALTER TABLE `project_zarah_beneficiario_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_caracteristicas_list`
--

DROP TABLE IF EXISTS `project_zarah_caracteristicas_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_caracteristicas_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_caracteristicas_list`
--

LOCK TABLES `project_zarah_caracteristicas_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_caracteristicas_list` DISABLE KEYS */;
INSERT INTO `project_zarah_caracteristicas_list` (`id`, `texto`) VALUES (1,'SALA'),(2,'COMEDOR'),(3,'COCINA'),(4,'RECAMARAS'),(5,'BAÑOS'),(6,'GARAGE'),(7,'JARDIN'),(8,'ALBERCA'),(9,'PATIO DE SERVICIO');
/*!40000 ALTER TABLE `project_zarah_caracteristicas_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_clasificaciones_list`
--

DROP TABLE IF EXISTS `project_zarah_clasificaciones_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_clasificaciones_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_clasificaciones_list`
--

LOCK TABLES `project_zarah_clasificaciones_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_clasificaciones_list` DISABLE KEYS */;
INSERT INTO `project_zarah_clasificaciones_list` (`id`, `texto`) VALUES (1,'A-'),(2,'A'),(3,'A+'),(4,'B'),(5,'B+'),(6,'C'),(7,'C+'),(8,'D'),(9,'D+');
/*!40000 ALTER TABLE `project_zarah_clasificaciones_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_dependiente_list`
--

DROP TABLE IF EXISTS `project_zarah_dependiente_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_dependiente_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_dependiente_list`
--

LOCK TABLES `project_zarah_dependiente_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_dependiente_list` DISABLE KEYS */;
INSERT INTO `project_zarah_dependiente_list` (`id`, `texto`) VALUES (1,'Sí'),(2,'No');
/*!40000 ALTER TABLE `project_zarah_dependiente_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_despensas_list`
--

DROP TABLE IF EXISTS `project_zarah_despensas_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_despensas_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_despensas_list`
--

LOCK TABLES `project_zarah_despensas_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_despensas_list` DISABLE KEYS */;
INSERT INTO `project_zarah_despensas_list` (`id`, `texto`) VALUES (1,'Diario'),(2,'Semanal'),(3,'Quincenal'),(4,'Mensual');
/*!40000 ALTER TABLE `project_zarah_despensas_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_discapacidad_list`
--

DROP TABLE IF EXISTS `project_zarah_discapacidad_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_discapacidad_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_discapacidad_list`
--

LOCK TABLES `project_zarah_discapacidad_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_discapacidad_list` DISABLE KEYS */;
INSERT INTO `project_zarah_discapacidad_list` (`id`, `texto`) VALUES (1,'Discapacidad motriz'),(2,'Discapacidad visual.'),(3,'Discapacidad mental.'),(4,'Discapacidad auditiva.'),(5,'Discapacidad para hablar o comunicarse.'),(6,'Discapacidad de atención y aprendizaje.'),(7,'Discapacidad de autocuidado.');
/*!40000 ALTER TABLE `project_zarah_discapacidad_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_egresos`
--

DROP TABLE IF EXISTS `project_zarah_egresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_egresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  `tipo_egreso_list_id` int(11) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `observacion` varchar(100) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_egresos`
--

LOCK TABLES `project_zarah_egresos` WRITE;
/*!40000 ALTER TABLE `project_zarah_egresos` DISABLE KEYS */;
INSERT INTO `project_zarah_egresos` (`id`, `estudio_socioeconomico_id`, `tipo_egreso_list_id`, `cantidad`, `observacion`, `fecha`) VALUES (2,1,6,5000,'NADA','2019-09-25 14:47:16'),(4,2,1,90,'cada 2 meses','2019-08-20 16:16:22'),(5,2,2,175,'cada 2 meses','2019-08-20 16:16:29'),(6,2,3,150,'recargas al celular, telefono de casa lo paga su hijo','2019-08-20 16:07:23'),(7,2,9,83,'lo paga su hijo (se lo presta su suegro)','2019-08-20 16:07:47'),(8,2,5,620,'gasolina/pasajes','2019-08-20 16:08:01'),(9,2,6,1650,'incluye despensa','2019-08-20 16:08:21'),(10,2,7,83,'cada año','2019-08-20 16:08:32'),(11,2,10,950,'seguro de vida, alimento para mascotas, gas','2019-08-20 16:08:53'),(12,39,1,120,'CADA BIMESTRE','2019-09-18 19:12:41'),(13,1,9,3000,'Renta','2019-09-18 19:32:14'),(14,1,1,120,'Cada mes','2019-09-18 19:34:58'),(15,1,4,80,'levotiroxina','2019-09-18 19:35:30'),(17,41,1,60,'','2019-09-24 14:46:22'),(18,41,2,600,'','2019-09-24 14:46:30'),(19,41,3,400,'recargas al celular','2019-09-24 14:46:56'),(20,41,5,1200,'','2019-09-24 14:47:13'),(21,41,6,6000,'','2019-09-24 14:47:27'),(22,41,7,166,'','2019-09-24 14:47:44'),(23,41,8,400,'','2019-09-24 14:47:54'),(24,41,10,345,'gas','2019-09-24 14:48:13'),(26,1,3,300,'Recargas','2019-09-25 15:05:55'),(32,1,5,800,'Gasolina + Transporte público','2019-09-25 16:30:31'),(39,1,8,1000,'Colegiatura','2019-09-25 17:15:25'),(40,1,7,50,'Dividido al año','2019-09-25 17:15:47');
/*!40000 ALTER TABLE `project_zarah_egresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_enfermedades_list`
--

DROP TABLE IF EXISTS `project_zarah_enfermedades_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_enfermedades_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_enfermedades_list`
--

LOCK TABLES `project_zarah_enfermedades_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_enfermedades_list` DISABLE KEYS */;
INSERT INTO `project_zarah_enfermedades_list` (`id`, `texto`) VALUES (1,'Diabetes'),(2,'Hipertensión Arterial (HAS)'),(3,'Artritis'),(4,'Glaucoma'),(5,'Cáncer'),(6,'Otra'),(7,'Ninguna');
/*!40000 ALTER TABLE `project_zarah_enfermedades_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_escolaridad_list`
--

DROP TABLE IF EXISTS `project_zarah_escolaridad_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_escolaridad_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_escolaridad_list`
--

LOCK TABLES `project_zarah_escolaridad_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_escolaridad_list` DISABLE KEYS */;
INSERT INTO `project_zarah_escolaridad_list` (`id`, `texto`) VALUES (1,'Primaria Trunca'),(2,'Primaria Concluida'),(3,'Secundaria Trunca'),(4,'Secundaria Concluida'),(5,'Bachillerato Trunco'),(6,'Bachillerato Concluido'),(7,'Carrera técnica Trunca'),(8,'Carrera técnica Concluida'),(9,'Licenciatura Trunca'),(10,'Licenciatura Concluida'),(11,'Maestría'),(12,'Doctorado'),(13,'Posdoctorado');
/*!40000 ALTER TABLE `project_zarah_escolaridad_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estado_civil_list`
--

DROP TABLE IF EXISTS `project_zarah_estado_civil_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estado_civil_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estado_civil_list`
--

LOCK TABLES `project_zarah_estado_civil_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_estado_civil_list` DISABLE KEYS */;
INSERT INTO `project_zarah_estado_civil_list` (`id`, `texto`) VALUES (1,'Soltero'),(2,'Casado'),(3,'Divorciado'),(4,'Unión Libre'),(5,'Separado'),(6,'Viudo');
/*!40000 ALTER TABLE `project_zarah_estado_civil_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estado_general_vivienda_list`
--

DROP TABLE IF EXISTS `project_zarah_estado_general_vivienda_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estado_general_vivienda_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estado_general_vivienda_list`
--

LOCK TABLES `project_zarah_estado_general_vivienda_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_estado_general_vivienda_list` DISABLE KEYS */;
INSERT INTO `project_zarah_estado_general_vivienda_list` (`id`, `texto`) VALUES (1,'Excelente'),(2,'Buena'),(3,'Regular'),(4,'Malo');
/*!40000 ALTER TABLE `project_zarah_estado_general_vivienda_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estudio_socioeconomico`
--

DROP TABLE IF EXISTS `project_zarah_estudio_socioeconomico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estudio_socioeconomico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `sexo_list_id` int(11) DEFAULT NULL,
  `escolaridad_list_id` int(11) DEFAULT NULL,
  `estado_civil_list_id` int(11) DEFAULT NULL,
  `codigo_postal` int(11) DEFAULT NULL,
  `calle` varchar(255) DEFAULT NULL,
  `colonia` varchar(100) DEFAULT NULL,
  `entre_calles` varchar(100) DEFAULT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `telefono_fijo` varchar(20) DEFAULT NULL,
  `telefono_celular` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `apaterno` varchar(100) DEFAULT NULL,
  `amaterno` varchar(100) DEFAULT NULL,
  `nombres` varchar(100) DEFAULT NULL,
  `ocupaciones_list_id` int(11) DEFAULT NULL,
  `num_interior` varchar(45) DEFAULT NULL,
  `num_exterior` varchar(45) DEFAULT NULL,
  `numero_habitantes` int(11) DEFAULT NULL,
  `tipo_vivienda_list_id` int(11) DEFAULT NULL,
  `tipo_propiedad_list_id` int(11) DEFAULT NULL,
  `m2_terreno` int(11) DEFAULT NULL,
  `m2_construccion` int(11) DEFAULT NULL,
  `tiempo_residencia` int(11) DEFAULT NULL,
  `tipo_piso_list_id` int(11) DEFAULT NULL,
  `tipo_techo_list_id` int(11) DEFAULT NULL,
  `tipo_paredes_list_id` int(11) DEFAULT NULL,
  `higiene_list_id` int(11) DEFAULT NULL,
  `estado_general_vivienda_list_id` int(11) DEFAULT NULL,
  `discapacidad_list_id` int(11) DEFAULT NULL,
  `despensas_list_id` int(11) DEFAULT NULL,
  `beneficiario_list_id` int(11) DEFAULT NULL,
  `servicio_salud_list_id` int(11) DEFAULT NULL,
  `enfermedades_list_id` int(11) DEFAULT NULL,
  `paciente_id` int(11) DEFAULT NULL,
  `clasificacion_ts` int(11) DEFAULT NULL,
  `clasificacion_ia` int(11) DEFAULT NULL,
  `observaciones` text DEFAULT NULL,
  `diagnostico` text DEFAULT NULL,
  `falsea_datos` tinyint(1) DEFAULT NULL,
  `ref_domicilio` varchar(500) DEFAULT NULL,
  `entrenamiento` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estudio_socioeconomico`
--

LOCK TABLES `project_zarah_estudio_socioeconomico` WRITE;
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico` DISABLE KEYS */;
INSERT INTO `project_zarah_estudio_socioeconomico` (`id`, `clave`, `fecha_nacimiento`, `fecha`, `sexo_list_id`, `escolaridad_list_id`, `estado_civil_list_id`, `codigo_postal`, `calle`, `colonia`, `entre_calles`, `localidad`, `telefono_fijo`, `telefono_celular`, `email`, `apaterno`, `amaterno`, `nombres`, `ocupaciones_list_id`, `num_interior`, `num_exterior`, `numero_habitantes`, `tipo_vivienda_list_id`, `tipo_propiedad_list_id`, `m2_terreno`, `m2_construccion`, `tiempo_residencia`, `tipo_piso_list_id`, `tipo_techo_list_id`, `tipo_paredes_list_id`, `higiene_list_id`, `estado_general_vivienda_list_id`, `discapacidad_list_id`, `despensas_list_id`, `beneficiario_list_id`, `servicio_salud_list_id`, `enfermedades_list_id`, `paciente_id`, `clasificacion_ts`, `clasificacion_ia`, `observaciones`, `diagnostico`, `falsea_datos`, `ref_domicilio`, `entrenamiento`) VALUES (1,'CLI1605945','1995-05-10','2019-09-25 19:52:17',2,10,1,62320,'4A PRIVADA FRANCISCO VILLA','ANTONIO BARONA','EMILIANO ZAPATA Y FRANCISCO VILLA','CUERNAVACA','7779876543','7775956923','angelzuriel.barriosflores@gmail.com','BARRIOS','FLORES','ANGEL ZURIEL',5,'4','23',5,3,2,50,50,30,2,1,1,2,2,2,4,1,2,6,109450,5,NULL,'HOLIII','HOLIII',0,'POR LA CALLE, GIRAR A LA OTRA CALLE Y ENTRAR POR LA OTRA CALLE. TOCAR EN LA PUERTA.',1),(2,'CLI1305122','1948-02-28','2019-09-26 17:01:54',2,6,3,62460,'CLAVEL','SATELITE','TULIPAN Y GLADIOLA','CUERNAVACA','7775619088','7773150833','','MARTINEZ','MARTINEZ','RUFINO',2,'','19',2,1,5,800,600,35,3,1,4,1,1,NULL,4,2,3,4,77799,NULL,NULL,NULL,NULL,0,NULL,1),(41,'','1999-01-24','2019-09-26 16:56:38',1,6,1,62950,'los pinos','cuauhtemoc','allende y eufemio zapata','axochiapan','','735-169-38-54','','CORTES','MORALES','ELDA DEYSI',7,'','0',10,1,5,100,90,20,2,1,4,1,2,NULL,2,NULL,2,7,141456,2,NULL,'','',0,NULL,NULL),(43,'','1961-11-03','2019-09-26 17:33:18',1,NULL,1,NULL,'','','','','','','','SANCHEZ','RANGEL','SILVIA',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,141489,NULL,NULL,'','',0,NULL,NULL),(44,'','2003-01-15','2019-09-26 17:33:51',2,NULL,1,NULL,'','','','','','','','ARTEAGA','ALBITER','KEVIN RUBEN',NULL,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,141734,NULL,NULL,'','',0,NULL,NULL);
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estudio_socioeconomico_alimentos`
--

DROP TABLE IF EXISTS `project_zarah_estudio_socioeconomico_alimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estudio_socioeconomico_alimentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  `alimentos_list_id` int(11) DEFAULT NULL,
  `frecuencias_list_id` int(11) DEFAULT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=364 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estudio_socioeconomico_alimentos`
--

LOCK TABLES `project_zarah_estudio_socioeconomico_alimentos` WRITE;
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_alimentos` DISABLE KEYS */;
INSERT INTO `project_zarah_estudio_socioeconomico_alimentos` (`id`, `estudio_socioeconomico_id`, `alimentos_list_id`, `frecuencias_list_id`, `observaciones`) VALUES (32,2,1,2,NULL),(33,2,2,6,NULL),(34,2,3,6,NULL),(35,2,4,4,NULL),(36,2,5,1,NULL),(37,2,6,3,NULL),(38,2,7,1,NULL),(39,2,8,1,NULL),(40,2,9,1,NULL),(41,2,10,2,NULL),(42,2,11,6,NULL),(262,41,1,2,NULL),(263,41,2,3,NULL),(264,41,3,3,NULL),(265,41,4,5,NULL),(266,41,5,3,NULL),(267,41,6,2,NULL),(268,41,7,2,NULL),(269,41,8,1,NULL),(270,41,9,1,NULL),(271,41,10,2,NULL),(272,41,11,5,NULL),(354,1,1,3,''),(355,1,2,3,''),(356,1,3,3,''),(357,1,4,5,''),(358,1,5,2,''),(359,1,6,5,''),(360,1,7,4,''),(361,1,8,4,''),(362,1,9,2,''),(363,1,10,1,'');
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_alimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estudio_socioeconomico_caracteristicas`
--

DROP TABLE IF EXISTS `project_zarah_estudio_socioeconomico_caracteristicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estudio_socioeconomico_caracteristicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caracteristicas_list_id` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estudio_socioeconomico_caracteristicas`
--

LOCK TABLES `project_zarah_estudio_socioeconomico_caracteristicas` WRITE;
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_caracteristicas` DISABLE KEYS */;
INSERT INTO `project_zarah_estudio_socioeconomico_caracteristicas` (`id`, `caracteristicas_list_id`, `cantidad`, `fecha`, `estudio_socioeconomico_id`) VALUES (46,1,1,'2019-08-20 16:36:26',2),(47,2,1,'2019-08-20 16:36:26',2),(48,3,1,'2019-08-20 16:36:26',2),(49,4,3,'2019-08-20 16:36:26',2),(50,5,2,'2019-08-20 16:36:26',2),(51,6,1,'2019-08-20 16:36:26',2),(52,7,1,'2019-08-20 16:36:26',2),(53,8,1,'2019-08-20 16:36:26',2),(54,9,1,'2019-08-20 16:36:26',2),(91,1,0,'2019-09-19 19:19:45',1),(92,2,1,'2019-09-19 19:19:45',1),(93,3,1,'2019-09-19 19:19:45',1),(94,4,3,'2019-09-19 19:19:45',1),(95,5,1,'2019-09-19 19:19:45',1),(96,6,0,'2019-09-19 19:19:45',1),(97,7,1,'2019-09-19 19:19:45',1),(98,8,0,'2019-09-19 19:19:45',1),(99,9,0,'2019-09-19 19:19:45',1),(100,1,0,'2019-09-24 14:51:08',41),(101,2,1,'2019-09-24 14:51:08',41),(102,3,1,'2019-09-24 14:51:08',41),(103,4,3,'2019-09-24 14:51:08',41),(104,5,1,'2019-09-24 14:51:08',41),(105,6,0,'2019-09-24 14:51:08',41),(106,7,0,'2019-09-24 14:51:08',41),(107,8,0,'2019-09-24 14:51:08',41),(108,9,0,'2019-09-24 14:51:08',41);
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_caracteristicas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estudio_socioeconomico_manaje`
--

DROP TABLE IF EXISTS `project_zarah_estudio_socioeconomico_manaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estudio_socioeconomico_manaje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manaje_list_id` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estudio_socioeconomico_manaje`
--

LOCK TABLES `project_zarah_estudio_socioeconomico_manaje` WRITE;
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_manaje` DISABLE KEYS */;
INSERT INTO `project_zarah_estudio_socioeconomico_manaje` (`id`, `manaje_list_id`, `cantidad`, `estudio_socioeconomico_id`) VALUES (56,1,2,2),(57,2,0,2),(58,3,0,2),(59,4,1,2),(60,5,1,2),(61,6,0,2),(62,7,1,2),(63,8,1,2),(64,9,1,2),(65,10,0,2),(66,11,0,2),(67,12,1,2),(68,13,1,2),(121,1,2,1),(122,2,0,1),(123,3,2,1),(124,4,1,1),(125,5,1,1),(126,6,0,1),(127,7,0,1),(128,8,1,1),(129,9,2,1),(130,10,0,1),(131,11,1,1),(132,12,1,1),(133,13,1,1),(134,1,1,41),(135,2,0,41),(136,3,1,41),(137,4,1,41),(138,5,0,41),(139,6,0,41),(140,7,0,41),(141,8,0,41),(142,9,0,41),(143,10,0,41),(144,11,0,41),(145,12,1,41),(146,13,1,41);
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_manaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_estudio_socioeconomico_servicios`
--

DROP TABLE IF EXISTS `project_zarah_estudio_socioeconomico_servicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_estudio_socioeconomico_servicios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servicios_list_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_estudio_socioeconomico_servicios`
--

LOCK TABLES `project_zarah_estudio_socioeconomico_servicios` WRITE;
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_servicios` DISABLE KEYS */;
INSERT INTO `project_zarah_estudio_socioeconomico_servicios` (`id`, `servicios_list_id`, `fecha`, `estudio_socioeconomico_id`) VALUES (79,1,'2019-08-20 16:36:26',2),(80,2,'2019-08-20 16:36:26',2),(81,4,'2019-08-20 16:36:26',2),(82,5,'2019-08-20 16:36:26',2),(83,6,'2019-08-20 16:36:26',2),(84,7,'2019-08-20 16:36:26',2),(85,8,'2019-08-20 16:36:26',2),(86,12,'2019-08-20 16:36:26',2),(123,1,'2019-09-19 19:19:45',1),(124,2,'2019-09-19 19:19:45',1),(125,3,'2019-09-19 19:19:45',1),(126,4,'2019-09-19 19:19:45',1),(127,5,'2019-09-19 19:19:45',1),(128,6,'2019-09-19 19:19:45',1),(129,7,'2019-09-19 19:19:45',1),(130,8,'2019-09-19 19:19:45',1),(131,12,'2019-09-19 19:19:45',1),(132,1,'2019-09-24 14:51:08',41),(133,2,'2019-09-24 14:51:08',41),(134,4,'2019-09-24 14:51:08',41),(135,5,'2019-09-24 14:51:08',41),(136,7,'2019-09-24 14:51:08',41),(137,8,'2019-09-24 14:51:08',41);
/*!40000 ALTER TABLE `project_zarah_estudio_socioeconomico_servicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_frecuencias_list`
--

DROP TABLE IF EXISTS `project_zarah_frecuencias_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_frecuencias_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_frecuencias_list`
--

LOCK TABLES `project_zarah_frecuencias_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_frecuencias_list` DISABLE KEYS */;
INSERT INTO `project_zarah_frecuencias_list` (`id`, `texto`) VALUES (1,'Diario'),(2,'Cada tercer día'),(3,'Una vez por semana'),(4,'Una vez al mes'),(5,'Ocasionalmente'),(6,'Nunca');
/*!40000 ALTER TABLE `project_zarah_frecuencias_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_higiene_list`
--

DROP TABLE IF EXISTS `project_zarah_higiene_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_higiene_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_higiene_list`
--

LOCK TABLES `project_zarah_higiene_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_higiene_list` DISABLE KEYS */;
INSERT INTO `project_zarah_higiene_list` (`id`, `texto`) VALUES (1,'Buena'),(2,'Regular'),(3,'Mala');
/*!40000 ALTER TABLE `project_zarah_higiene_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_ingresos`
--

DROP TABLE IF EXISTS `project_zarah_ingresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_ingresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `ingreso` double DEFAULT NULL,
  `ocupaciones_list_id` int(11) DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `ingresos_tipo_list_id` int(11) DEFAULT NULL,
  `parentesco_list_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_ingresos`
--

LOCK TABLES `project_zarah_ingresos` WRITE;
/*!40000 ALTER TABLE `project_zarah_ingresos` DISABLE KEYS */;
INSERT INTO `project_zarah_ingresos` (`id`, `nombre`, `edad`, `ingreso`, `ocupaciones_list_id`, `estudio_socioeconomico_id`, `fecha`, `ingresos_tipo_list_id`, `parentesco_list_id`) VALUES (11,'ISABEL BARRIOS FLORES',24,6000,5,1,'2019-09-25 18:00:34',1,1),(16,'ORLANDO MARTINEZ SANCHEZ',37,1000,12,2,'2019-08-16 16:41:04',1,5),(17,'YARENI YADIRA MARTINEZ SANCHEZ',36,1000,12,2,'2019-08-16 16:41:32',2,5),(18,'NIDIA MARTINEZ SANCHEZ',39,1000,6,2,'2019-08-16 16:41:54',2,5),(19,'PENSIÓN',0,3500,NULL,2,'2019-08-16 16:42:52',6,NULL),(20,'60+',0,1250,NULL,2,'2019-08-16 16:43:16',4,NULL),(22,'BARRIOS FLORES ANGEL ZURIEL',24,6000,5,1,'2019-09-25 18:00:39',6,6),(23,'',49,2000,10,41,'2019-09-24 14:43:49',1,1),(24,'',51,2400,NULL,41,'2019-09-24 14:44:06',NULL,2),(25,'',32,2400,NULL,41,'2019-09-24 14:44:27',NULL,3),(26,'',26,2000,NULL,41,'2019-09-24 14:44:41',NULL,NULL),(27,'',20,2400,NULL,41,'2019-09-24 14:45:47',5,NULL);
/*!40000 ALTER TABLE `project_zarah_ingresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_ingresos_tipo_list`
--

DROP TABLE IF EXISTS `project_zarah_ingresos_tipo_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_ingresos_tipo_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_ingresos_tipo_list`
--

LOCK TABLES `project_zarah_ingresos_tipo_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_ingresos_tipo_list` DISABLE KEYS */;
INSERT INTO `project_zarah_ingresos_tipo_list` (`id`, `texto`) VALUES (1,'Familiar'),(2,'Familiar Externo'),(3,'Otro Externo'),(4,'Gubernamental'),(5,'Estudiante Beca'),(6,'Propio');
/*!40000 ALTER TABLE `project_zarah_ingresos_tipo_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_institucion_academica_list`
--

DROP TABLE IF EXISTS `project_zarah_institucion_academica_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_institucion_academica_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_institucion_academica_list`
--

LOCK TABLES `project_zarah_institucion_academica_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_institucion_academica_list` DISABLE KEYS */;
INSERT INTO `project_zarah_institucion_academica_list` (`id`, `texto`) VALUES (1,'Pública'),(2,'Privada');
/*!40000 ALTER TABLE `project_zarah_institucion_academica_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_integrantes_hogar`
--

DROP TABLE IF EXISTS `project_zarah_integrantes_hogar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_integrantes_hogar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `dependiente_list_id` int(11) DEFAULT NULL,
  `parentesco_list_id` int(11) DEFAULT NULL,
  `institucion_academica_list_id` int(11) DEFAULT NULL,
  `ocupaciones_list_id` int(11) DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `discapacidad_list_id` int(11) DEFAULT NULL,
  `trabajo` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_integrantes_hogar`
--

LOCK TABLES `project_zarah_integrantes_hogar` WRITE;
/*!40000 ALTER TABLE `project_zarah_integrantes_hogar` DISABLE KEYS */;
INSERT INTO `project_zarah_integrantes_hogar` (`id`, `nombre`, `edad`, `dependiente_list_id`, `parentesco_list_id`, `institucion_academica_list_id`, `ocupaciones_list_id`, `estudio_socioeconomico_id`, `fecha`, `discapacidad_list_id`, `trabajo`) VALUES (5,'JESUS BARRIOS VERGARA',73,2,10,1,4,1,'2019-09-25 17:59:29',NULL,'Albañilería y oficios varios eventuales'),(9,'ISABEL BARRIOS FLORES',49,2,43,1,5,1,'2019-09-25 17:59:33',NULL,''),(12,'YAZMIN ALEXANDRA BARRIOS FLORES',16,1,5,1,7,1,'2019-09-25 17:59:38',NULL,''),(16,'VIRGINIA FLORES BENITEZ',68,2,9,1,1,1,'2019-09-25 17:59:47',4,''),(17,'ORLANDO MARTINEZ MARTINEZ',37,2,5,1,12,2,'2019-08-16 16:40:15',NULL,NULL),(18,'',49,2,1,NULL,10,41,'2019-09-24 14:43:09',NULL,NULL),(19,'',51,2,2,NULL,10,41,'2019-09-24 14:34:45',NULL,NULL),(20,'',22,1,3,1,16,41,'2019-09-24 14:35:17',NULL,NULL),(21,'',18,1,3,1,16,41,'2019-09-24 14:35:39',NULL,NULL),(23,'',26,2,3,1,10,41,'2019-09-24 14:36:26',NULL,NULL),(24,'',7,1,3,1,16,41,'2019-09-24 14:36:42',NULL,NULL),(26,'',40,1,6,NULL,8,41,'2019-09-24 14:40:47',NULL,NULL),(27,'',32,2,3,NULL,10,41,'2019-09-24 14:41:23',NULL,NULL),(28,'',74,1,6,NULL,6,41,'2019-09-24 14:42:54',NULL,NULL);
/*!40000 ALTER TABLE `project_zarah_integrantes_hogar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_manaje_list`
--

DROP TABLE IF EXISTS `project_zarah_manaje_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_manaje_list` (
  `id` int(11) NOT NULL,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_manaje_list`
--

LOCK TABLES `project_zarah_manaje_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_manaje_list` DISABLE KEYS */;
INSERT INTO `project_zarah_manaje_list` (`id`, `texto`) VALUES (1,'TELEVISIÓN O PLASMA'),(2,'REPRODUCTOR DE DVD O BLU RAY'),(3,'EQUIPO DE SONIDO'),(4,'REFRIGERADOR'),(5,'HORNO DE MICROONDAS'),(6,'LAVAVAJILLAS'),(7,'CAFETERA'),(8,'LAVADORA'),(9,'COMPUTADORA'),(10,'CENTRO DE LAVADO'),(11,'JUEGOS DE VIDEO'),(12,'ESTUFA'),(13,'LICUADORA');
/*!40000 ALTER TABLE `project_zarah_manaje_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_model_def`
--

DROP TABLE IF EXISTS `project_zarah_model_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_model_def` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_optimizer` varchar(100) DEFAULT NULL,
  `model_loss` varchar(100) DEFAULT NULL,
  `model_trained` tinyint(1) DEFAULT NULL,
  `model_name` varchar(45) DEFAULT NULL,
  `model_error` double DEFAULT NULL,
  `model_acc` double DEFAULT NULL,
  `model_dir` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_model_def`
--

LOCK TABLES `project_zarah_model_def` WRITE;
/*!40000 ALTER TABLE `project_zarah_model_def` DISABLE KEYS */;
INSERT INTO `project_zarah_model_def` (`id`, `model_optimizer`, `model_loss`, `model_trained`, `model_name`, `model_error`, `model_acc`, `model_dir`) VALUES (1,'sgd','mse',1,'project_zarah',0.00019225022697355598,0.9998077497730264,'project_zarah/cp.ckpt'),(2,NULL,NULL,0,'zarah_speech',0,0,'project_zarah/speech.ckpt');
/*!40000 ALTER TABLE `project_zarah_model_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_model_layers`
--

DROP TABLE IF EXISTS `project_zarah_model_layers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_model_layers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layer_units` int(11) DEFAULT NULL,
  `layer_activation` varchar(100) DEFAULT NULL,
  `layer_model_def_id` int(11) DEFAULT NULL,
  `layer_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_model_layers`
--

LOCK TABLES `project_zarah_model_layers` WRITE;
/*!40000 ALTER TABLE `project_zarah_model_layers` DISABLE KEYS */;
INSERT INTO `project_zarah_model_layers` (`id`, `layer_units`, `layer_activation`, `layer_model_def_id`, `layer_order`) VALUES (28,200,'relu',1,NULL),(29,200,'relu',1,NULL),(30,1,'sigmoid',1,NULL);
/*!40000 ALTER TABLE `project_zarah_model_layers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_ocupaciones_list`
--

DROP TABLE IF EXISTS `project_zarah_ocupaciones_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_ocupaciones_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(100) DEFAULT NULL,
  `observacion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_ocupaciones_list`
--

LOCK TABLES `project_zarah_ocupaciones_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_ocupaciones_list` DISABLE KEYS */;
INSERT INTO `project_zarah_ocupaciones_list` (`id`, `texto`, `observacion`) VALUES (1,'Desempleado (Más de 3 meses sin trabajo) / Adultos con programas de gobierno',NULL),(2,'Jubilado / Pensionado',NULL),(3,'Subempleado',NULL),(4,'Obrero',NULL),(5,'Empleados (Asalariado y Técnico)','Solo menores de 5 años'),(6,'Profesionista, empresario o ejecutivo',NULL),(7,'Estudiante','Solo menores de 16 años que no trabajan ni estudian');
/*!40000 ALTER TABLE `project_zarah_ocupaciones_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_parentesco_list`
--

DROP TABLE IF EXISTS `project_zarah_parentesco_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_parentesco_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_parentesco_list`
--

LOCK TABLES `project_zarah_parentesco_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_parentesco_list` DISABLE KEYS */;
INSERT INTO `project_zarah_parentesco_list` (`id`, `texto`) VALUES (1,'Esposa'),(2,'Esposo'),(3,'Hija'),(4,'Hijo'),(5,'Hermana'),(6,'Hermano'),(7,'Prima'),(8,'Primo'),(9,'Abuela'),(10,'Abuelo'),(11,'Tía'),(12,'Tío'),(13,'Sobrina'),(14,'Sobrino'),(15,'Madrastra'),(16,'Padrastro'),(17,'Hermanastra'),(18,'Hermanastro'),(19,'Concubina'),(20,'Concubino'),(21,'Cuñada'),(22,'Cuñado'),(23,'Amiga'),(24,'Amigo'),(25,'Bisabuela'),(26,'Bisabuelo'),(27,'Concuña'),(28,'Concuño'),(29,'Nieta'),(30,'Nieto'),(31,'Bisnieta'),(32,'Bisnieto'),(33,'Tataranieta'),(34,'Tataranieto'),(35,'Tatarabuela'),(36,'Tatarabuelo'),(37,'Hija Adoptiva'),(38,'Hijo Adoptivo'),(39,'Nuera'),(40,'Yerno'),(41,'Suegra'),(42,'Suegro'),(43,'Mamá'),(44,'Papá');
/*!40000 ALTER TABLE `project_zarah_parentesco_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_servicio_salud_list`
--

DROP TABLE IF EXISTS `project_zarah_servicio_salud_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_servicio_salud_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_servicio_salud_list`
--

LOCK TABLES `project_zarah_servicio_salud_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_servicio_salud_list` DISABLE KEYS */;
INSERT INTO `project_zarah_servicio_salud_list` (`id`, `texto`) VALUES (1,'Seguro popular'),(2,'IMSS-ISSSTE'),(3,'PEMEX, SEDENA o particulares'),(4,'Seguro de Gastos Médicos'),(5,'Farmacias'),(6,'Sin acceso (literalmente no puede atenderse)'),(7,'Otros (Curandero, homeopatía)');
/*!40000 ALTER TABLE `project_zarah_servicio_salud_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_servicios_list`
--

DROP TABLE IF EXISTS `project_zarah_servicios_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_servicios_list` (
  `id` int(11) NOT NULL,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_servicios_list`
--

LOCK TABLES `project_zarah_servicios_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_servicios_list` DISABLE KEYS */;
INSERT INTO `project_zarah_servicios_list` (`id`, `texto`) VALUES (1,'Agua'),(2,'Luz'),(3,'Internet'),(4,'Drenaje'),(5,'Gas'),(6,'Teléfono fijo'),(7,'Teléfono celular'),(8,'Televisión abierta'),(9,'Televisión de paga'),(10,'Vigilancia'),(11,'Servicio de limpieza'),(12,'Pavimentación');
/*!40000 ALTER TABLE `project_zarah_servicios_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_sexo_list`
--

DROP TABLE IF EXISTS `project_zarah_sexo_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_sexo_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_sexo_list`
--

LOCK TABLES `project_zarah_sexo_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_sexo_list` DISABLE KEYS */;
INSERT INTO `project_zarah_sexo_list` (`id`, `texto`) VALUES (1,'Femenino'),(2,'Masculino');
/*!40000 ALTER TABLE `project_zarah_sexo_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_egreso_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_egreso_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_egreso_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_egreso_list`
--

LOCK TABLES `project_zarah_tipo_egreso_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_egreso_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_egreso_list` (`id`, `texto`) VALUES (1,'Agua'),(2,'Luz'),(3,'Telefono'),(4,'Salud'),(5,'Transporte'),(6,'Alimentación'),(7,'Ropa y calzado'),(8,'Educación'),(9,'Vivienda'),(10,'Otros');
/*!40000 ALTER TABLE `project_zarah_tipo_egreso_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_paredes_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_paredes_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_paredes_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_paredes_list`
--

LOCK TABLES `project_zarah_tipo_paredes_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_paredes_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_paredes_list` (`id`, `texto`) VALUES (1,'Ladrillo'),(2,'Adobe'),(3,'Madera'),(4,'Block'),(5,'Cartón'),(6,'Piedra');
/*!40000 ALTER TABLE `project_zarah_tipo_paredes_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_piso_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_piso_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_piso_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_piso_list`
--

LOCK TABLES `project_zarah_tipo_piso_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_piso_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_piso_list` (`id`, `texto`) VALUES (1,'Tierra'),(2,'Cemento o Firme'),(3,'Loseta o Mosaico'),(4,'Madera');
/*!40000 ALTER TABLE `project_zarah_tipo_piso_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_propiedad_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_propiedad_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_propiedad_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_propiedad_list`
--

LOCK TABLES `project_zarah_tipo_propiedad_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_propiedad_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_propiedad_list` (`id`, `texto`) VALUES (1,'Propia'),(2,'Rentada'),(3,'Hipotecada'),(4,'Renta Congelada'),(5,'Prestada'),(6,'Familiar');
/*!40000 ALTER TABLE `project_zarah_tipo_propiedad_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_techo_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_techo_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_techo_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_techo_list`
--

LOCK TABLES `project_zarah_tipo_techo_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_techo_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_techo_list` (`id`, `texto`) VALUES (1,'Loza concreto'),(2,'Lamina de cartón'),(3,'Lámina de Asbesto'),(4,'Palma, paja o madera'),(5,'teja'),(6,'Lamina galbanizada');
/*!40000 ALTER TABLE `project_zarah_tipo_techo_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_tipo_vivienda_list`
--

DROP TABLE IF EXISTS `project_zarah_tipo_vivienda_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_tipo_vivienda_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_tipo_vivienda_list`
--

LOCK TABLES `project_zarah_tipo_vivienda_list` WRITE;
/*!40000 ALTER TABLE `project_zarah_tipo_vivienda_list` DISABLE KEYS */;
INSERT INTO `project_zarah_tipo_vivienda_list` (`id`, `texto`) VALUES (1,'Casa Sola'),(2,'Duplex'),(3,'Condominio'),(4,'Departamento'),(5,'Vecindad'),(6,'Casa de Huéspedes');
/*!40000 ALTER TABLE `project_zarah_tipo_vivienda_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_zarah_vehiculos`
--

DROP TABLE IF EXISTS `project_zarah_vehiculos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_zarah_vehiculos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(50) DEFAULT NULL,
  `modelo` varchar(10) DEFAULT NULL,
  `propietario` varchar(100) DEFAULT NULL,
  `valor_actual` varchar(100) DEFAULT NULL,
  `pagado` tinyint(1) DEFAULT NULL,
  `adeudo` varchar(100) DEFAULT NULL,
  `estudio_socioeconomico_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_zarah_vehiculos`
--

LOCK TABLES `project_zarah_vehiculos` WRITE;
/*!40000 ALTER TABLE `project_zarah_vehiculos` DISABLE KEYS */;
INSERT INTO `project_zarah_vehiculos` (`id`, `marca`, `modelo`, `propietario`, `valor_actual`, `pagado`, `adeudo`, `estudio_socioeconomico_id`) VALUES (1,'CHEVROLET MERIVA','2004','ISABEL BARRIOS FLORES','60000',1,'0',2),(3,'CHEVROLET MERIVA','2004','ISABEL BARRIOS FLORES','60000',1,'0',1);
/*!40000 ALTER TABLE `project_zarah_vehiculos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `cuenta_dependientes`
--

/*!50001 DROP TABLE IF EXISTS `cuenta_dependientes`*/;
/*!50001 DROP VIEW IF EXISTS `cuenta_dependientes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`prueba`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `cuenta_dependientes` AS select count(distinct `project_zarah_integrantes_hogar`.`id`) AS `dependientes`,`project_zarah_integrantes_hogar`.`estudio_socioeconomico_id` AS `estudio` from `project_zarah_integrantes_hogar` where `project_zarah_integrantes_hogar`.`dependiente_list_id` = 1 group by `project_zarah_integrantes_hogar`.`estudio_socioeconomico_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `cuenta_ingresos`
--

/*!50001 DROP TABLE IF EXISTS `cuenta_ingresos`*/;
/*!50001 DROP VIEW IF EXISTS `cuenta_ingresos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`prueba`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `cuenta_ingresos` AS select `project_zarah_ingresos`.`estudio_socioeconomico_id` AS `estudio`,count(distinct `project_zarah_ingresos`.`id`) AS `ingresos`,avg(distinct `project_zarah_ingresos`.`edad`) AS `edad`,avg(distinct `project_zarah_ingresos`.`parentesco_list_id`) AS `avg_parentesco`,avg(distinct `project_zarah_ingresos`.`ocupaciones_list_id`) AS `ocupacion`,avg(distinct `project_zarah_ingresos`.`ingresos_tipo_list_id`) AS `tipo` from `project_zarah_ingresos` group by `project_zarah_ingresos`.`estudio_socioeconomico_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `cuenta_integrantes`
--

/*!50001 DROP TABLE IF EXISTS `cuenta_integrantes`*/;
/*!50001 DROP VIEW IF EXISTS `cuenta_integrantes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`prueba`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `cuenta_integrantes` AS select `project_zarah_integrantes_hogar`.`estudio_socioeconomico_id` AS `estudio`,count(distinct `project_zarah_integrantes_hogar`.`id`) AS `integrantes`,avg(distinct `project_zarah_integrantes_hogar`.`edad`) AS `edad`,avg(distinct `project_zarah_integrantes_hogar`.`parentesco_list_id`) AS `avg_parentesco`,avg(distinct `project_zarah_integrantes_hogar`.`institucion_academica_list_id`) AS `institucion_academica`,avg(distinct `project_zarah_integrantes_hogar`.`ocupaciones_list_id`) AS `ocupacion` from `project_zarah_integrantes_hogar` group by `project_zarah_integrantes_hogar`.`estudio_socioeconomico_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `int_edades`
--

/*!50001 DROP TABLE IF EXISTS `int_edades`*/;
/*!50001 DROP VIEW IF EXISTS `int_edades`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`prueba`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `int_edades` AS select round((to_days(curdate()) - to_days(`project_zarah_estudio_socioeconomico`.`fecha_nacimiento`)) / 365.25,0) AS `edad`,`project_zarah_estudio_socioeconomico`.`id` AS `id` from `project_zarah_estudio_socioeconomico` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `normalizados`
--

/*!50001 DROP TABLE IF EXISTS `normalizados`*/;
/*!50001 DROP VIEW IF EXISTS `normalizados`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`prueba`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `normalizados` AS select `project_zarah_estudio_socioeconomico`.`id` AS `id`,`project_zarah_estudio_socioeconomico`.`entrenamiento` AS `entrenamiento`,coalesce(((select `int_edades`.`edad` from `int_edades` where `int_edades`.`id` = `project_zarah_estudio_socioeconomico`.`id`) - (select min(`int_edades`.`edad`) from `int_edades`)) / ((select max(`int_edades`.`edad`) from `int_edades`) - (select min(`int_edades`.`edad`) from `int_edades`)),0) AS `edad`,coalesce((`project_zarah_estudio_socioeconomico`.`sexo_list_id` - (select min(`project_zarah_sexo_list`.`id`) from `project_zarah_sexo_list`)) / ((select max(`project_zarah_sexo_list`.`id`) from `project_zarah_sexo_list`) - (select min(`project_zarah_sexo_list`.`id`) from `project_zarah_sexo_list`)),0) AS `sexo`,coalesce((`project_zarah_estudio_socioeconomico`.`escolaridad_list_id` - (select min(`project_zarah_escolaridad_list`.`id`) from `project_zarah_escolaridad_list`)) / ((select max(`project_zarah_escolaridad_list`.`id`) from `project_zarah_escolaridad_list`) - (select min(`project_zarah_escolaridad_list`.`id`) from `project_zarah_escolaridad_list`)),0) AS `escolaridad`,coalesce((`project_zarah_estudio_socioeconomico`.`estado_civil_list_id` - (select min(`project_zarah_estado_civil_list`.`id`) from `project_zarah_estado_civil_list`)) / ((select max(`project_zarah_estado_civil_list`.`id`) from `project_zarah_estado_civil_list`) - (select min(`project_zarah_estado_civil_list`.`id`) from `project_zarah_estado_civil_list`)),0) AS `estado_civil`,coalesce((`project_zarah_estudio_socioeconomico`.`ocupaciones_list_id` - (select min(`project_zarah_ocupaciones_list`.`id`) from `project_zarah_ocupaciones_list`)) / ((select max(`project_zarah_ocupaciones_list`.`id`) from `project_zarah_ocupaciones_list`) - (select min(`project_zarah_ocupaciones_list`.`id`) from `project_zarah_ocupaciones_list`)),0) AS `ocupacion`,coalesce((`project_zarah_estudio_socioeconomico`.`codigo_postal` - (select min(`project_zarah_estudio_socioeconomico`.`codigo_postal`) from `project_zarah_estudio_socioeconomico`)) / ((select max(`project_zarah_estudio_socioeconomico`.`codigo_postal`) from `project_zarah_estudio_socioeconomico`) - (select min(`project_zarah_estudio_socioeconomico`.`codigo_postal`) from `project_zarah_estudio_socioeconomico`)),0) AS `codigo_postal`,coalesce(((select `cuenta_integrantes`.`integrantes` from `cuenta_integrantes` where `cuenta_integrantes`.`estudio` = `project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_integrantes`.`integrantes`) from `cuenta_integrantes`)) / ((select max(`cuenta_integrantes`.`integrantes`) from `cuenta_integrantes`) - (select min(`cuenta_integrantes`.`integrantes`) from `cuenta_integrantes`)),0) AS `integrantes`,coalesce(((select `cuenta_dependientes`.`dependientes` from `cuenta_dependientes` where `cuenta_dependientes`.`estudio` = `project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_dependientes`.`dependientes`) from `cuenta_dependientes`)) / ((select max(`cuenta_dependientes`.`dependientes`) from `cuenta_dependientes`) - (select min(`cuenta_dependientes`.`dependientes`) from `cuenta_dependientes`)),0) AS `dependientes`,coalesce(((select `cuenta_integrantes`.`edad` from `cuenta_integrantes` where `cuenta_integrantes`.`estudio` = `project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_integrantes`.`edad`) from `cuenta_integrantes`)) / ((select max(`cuenta_integrantes`.`edad`) from `cuenta_integrantes`) - (select min(`cuenta_integrantes`.`edad`) from `cuenta_integrantes`)),0) AS `int_edad`,((select `cuenta_integrantes`.`avg_parentesco` from `cuenta_integrantes` where `cuenta_integrantes`.`estudio` = `project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_integrantes`.`avg_parentesco`) from `cuenta_integrantes`)) / ((select max(`cuenta_integrantes`.`avg_parentesco`) from `cuenta_integrantes`) - (select min(`cuenta_integrantes`.`avg_parentesco`) from `cuenta_integrantes`)) AS `int_parentesco`,coalesce(((select `cuenta_integrantes`.`institucion_academica` from `cuenta_integrantes` where `cuenta_integrantes`.`estudio` = `project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_integrantes`.`institucion_academica`) from `cuenta_integrantes`)) / ((select max(`cuenta_integrantes`.`institucion_academica`) from `cuenta_integrantes`) - (select min(`cuenta_integrantes`.`institucion_academica`) from `cuenta_integrantes`)),0) AS `institucion_academica`,coalesce(((select `cuenta_integrantes`.`ocupacion` from `cuenta_integrantes` where `cuenta_integrantes`.`estudio` = `project_zarah_estudio_socioeconomico`.`id`) - (select min(`cuenta_integrantes`.`ocupacion`) from `cuenta_integrantes`)) / ((select max(`cuenta_integrantes`.`ocupacion`) from `cuenta_integrantes`) - (select min(`cuenta_integrantes`.`ocupacion`) from `cuenta_integrantes`)),0) AS `int_ocupacion`,coalesce((`project_zarah_estudio_socioeconomico`.`clasificacion_ts` - (select min(`project_zarah_clasificaciones_list`.`id`) from `project_zarah_clasificaciones_list`)) / ((select max(`project_zarah_clasificaciones_list`.`id`) from `project_zarah_clasificaciones_list`) - (select min(`project_zarah_clasificaciones_list`.`id`) from `project_zarah_clasificaciones_list`)),0) AS `clasificacion_ts` from `project_zarah_estudio_socioeconomico` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-27 14:49:03
